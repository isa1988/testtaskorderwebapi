﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TestTaskOrder.ContainerContract.Contract;

namespace TestTaskOrder_WebAPI.Custom
{
    public class CustomAuthorize : AuthorizeAttribute, IAsyncAuthorizationFilter
    {
        private readonly IAppConfiguration appConfiguration;

        public CustomAuthorize(IAppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }


        public async Task OnAuthorizationAsync(AuthorizationFilterContext authorizationFilterContext)
        {
            var pathObj =
                authorizationFilterContext.ActionDescriptor as
                    Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor;

            if (pathObj.ControllerName == "User" && pathObj.ActionName == "Login")
            {
                return;
            }

            if (!authorizationFilterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                authorizationFilterContext.Result = new ObjectResult("Пользователь не авторизирован")
                {
                    StatusCode = 401
                };
            }
            else
            {
                try
                {
                    var sessionId = Guid.Parse(authorizationFilterContext.HttpContext.User.Identity.Name);
                    var userId = await appConfiguration.GetUserIdBySessionIdAsync(sessionId);
                }
                catch (Exception ex)
                {
                    authorizationFilterContext.Result = new ObjectResult("Пользователь не авторизирован")
                    {
                        StatusCode = 401
                    };
                }
            }
        }
    }
}
