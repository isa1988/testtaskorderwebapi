﻿using System.Security.Claims;
using TestTaskOrder.ContainerContract.Contract;

namespace TestTaskOrder_WebAPI.UserMiddleware
{
    public class AuthenticationSchemeMiddleware
    {
        private readonly RequestDelegate next;
        private readonly IAppConfiguration appConfiguration;

        public AuthenticationSchemeMiddleware(RequestDelegate next, IAppConfiguration appConfiguration)
        {
            this.next = next;
            this.appConfiguration = appConfiguration;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!string.IsNullOrWhiteSpace(context.Request.Headers["AuthorizationToken"]))
            {
                var session = await appConfiguration.GetSessionByTokenAsync(context.Request.Headers["AuthorizationToken"]);

                if (session != null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, session.Id.ToString()),
                        new Claim(ClaimsIdentity.DefaultNameClaimType, session.User.Email),
                    };

                    if (session.RoleId.HasValue)
                    {
                        claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, session.RoleId.ToString()));
                    }

                    var appIdentity = new ClaimsIdentity(claims, "Auth");
                    context.User = new ClaimsPrincipal(appIdentity);
                    
                }
                else
                {
                    context.User = null;
                }

            }
            else
            {
                context.User = null;
            }
            
            await this.next(context);
        }
    }

    public static class AuthenticationSchemeMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationSchemeMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationSchemeMiddleware>();
        }
    }
}
