﻿using Microsoft.AspNetCore.Mvc;
using TestTaskOrder.ContainerContract.Contract.Order;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.Unit;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;
using TestTaskOrder_WebAPI.Custom;

namespace TestTaskOrder_WebAPI.Controllers.Order
{
    [ApiController]
    [ServiceFilter(typeof(CustomAuthorize))]
    [Route("api/v1/[controller]")]
    public class UnitController : ControllerBase
    {
        public UnitController(IUnitContainer unitContainer)
        {
            this.unitContainer = unitContainer;
        }

        private readonly IUnitContainer unitContainer;

        /// <summary>
        /// Поля для сортировки единиц измерений
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoUnit")]
        public async Task<List<EnumInfo>> GetInfoUnitAsync()
        {
            var model = await unitContainer.GetInfoUnitAsync();
            return model;
        }

        /// <summary>
        /// Вернуть всех единиц измерений с удаленными
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("allWithDeleted")]
        public async Task<OperationResultModel<PageInfoListModel<UnitWithDeletedInfoModel, OrderUnitFields>>>
            GetAllWithDeletedAsync(SearchByOneLineFilter<OrderUnitFields> filter)
        {
            var modelList = await unitContainer.GetAllWithDeletedAsync(filter);
            return modelList;
        }

        /// <summary>
        /// Вернуть всех единиц измерений
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("all")]
        public async Task<OperationResultModel<PageInfoListModel<UnitInfoModel, OrderUnitFields>>>
            GetAllAsync(SearchByOneLineFilter<OrderUnitFields> filter)
        {
            var modelList = await unitContainer.GetAllAsync(filter);
            return modelList;
        }

        /// <summary>
        /// Вернуть единицу измерения
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("id/{id:guid}")]
        public async Task<OperationResultModel<UnitWithDeletedInfoModel>> GetByIdAsync(Guid id)
        {
            var model = await unitContainer.GetByIdAsync(id);
            return model;
        }

        /// <summary>
        /// Добавить единицу измерения
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<OperationResultModel<UnitInfoModel>> AddAsync(UnitAddModel request)
        {
            var result = await unitContainer.AddAsync(request);

            return result;
        }

        /// <summary>
        /// Редактировать единицу измерения
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("edit")]
        public async Task<OperationResultModel<UnitInfoModel>> EditAsync(UnitEditModel request)
        {
            var result = await unitContainer.EditAsync(request);

            return result;
        }


        /// <summary>
        /// Удалить единицу измерения
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<OperationResultModel<UnitWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var result = await unitContainer.DeleteAsync(id);

            return result;
        }
    }
}
