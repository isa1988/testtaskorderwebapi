﻿using Microsoft.AspNetCore.Mvc;
using TestTaskOrder.ContainerContract.Contract.Order;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.Filters.Order;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.Order;
using TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;
using TestTaskOrder_WebAPI.Custom;

namespace TestTaskOrder_WebAPI.Controllers.Order
{
    [ApiController]
    [ServiceFilter(typeof(CustomAuthorize))]
    [Route("api/v1/[controller]")]
    public class OrderController : ControllerBase
    {
        public OrderController(IOrderContainer orderContainer,
            IOrderItemContainer orderItemContainer)
        {
            this.orderContainer = orderContainer; 
            this.orderItemContainer = orderItemContainer;
        }

        private readonly IOrderContainer orderContainer;
        private readonly IOrderItemContainer orderItemContainer;

        /// <summary>
        /// Поля для сортировки заказов
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoOrder")]
        public async Task<List<EnumInfo>> GetInfoOrderAsync()
        {
            var model = await orderContainer.GetInfoOrderAsync();
            return model;
        }

        /// <summary>
        /// Вернуть всех заказов с удаленными
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("allWithDeleted")]
        public async Task<OperationResultModel<PageInfoListModel<OrderWithDeletedInfoModel, OrderOrderFields>>>
            GetAllWithDeletedAsync(OrderFilterModel filter)
        {
            var modelList = await orderContainer.GetAllWithDeletedAsync(filter);
            return modelList;
        }
        
        /// <summary>
        /// Вернуть заказа
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("id/{id:guid}")]
        public async Task<OperationResultModel<OrderWithDeletedInfoModel>> GetByIdAsync(Guid id)
        {
            var model = await orderContainer.GetByIdAsync(id);
            return model;
        }

        /// <summary>
        /// Добавить заказа
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<OperationResultModel<OrderInfoModel>> AddAsync(OrderAddModel request)
        {
            var result = await orderContainer.AddAsync(request);

            return result;
        }

        /// <summary>
        /// Редактировать заказа
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("edit")]
        public async Task<OperationResultModel<OrderInfoModel>> EditAsync(OrderEditModel request)
        {
            var result = await orderContainer.EditAsync(request);

            return result;
        }


        /// <summary>
        /// Удалить заказа
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<OperationResultModel<OrderWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var result = await orderContainer.DeleteAsync(id);

            return result;
        }

        /// <summary>
        /// Поля для сортировки товаров заказа
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoOrderItem")]
        public async Task<List<EnumInfo>> GetInfoOrderItemAsync()
        {
            var model = await orderItemContainer.GetInfoOrderItemAsync();
            return model;
        }

        /// <summary>
        /// Вернуть всех товаров заказа
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("allItem")]
        public async Task<OperationResultModel<PageInfoListModel<OrderItemInfoModel, OrderOrderItemFields>>>
            GetAllWithDeletedAsync(OrderItemFilterModel filter)
        {
            var modelList = await orderItemContainer.GetAllAsync(filter);
            return modelList;
        }

        /// <summary>
        /// Вернуть товар в заказе
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("idItem/{id:guid}")]
        public async Task<OperationResultModel<OrderItemInfoModel>> GetItemByIdAsync(Guid id)
        {
            var model = await orderItemContainer.GetByIdAsync(id);
            return model;
        }

        /// <summary>
        /// Добавить товар в заказе
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("createItem")]
        public async Task<OperationResultModel<OrderItemInfoModel>> AddAsync(OrderItemAddModel request)
        {
            var result = await orderItemContainer.AddAsync(request);

            return result;
        }

        /// <summary>
        /// Редактировать товар в заказе
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("editItem")]
        public async Task<OperationResultModel<OrderItemInfoModel>> EditAsync(OrderItemEditModel request)
        {
            var result = await orderItemContainer.EditAsync(request);

            return result;
        }


        /// <summary>
        /// Удалить товар в заказе
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("deleteItem/{id:guid}")]
        public async Task<OperationResultModel<OrderItemInfoModel>> DeleteItemAsync(Guid id)
        {
            var result = await orderItemContainer.DeleteAsync(id);

            return result;
        }
    }
}
