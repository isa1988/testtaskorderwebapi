﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestTaskOrder.ContainerContract.Contract.Provider;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Provider;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;
using TestTaskOrder_WebAPI.Custom;

namespace TestTaskOrder_WebAPI.Controllers.Provider
{
    [ApiController]
    [ServiceFilter(typeof(CustomAuthorize))]
    [Route("api/v1/[controller]")]
    public class ProviderController : ControllerBase
    {
        public ProviderController(IProviderContainer providerContainer)
        {
            this.providerContainer = providerContainer;
        }

        private readonly IProviderContainer providerContainer;

        /// <summary>
        /// Поля для сортировки поставщиков
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoProvider")]
        public async Task<List<EnumInfo>> GetInfoProviderAsync()
        {
            var model = await providerContainer.GetInfoProviderAsync();
            return model;
        }

        /// <summary>
        /// Вернуть всех поставщиков с удаленными
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("allWithDeleted")]
        public async Task<OperationResultModel<PageInfoListModel<ProviderWithDeletedInfoModel, OrderProviderFields>>>
            GetAllWithDeletedAsync(SearchByOneLineFilter<OrderProviderFields> filter)
        {
            var modelList = await providerContainer.GetAllWithDeletedAsync(filter);
            return modelList;
        }

        /// <summary>
        /// Вернуть всех поставщиков
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("all")]
        public async Task<OperationResultModel<PageInfoListModel<ProviderInfoModel, OrderProviderFields>>>
            GetAllAsync(SearchByOneLineFilter<OrderProviderFields> filter)
        {
            var modelList = await providerContainer.GetAllAsync(filter);
            return modelList;
        }

        /// <summary>
        /// Вернуть поставщика
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("id/{id:guid}")]
        public async Task<OperationResultModel<ProviderWithDeletedInfoModel>> GetByIdAsync(Guid id)
        {
            var model = await providerContainer.GetByIdAsync(id);
            return model;
        }

        /// <summary>
        /// Добавить поставщика
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<OperationResultModel<ProviderInfoModel>> AddAsync(ProviderAddModel request)
        {
            var result = await providerContainer.AddAsync(request);

            return result;
        }

        /// <summary>
        /// Редактировать поставщика
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("edit")]
        public async Task<OperationResultModel<ProviderInfoModel>> EditAsync(ProviderEditModel request)
        {
            var result = await providerContainer.EditAsync(request);

            return result;
        }


        /// <summary>
        /// Удалить поставщика
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<OperationResultModel<ProviderWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var result = await providerContainer.DeleteAsync(id);

            return result;
        }
    }
}
