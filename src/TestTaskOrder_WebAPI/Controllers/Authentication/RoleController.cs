﻿using Microsoft.AspNetCore.Mvc;
using TestTaskOrder.ContainerContract.Contract.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;
using TestTaskOrder_WebAPI.Custom;

namespace TestTaskOrder_WebAPI.Controllers.Authentication
{
    [ApiController]
    [ServiceFilter(typeof(CustomAuthorize))]
    [Route("api/v1/[controller]")]
    public class RoleController : ControllerBase
    {
        public RoleController(IRoleContainer roleContainer)
        {
            this.roleContainer = roleContainer;
        }
        private readonly IRoleContainer roleContainer;

        /// <summary>
        /// Вернуть все роли
        /// </summary>
        /// <param name="orderSettingModel"></param>
        /// <returns></returns>
        [HttpPost("all")]
        public async Task<OperationResultModel<PageInfoListModel<RoleInfoModel, OrderRoleFields>>> GetAllRoles(
            PagingOrderSettingModel<OrderRoleFields> orderSettingModel)
        {
            var modelList = await roleContainer.GetAllAsync(orderSettingModel);
            return modelList;
        }

        /// <summary>
        /// Вернуть все роли
        /// </summary>
        /// <param name="orderSettingModel"></param>
        /// <returns></returns>
        [HttpPost("allWithDelete")]
        public async Task<OperationResultModel<PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>>> GetAllWithDeletedAsync(
            PagingOrderSettingModel<OrderRoleFields> orderSettingModel)
        {
            var modelList = await roleContainer.GetAllWithDeletedAsync(orderSettingModel);
            return modelList;
        }

        /// <summary>
        /// Вернуть роль 
        /// </summary>
        /// <returns></returns>
        [HttpGet("id/{id:guid}")]
        public async Task<OperationResultModel<RoleWithDeletedInfoModel>> GetIdAsync(Guid id)
        {
            var modelList = await roleContainer.GetIdAsync(id);
            return modelList;
        }

        /// <summary>
        /// Добавить роль 
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<OperationResultModel<RoleWithDeletedInfoModel>> CreateAsync(RoleAddModel request)
        {
            var modelList = await roleContainer.CreateAsync(request);
            return modelList;
        }

        /// <summary>
        /// Редактировать роль 
        /// </summary>
        /// <returns></returns>
        [HttpPost("edit")]
        public async Task<OperationResultModel<RoleWithDeletedInfoModel>> EditAsync(RoleEditModel request)
        {
            var modelList = await roleContainer.EditAsync(request);
            return modelList;
        }
    }
}
