﻿using Microsoft.AspNetCore.Mvc;
using TestTaskOrder.ContainerContract.Contract.Authentication;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.Filters.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;
using TestTaskOrder_WebAPI.Custom;

namespace TestTaskOrder_WebAPI.Controllers.Authentication
{
    [ApiController]
    [ServiceFilter(typeof(CustomAuthorize))]
    [Route("api/v1/[controller]")]
    public class UserController : ControllerBase
    {
        public UserController(IUserContainer userContainer, 
            IUserRoleContainer userRoleContainer)
        {
            this.userContainer = userContainer;
            this.userRoleContainer = userRoleContainer;
        }

        private readonly IUserContainer userContainer;
        private readonly IUserRoleContainer userRoleContainer;

        /// <summary>
        /// Поля для сортировки пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoOrderUser")]
        public async Task<List<EnumInfo>> GetInfoOrderUserAsync()
        {
            var modelList = await userContainer.GetInfoOrderUserAsync();
            return modelList;
        }


        /// <summary>
        /// Вытащить всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpPost("allWithLastState")]
        public async Task<OperationResultModel<PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>>> GetAll(
            [FromBody] PagingOrderSettingModel<OrderUserFields> orderModel)
        {
            var modelList = await userContainer.GetAllWithLastStateAsync(orderModel);
            return modelList;
        }

        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("searchWithLastState")]
        public async Task<OperationResultModel<PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>>>
            GetAllFromSearchAsync(SearchByOneLineFilter<OrderUserFields> filter)
        {
            var modelList = await userContainer.GetAllWithLastStateFromSearchAsync(filter);
            return modelList;
        }


        /// <summary>
        /// Показать пользователя по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("id/{id:guid}")]
        public async Task<OperationResultModel<UserWithDeletedInfoModel>> GetById(Guid id)
        {
            var responseModel = await userContainer.GetByIdAsync(id);
            return responseModel;
        }


        /// <summary>
        /// Показать информацию о текущем пользователе
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("infoCurrentUser")]
        public async Task<OperationResultModel<UserCurrentInfoModel>> GetInfoCurrentUser()
        {
            Guid sessionId = Guid.Parse(User.Identity.Name);

            var responseModel = await userContainer.GetInfoCurrentUser(sessionId);
            return responseModel;
        }

        /// <summary>
        /// Авторизация
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<OperationResultModel<AuthenticateResponseModel>> Login([FromBody]LoginModel request)
        {
            var model = await userContainer.LoginAsync(request);
            
            return model;
        }

        /// <summary>
        /// Завершить сеанс работы
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("logout")]
        public async Task<OperationResultModel<bool>> Logout()
        {
            Guid sessionId = Guid.Parse(User.Identity.Name);

            var result = await userContainer.LogoutAsync(sessionId);

            return result;
        }

        /// <summary>
        /// Вернуть список не заполненных ролей для пользователя
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("roleExcludeUserRole")]
        public async Task<OperationResultModel<PageInfoListModel<RoleInfoModel, OrderRoleFields>>>
            GetRoleExcludeUserRoleAsync(RoleFilter filter)
        {
            var modelList = await userContainer.GetRoleExcludeUserRoleAsync(filter);
            return modelList;
        }

        /// <summary>
        /// Подтвеждеие роли, если у пользователя больше одной
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        [HttpPost("roleConfirm")]
        public async Task<OperationResultModel<bool>> RoleConfirm([FromBody] LoginConfirmRoleModel request)
        {
            request.SessionId = Guid.Parse(User.Identity.Name);
            var model = await userContainer.RoleConfirmAsync(request);
            
            return model;
        }
        
        /// <summary>
        /// Админ управление в нашу компанию
        /// </summary>
        /// <param name="registrationModel"></param>
        /// <returns></returns>
        [HttpPost("registrationByAdmin")]
        public async Task<OperationResultModel<UserWithDeletedInfoModel>> RegistrationByAdminAsync(RegistrationByAdminViewModel registrationModel)
        {
            var result = await userContainer.RegistrationByAdminAsync(registrationModel);

            return result;
        }

        /// <summary>
        /// Редакировать пользователя
        /// </summary>
        /// <param name="editModel"></param>
        /// <returns></returns>
        [HttpPut("edit")]
        public async Task<OperationResultModel<UserWithDeletedInfoModel>> EditAsync(UserEditModel editModel)
        {
            var result = await userContainer.EditAsync(editModel);

            return result;
        }

        /// <summary>
        /// Редакировать текущего пользователя
        /// </summary>
        /// <param name="editModel"></param>
        /// <returns></returns>
        [HttpPut("editCurrent")]
        public async Task<OperationResultModel<UserWithDeletedInfoModel>> EditCurrentAsync(CurrentUserEditModel editModel)
        {
            editModel.SessionId = Guid.Parse(User.Identity.Name);
            var result = await userContainer.EditCurrentAsync(editModel);

            return result;
        }

        /// <summary>
        /// Поменять пароль
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPut("changePassword")]
        public async Task<OperationResultModel<bool>> ChangePasswordAsync(UserChangePasswordModel changePasswordModel)
        {
            var result = await userContainer.ChangePasswordAsync(changePasswordModel);

            return result;
        }

        /// <summary>
        /// Поменять пароль у текущего пользоватля
        /// </summary>
        /// <param name="changePasswordModel"></param>
        /// <returns></returns>
        [HttpPut("changePasswordCurrent")]
        public async Task<OperationResultModel<bool>> ChangePasswordCurrentAsync(CurrentUserChangePasswordModel changePasswordModel)
        {
            changePasswordModel.SessionId = Guid.Parse(User.Identity.Name);
            var result = await userContainer.ChangePasswordCurrentAsync(changePasswordModel);
            
            return result;
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<OperationResultModel<UserWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var result = await userContainer.DeleteAsync(id);
            
            return result;
        }

        /// <summary>
        /// Поля для сортировки пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoOrderRole")]
        public async Task<List<EnumInfo>> GetInfoOrderRoleAsync()
        {
            var modelList = await userContainer.GetInfoOrderRoleAsync();
            return modelList;
        }
        
        /// <summary>
        /// Все роли для текущего пользователя
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("getAllRoleForUser")]
        public async Task<OperationResultModel<PageInfoListModel<UserRoleInfoModel, OrderUserRoleFields>>> GetAllRoleForUserAsync(UserRoleFilter filter)
        {
            var result = await userRoleContainer.GetAllForUserAsync(filter);
            return result;
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="addRoleForUserModel"></param>
        /// <returns></returns>
        [HttpPut("addRole")]
        public async Task<OperationResultModel<UserRoleInfoModel>> AddRoleForUserAsync(AddRoleForUserModel addRoleForUserModel)
        {
            var result = await userContainer.AddRoleForUserAsync(addRoleForUserModel);
            
            return result;
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="userRoleId"></param>
        /// <returns></returns>
        [HttpDelete("deleteRole")]
        public async Task<OperationResultModel<UserRoleInfoModel>> DeleteRoleForUserAsync(Guid userRoleId)
        {
            var result = await userContainer.DeleteRoleForUserAsync(userRoleId);
            
            return result;
        }
        
        /// <summary>
        /// Вернуть приветствие
        /// </summary>
        /// <returns></returns>
        [HttpGet("userCaption")]
        public async Task<OperationResultModel<UserCaptionModel>> GetUserCaptionAsync()
        {
            Guid sessionId = Guid.Parse(User.Identity.Name);
            var result = await userContainer.GetUserCaptionAsync(sessionId);
            return result;
        }
        
    }
}
