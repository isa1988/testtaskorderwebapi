﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using TestTaskOrder.DAL.Data;

namespace TestTaskOrder_WebAPI.AppStart
{
    public static class DatabaseContextServiceExtension
    {
        public static void AddDatabaseContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("DefaultConnection");

            services.AddDbContextPool<TestTaskOrderContext>(options => options.UseNpgsql(connection));

            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }
    }

    public class BloggingContextFactory : IDesignTimeDbContextFactory<TestTaskOrderContext>
    {
        public TestTaskOrderContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Development.json")
                .Build();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            var optionsBuilder = new DbContextOptionsBuilder<TestTaskOrderContext>();
            optionsBuilder.UseNpgsql(connectionString);

            return new TestTaskOrderContext(optionsBuilder.Options);
        }
    }
}
