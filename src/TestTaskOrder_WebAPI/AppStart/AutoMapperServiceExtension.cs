﻿using AutoMapper;

namespace TestTaskOrder_WebAPI.AppStart
{
    public static class AutoMapperServiceExtension
    {
        public static void AddAutoMapperCustom(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new TestTaskOrder.Manager.MappingProfile());
                mc.AddProfile(new TestTaskOrder.Container.MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
