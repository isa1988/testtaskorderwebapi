﻿using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace TestTaskOrder_WebAPI.AppStart
{
    class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            var httpContext = context.GetHttpContext();
            return httpContext.User.Identity.IsAuthenticated && httpContext.User.IsInRole("HangfireAdmin");
        }
    }
}
