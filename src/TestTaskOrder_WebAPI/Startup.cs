using System.Reflection;
using Autofac;
using Hangfire;
using Hangfire.Common;
using Hangfire.PostgreSql;
using HangfireBasicAuthenticationFilter;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using TestTaskOrder.Container;
using TestTaskOrder.Container.Authentication;
using TestTaskOrder.Container.Order;
using TestTaskOrder.Container.Provider;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Authentication;
using TestTaskOrder.ContainerContract.Contract.Order;
using TestTaskOrder.ContainerContract.Contract.Provider;
using TestTaskOrder.ContainerContract.ViewModel.Setting;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Contract.Provider;
using TestTaskOrder.Core.DataBaseInitializer;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.DAL.Repository.Authentication;
using TestTaskOrder.DAL.Repository.Order;
using TestTaskOrder.DAL.Repository.Provider;
using TestTaskOrder.Manager.Authentication;
using TestTaskOrder.Manager.Order;
using TestTaskOrder.Manager.Provider;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Contract.Provider;
using TestTaskOrder.Primitive.Helper;
using TestTaskOrder_WebAPI.AppStart;
using TestTaskOrder_WebAPI.Custom;
using TestTaskOrder_WebAPI.UserMiddleware;

namespace TestTaskOrder_WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

            if (env.IsDevelopment())
            {
                builder = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.Development.json");
            }

            Configuration = builder.Build();
            this.env = env;
        }
        
        public IConfiguration Configuration { get; }

        private readonly IWebHostEnvironment env;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var siteList = Configuration.GetSection("FriendlyFrontServer:Site").Value.Split(";");
            siteList = siteList.Select(f => f.Trim()).ToArray();

            services.AddCors(options =>
            {
                options.AddPolicy("TestTaskOrderApp",
                    builder => builder
                        .WithOrigins(siteList)
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            services.AddControllers();

            services.AddDatabaseContext(Configuration); 
            services.AddAutoMapperCustom();
            

            services.Configure<JwtModel>(Configuration.GetSection("Jwt"));

            services.AddTransient<CustomAuthorize>();
            services.AddSingleton<IAppConfiguration, AppConfiguration>();

            services.AddHangfire(options =>
            {
                options.UsePostgreSqlStorage(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddSwaggerGen(swagger =>
            {
                //This is to generate the Default UI of Swagger Documentation  
                swagger.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "JWT Token Authentication API",
                    Description = "ASP.NET Core 3.1 Web API",
                    
                });
                // To Enable authorization using Swagger (JWT)  
                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "AuthorizationToken",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}

                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                swagger.IncludeXmlComments(xmlPath);
            });

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
                            IWebHostEnvironment env, 
                            IAppConfiguration appconfig,
                            IDataBaseInitializer baseInitializer,
                            IHelperReadToAppConfigContainer helperReadToAppConfig,
                            Hangfire.IRecurringJobManager recurringJobs)
        {
            app.UseAuthenticationSchemeMiddleware();

            app.UseCors("TestTaskOrderApp");

            appconfig.PageSize = 50;
            ConstValue.NameOfDB = Configuration.GetSection("DDName").Value;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseRouting();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Swagger Configuration in API  
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API v1");
            });

            baseInitializer.InitializeForDeveloperAsync().GetAwaiter().GetResult();

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[]
                {
                    new HangfireCustomBasicAuthenticationFilter{
                        User = Configuration.GetSection("HangfireSettings:UserName").Value,
                        Pass = Configuration.GetSection("HangfireSettings:Password").Value
                    }
                }
            });
            
            app.UseHangfireServer();

            helperReadToAppConfig.LoadAsync().GetAwaiter().GetResult();
            

            string load = Configuration.GetSection("TimeForCron:Load").Value;
            recurringJobs.AddOrUpdate("Load", Job.FromExpression<IHelperReadToAppConfigContainer>(x =>
                x.LoadAsync()), load, TimeZoneInfo.Local);
            
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<RoleRepository>().As<IRoleRepository>();
            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>();
            builder.RegisterType<SessionRepository>().As<ISessionRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<OrderRepository>().As<IOrderRepository>();
            builder.RegisterType<OrderItemRepository>().As<IOrderItemRepository>();
            builder.RegisterType<UnitRepository>().As<IUnitRepository>();
            builder.RegisterType<ProviderRepository>().As<IProviderRepository>();


            if (env.IsDevelopment())
            {
                builder.RegisterType<DataBaseInitializerDeveloper>().As<IDataBaseInitializer>();
            }
            else
            {
                builder.RegisterType<DataBaseInitializerProduction>().As<IDataBaseInitializer>();
            }

            builder.RegisterType<UserContainer>().As<IUserContainer>();
            builder.RegisterType<UserRoleContainer>().As<IUserRoleContainer>();
            builder.RegisterType<RoleContainer>().As<IRoleContainer>();
            builder.RegisterType<OrderContainer>().As<IOrderContainer>();
            builder.RegisterType<OrderItemContainer>().As<IOrderItemContainer>();
            builder.RegisterType<UnitContainer>().As<IUnitContainer>();
            builder.RegisterType<ProviderContainer>().As<IProviderContainer>();
            builder.RegisterType<HelperReadToAppConfigContainer>().As<IHelperReadToAppConfigContainer>();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<RoleService>().As<IRoleService>();
            builder.RegisterType<UserRoleService>().As<IUserRoleService>();
            builder.RegisterType<SessionService>().As<ISessionService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<OrderItemService>().As<IOrderItemService>();
            builder.RegisterType<UnitService>().As<IUnitService>();
            builder.RegisterType<ProviderService>().As<IProviderService>();
            
        }
    }
}
