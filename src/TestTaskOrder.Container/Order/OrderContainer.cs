﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Order;
using TestTaskOrder.ContainerContract.Filters.Order;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.Order;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Container.Order
{
    public class OrderContainer : GeneralContainer<OrderInfoModel, OrderOrderFields>, IOrderContainer
    {

        public OrderContainer(IAppConfiguration appConfiguration,
            IMapper mapper,
            IOrderService orderService) 
            : base(appConfiguration)
        {
            this.mapper = mapper;
            this.orderService = orderService;
        }

        private readonly IMapper mapper;
        private readonly IOrderService orderService;

        public async Task<List<EnumInfo>> GetInfoOrderAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderOrderFields>();

            return retList;
        }

        public async Task<OperationResultModel<PageInfoListModel<OrderWithDeletedInfoModel, OrderOrderFields>>>
            GetAllWithDeletedAsync(OrderFilterModel filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterDto = mapper.Map<OrderFilterDto>(filter);
            var result = await orderService.GetAllWithDelAsync(filterDto);
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<OrderWithDeletedInfoModel, OrderOrderFields>>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<
                    PageInfoListModel<OrderWithDeletedInfoModel, OrderOrderFields>>(null,
                    result.Code, result.ErrorMessage);
            }
        }
        
        public async Task<OperationResultModel<OrderWithDeletedInfoModel>> GetByIdAsync(Guid id)
        {
            var response = await orderService.GetByIdDetailsAsync(id);
            if (response.IsSuccess)
            {
                var model =
                    mapper.Map<OrderWithDeletedInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderWithDeletedInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderInfoModel>> AddAsync(OrderAddModel request)
        {
            var dto = mapper.Map<OrderDto>(request);

            var response = await orderService.CreateAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<OrderInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderInfoModel>> EditAsync(OrderEditModel request)
        {
            var dto = mapper.Map<OrderDto>(request);

            var response = await orderService.EditAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<OrderInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var response = await orderService.DeleteItemAsync(id);
            if (response.IsSuccess)
            {
                var model = mapper.Map<OrderWithDeletedInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderWithDeletedInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }
    }
}
