﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Order;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.Unit;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Container.Order
{
    public class UnitContainer : GeneralContainer<UnitInfoModel, OrderUnitFields>, IUnitContainer
    {

        public UnitContainer(IAppConfiguration appConfiguration,
            IMapper mapper,
            IUnitService unitService) 
            : base(appConfiguration)
        {
            this.mapper = mapper;
            this.unitService = unitService;
        }

        private readonly IMapper mapper;
        private readonly IUnitService unitService;

        public async Task<List<EnumInfo>> GetInfoUnitAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderUnitFields>();

            return retList;
        }

        public async Task<OperationResultModel<PageInfoListModel<UnitWithDeletedInfoModel, OrderUnitFields>>>
            GetAllWithDeletedAsync(SearchByOneLineFilter<OrderUnitFields> filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterDto = mapper.Map<SearchByOneLineDtoFilter<OrderUnitFields>>(filter);
            var result = await unitService.GetAllWithDelAsync(filterDto);
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<UnitWithDeletedInfoModel, OrderUnitFields>>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<
                    PageInfoListModel<UnitWithDeletedInfoModel, OrderUnitFields>>(null,
                    result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<PageInfoListModel<UnitInfoModel, OrderUnitFields>>>
            GetAllAsync(SearchByOneLineFilter<OrderUnitFields> filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterDto = mapper.Map<SearchByOneLineDtoFilter<OrderUnitFields>>(filter);
            var result = await unitService.GetAllAsync(filterDto);
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<UnitInfoModel, OrderUnitFields>>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<
                    PageInfoListModel<UnitInfoModel, OrderUnitFields>>(null,
                    result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UnitWithDeletedInfoModel>> GetByIdAsync(Guid id)
        {
            var response = await unitService.GetByIdDetailsAsync(id);
            if (response.IsSuccess)
            {
                var model =
                    mapper.Map<UnitWithDeletedInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UnitWithDeletedInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UnitInfoModel>> AddAsync(UnitAddModel request)
        {
            var dto = mapper.Map<UnitDto>(request);

            var response = await unitService.CreateAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<UnitInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UnitInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UnitInfoModel>> EditAsync(UnitEditModel request)
        {
            var dto = mapper.Map<UnitDto>(request);

            var response = await unitService.EditAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<UnitInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UnitInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UnitWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var response = await unitService.DeleteItemAsync(id);
            if (response.IsSuccess)
            {
                var model = mapper.Map<UnitWithDeletedInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UnitWithDeletedInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }
    }
}
