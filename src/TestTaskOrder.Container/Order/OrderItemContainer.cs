﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Order;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.Filters.Order;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Container.Order
{
    public class OrderItemContainer : GeneralContainer<OrderItemInfoModel, OrderOrderItemFields>, IOrderItemContainer
    {

        public OrderItemContainer(IAppConfiguration appConfiguration,
            IMapper mapper,
            IOrderItemService OrderItemService) 
            : base(appConfiguration)
        {
            this.mapper = mapper;
            this.OrderItemService = OrderItemService;
        }

        private readonly IMapper mapper;
        private readonly IOrderItemService OrderItemService;

        public async Task<List<EnumInfo>> GetInfoOrderItemAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderOrderItemFields>();

            return retList;
        }
        
        public async Task<OperationResultModel<PageInfoListModel<OrderItemInfoModel, OrderOrderItemFields>>>
            GetAllAsync(OrderItemFilterModel filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterDto = mapper.Map<OrderItemFilterDto>(filter);
            var result = await OrderItemService.GetAllAsync(filterDto);
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<OrderItemInfoModel, OrderOrderItemFields>>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<
                    PageInfoListModel<OrderItemInfoModel, OrderOrderItemFields>>(null,
                    result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderItemInfoModel>> GetByIdAsync(Guid id)
        {
            var response = await OrderItemService.GetByIdDetailsAsync(id);
            if (response.IsSuccess)
            {
                var model =
                    mapper.Map<OrderItemInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderItemInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderItemInfoModel>> AddAsync(OrderItemAddModel request)
        {
            var dto = mapper.Map<OrderItemDto>(request);

            var response = await OrderItemService.CreateAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<OrderItemInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderItemInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderItemInfoModel>> EditAsync(OrderItemEditModel request)
        {
            var dto = mapper.Map<OrderItemDto>(request);

            var response = await OrderItemService.EditAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<OrderItemInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderItemInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<OrderItemInfoModel>> DeleteAsync(Guid id)
        {
            var response = await OrderItemService.DeleteItemAsync(id);
            if (response.IsSuccess)
            {
                var model = mapper.Map<OrderItemInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<OrderItemInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }
    }
}
