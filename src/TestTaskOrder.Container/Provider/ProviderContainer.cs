﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Provider;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Provider;
using TestTaskOrder.ManagerContract.Contract.Provider;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.ManagerContract.Dto.Provider;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;

namespace TestTaskOrder.Container.Provider
{
    public class ProviderContainer : GeneralContainer<ProviderInfoModel, OrderProviderFields>, IProviderContainer
    {

        public ProviderContainer(IAppConfiguration appConfiguration,
            IMapper mapper,
            IProviderService providerService) 
            : base(appConfiguration)
        {
            this.mapper = mapper;
            this.providerService = providerService;
        }

        private readonly IMapper mapper;
        private readonly IProviderService providerService;

        public async Task<List<EnumInfo>> GetInfoProviderAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderProviderFields>();

            return retList;
        }

        public async Task<OperationResultModel<PageInfoListModel<ProviderWithDeletedInfoModel, OrderProviderFields>>>
            GetAllWithDeletedAsync(SearchByOneLineFilter<OrderProviderFields> filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterDto = mapper.Map<SearchByOneLineDtoFilter<OrderProviderFields>>(filter);
            var result = await providerService.GetAllWithDelAsync(filterDto);
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<ProviderWithDeletedInfoModel, OrderProviderFields>>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<
                    PageInfoListModel<ProviderWithDeletedInfoModel, OrderProviderFields>>(null,
                    result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<PageInfoListModel<ProviderInfoModel, OrderProviderFields>>>
            GetAllAsync(SearchByOneLineFilter<OrderProviderFields> filter)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            var filterDto = mapper.Map<SearchByOneLineDtoFilter<OrderProviderFields>>(filter);
            var result = await providerService.GetAllAsync(filterDto);
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<ProviderInfoModel, OrderProviderFields>>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<
                    PageInfoListModel<ProviderInfoModel, OrderProviderFields>>(null,
                    result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<ProviderWithDeletedInfoModel>> GetByIdAsync(Guid id)
        {
            var response = await providerService.GetByIdDetailsAsync(id);
            if (response.IsSuccess)
            {
                var model =
                    mapper.Map<ProviderWithDeletedInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<ProviderWithDeletedInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<ProviderInfoModel>> AddAsync(ProviderAddModel request)
        {
            var dto = mapper.Map<ProviderDto>(request);

            var response = await providerService.CreateAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<ProviderInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<ProviderInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<ProviderInfoModel>> EditAsync(ProviderEditModel request)
        {
            var dto = mapper.Map<ProviderDto>(request);

            var response = await providerService.EditAsync(dto);
            if (response.IsSuccess)
            {
                var model = mapper.Map<ProviderInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<ProviderInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<ProviderWithDeletedInfoModel>> DeleteAsync(Guid id)
        {
            var response = await providerService.DeleteItemAsync(id);
            if (response.IsSuccess)
            {
                var model = mapper.Map<ProviderWithDeletedInfoModel>(response.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<ProviderWithDeletedInfoModel>(null, response.Code, response.ErrorMessage);
            }
        }
    }
}
