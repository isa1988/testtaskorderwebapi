﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Session;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;

namespace TestTaskOrder.Container
{
    public class AppConfiguration : IAppConfiguration
    {

        public AppConfiguration(IMapper mapper)
        {
            this.mapper = mapper;
        }

        private readonly IMapper mapper;
        
        public int PageSize { get; set; }
        public List<UserInfoModel> UserList { get; set; } = new List<UserInfoModel>();
        public List<SessionWithTokenInfoModel> SessionList { get; set; } = new List<SessionWithTokenInfoModel>();
        public List<UserRoleInfoModel> UserRoleList { get; set; } = new List<UserRoleInfoModel>();
        public List<RoleWithDeletedInfoModel> RoleList { get; set; } = new List<RoleWithDeletedInfoModel>();
        
        public async Task<UserInfoModel> GetUserByIdAsync(Guid userId)
        {
            var user = UserList.FirstOrDefault(s => s.Id == userId);

            return user;
        }

        public async Task LoginAsync(SessionWithTokenInfoModel model)
        { 
            model.User = UserList.FirstOrDefault(x => x.Id == model.UserId);

            var session = SessionList.FirstOrDefault(s => s.UserId == model.UserId);

            if (session != null)
            {
                SessionList[SessionList.IndexOf(session)] = model;
            }
            else
            {
                SessionList.Add(model);
            }
        }

        public async Task LogoutAsync(Guid sessionId)
        {
            var session = SessionList.FirstOrDefault(s => s.Id == sessionId);

            if (session != null)
            {
                SessionList.Remove(session);
            }
        }


        public async Task<RoleWithDeletedInfoModel> GetRoleBySessionIdAsync(Guid sessionId)
        {
            var session = SessionList.FirstOrDefault(s => s.Id == sessionId);
            if (session == null || !session.RoleId.HasValue)
            {
                return new RoleWithDeletedInfoModel();
            }

            var userRole = UserRoleList.FirstOrDefault(x => x.Id == session.RoleId.Value);
            if (userRole == null)
            {
                return new RoleWithDeletedInfoModel();
            }
            var role = RoleList.FirstOrDefault(x => x.Id == userRole.RoleId);

            return role;
        }

        public async Task<UserInfoModel> GetUserBySessionIdAsync(Guid sessionId)
        {
            var session = SessionList.FirstOrDefault(s => s.Id == sessionId);

            var user = session?.User;

            return user;
        } 
        
        public async Task<Guid> GetUserIdBySessionIdAsync(Guid sessionId)
        {
            var session = SessionList.FirstOrDefault(s => s.Id == sessionId);

            var userId = session?.UserId ?? throw new RawDbNullException("Пользователь не найдена");

            return userId;
        }

        public async Task<SessionInfoModel> GetSessionByTokenAsync(string token)
        {
            var sessionWithToken = SessionList.FirstOrDefault(s => s.Token == token);
            
            var session = mapper.Map<SessionInfoModel>(sessionWithToken);

            return session;
        }

        public async Task<SessionInfoModel> GetSessionByIdAsync(Guid id)
        {
            var sessionWithToken = SessionList.FirstOrDefault(s => s.Id == id);

            if (sessionWithToken == null)
            {
                throw new RawDbNullException("Сессия не найдена");
            }

            var session = mapper.Map<SessionInfoModel>(sessionWithToken);

            return session;
        }
        
        public async Task SetPageSize(int pageSize)
        {
            PageSize = pageSize;
        }
        
        public async Task RoleConfirmAsync(Guid sessionId, Guid? roleId)
        {
            var sessionModel = SessionList.FirstOrDefault(s => s.Id == sessionId);

            if (sessionModel != null)
            {
                sessionModel.RoleId = roleId;
            }
        }

        public async Task RegistrationAsync(UserInfoModel user, List<UserRoleInfoModel> userRoleList)
        {
            UserList.Add(user);
            UserRoleList.AddRange(userRoleList);
        }

        public async Task EditUserAsync(UserInfoModel user)
        {
            var userViewModel = UserList.FirstOrDefault(u => u.Id == user.Id);

            if (userViewModel != null)
            {
                userViewModel.FirstName = user.FirstName;
                userViewModel.MiddleName = user.MiddleName;
                userViewModel.LastName = user.LastName;
                userViewModel.Phone = user.Phone;
                userViewModel.Login = user.Login;

                var sessionList = SessionList.Where(x => x.UserId == user.Id).ToList();
                foreach (var session in sessionList)
                {
                    session.User.FirstName = user.FirstName;
                    session.User.MiddleName = user.MiddleName;
                    session.User.LastName = user.LastName;
                    session.User.Phone = user.Phone;
                    userViewModel.Login = user.Login;
                }
            }
            else
            {
                UserList.Add(user);
            }
        }

        public async Task DeleteUserAsync(Guid userId)
        {
            var user = UserList.FirstOrDefault(u => u.Id == userId);

            if (user != null)
            {
                var userRoleList = UserRoleList.Where(u => u.UserId == user.Id).ToList();

                foreach (var userRole in userRoleList)
                {
                    UserRoleList.Remove(userRole);
                }

                var session = SessionList.FirstOrDefault(s => s.UserId == user.Id);

                if (session != null)
                {
                    SessionList.Remove(session);
                }
                
                UserList.Remove(user);
            }
        }

        public async Task AddUserRoleAsync(UserRoleInfoModel userRoleModel)
        {
            var role = RoleList.FirstOrDefault(x => x.Id == userRoleModel.RoleId);
            userRoleModel.Role = mapper.Map<RoleInfoModel>(role);
            userRoleModel.User = UserList.FirstOrDefault(x => x.Id == userRoleModel.UserId);

            UserRoleList.Add(userRoleModel);
        }

        public async Task DeleteUserRoleAsync(Guid userRoleId)
        {
            var userRoleModel = UserRoleList.FirstOrDefault(u => u.Id == userRoleId);

            if (userRoleModel != null)
            {
                UserRoleList.Remove(userRoleModel);
            }
        }

        

        public async Task SetRoleAsync(RoleWithDeletedInfoModel roleModel)
        {
            var role = RoleList.FirstOrDefault(x => x.Id == roleModel.Id);
            if (role != null)
            {
                role.Name = roleModel.Name;
                role.SysName = role.SysName;
                var sessionList = SessionList.Where(x => x.RoleId == roleModel.Id).ToList();
                foreach (var session in sessionList)
                {
                    session.Role.Name = roleModel.Name;
                    session.Role.SysName = role.SysName;
                }
            }
            else
            {
                RoleList.Add(roleModel);
            }

            var userUserRoles = UserRoleList.Where(x => x.RoleId == roleModel.Id).ToList();
            foreach (var userRoleView in userUserRoles)
            {
                if (userRoleView.Role == null)
                {
                    continue;
                }
                userRoleView.Role.Name = roleModel.Name;
                userRoleView.Role.SysName = role.SysName;
            }
        }

        private PagingOrderSettingModel<TEnum> DefaultOrder<TEnum>(PagingOrderSettingModel<TEnum> orderSettingModel)
            where TEnum : Enum
        {
            if (orderSettingModel == null)
            {
                return new PagingOrderSettingModel<TEnum>
                {
                    StartPosition = 0,
                    PageSize = PageSize
                };
            }
            else if (orderSettingModel.PageSize == 0)
            {
                orderSettingModel.PageSize = PageSize;
                return orderSettingModel;
            }
            else
            {
                return orderSettingModel;
            }
        }

        private OperationResultModel<T> GetOperationResult<T>(T model, ResultCode code, string error)
        {
            return new OperationResultModel<T>
            {
                Result = model,
                Code = code,
                Message = error,
            };
        }

    }
}

