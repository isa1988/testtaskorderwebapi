﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Authentication;
using TestTaskOrder.ContainerContract.Filters.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Container.Authentication
{
    public class UserRoleContainer : GeneralContainer<UserRoleInfoModel, OrderUserRoleFields>, IUserRoleContainer
    {
        public UserRoleContainer(IMapper mapper, 
            IAppConfiguration appConfiguration,
            IUserRoleService userRoleService) : base(appConfiguration)
        {
            this.mapper = mapper;
            this.userRoleService = userRoleService;
        }

        private readonly IMapper mapper;
        private readonly IUserRoleService userRoleService;

        public async Task<OperationResultModel<PageInfoListModel<UserRoleInfoModel, OrderUserRoleFields>>>
            GetAllForUserAsync(UserRoleFilter filter, bool isFromDb = false)
        {

            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            
            if (isFromDb)
            {
                return GetOperationResult(new PageInfoListModel<UserRoleInfoModel, OrderUserRoleFields>(),
                    ResultCode.Success, string.Empty);
            }
            else
            {
                var userRoleViewModel = appConfiguration.UserRoleList.Where(u => u.UserId == filter.UserId).ToList();

                var result = await GetListOfTypeAsync(filter.OrderSetting, userRoleViewModel);

                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
        }

        protected override IQueryable<UserRoleInfoModel> OrderSort(IQueryable<UserRoleInfoModel> query, PagingOrderSettingModel<OrderUserRoleFields> pagingOrderSetting)
        {

            if (!pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderUserRoleFields.None)
                return query;
            
            return query;
        }
    }
}
