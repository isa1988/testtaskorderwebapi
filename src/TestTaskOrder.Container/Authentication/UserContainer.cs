﻿using AutoMapper;
using Microsoft.Extensions.Options;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Authentication;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.Filters.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Session;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.ContainerContract.ViewModel.Setting;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.ManagerContract.Dto.Setting;
using TestTaskOrder.ManagerContract.Filters.Authentication;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Container.Authentication
{
    public class UserContainer : GeneralContainer<UserWithDeletedInfoModel, OrderUserFields>, IUserContainer
    {
        public UserContainer(IMapper mapper,
            IHelperReadToAppConfigContainer helperReadToAppConfig,
            IUserService userService,
            IUserRoleService userRoleService,
            ISessionService sessionService,
            IAppConfiguration appConfiguration,
            IOptions<JwtModel> jwtSettingsModel)
            : base(appConfiguration)
        {
            this.mapper = mapper;
            this.helperReadToAppConfig = helperReadToAppConfig;
            this.userService = userService;
            this.userRoleService = userRoleService;
            this.sessionService = sessionService;
            this.jwtSettingsModel = jwtSettingsModel;
        }


        private readonly IMapper mapper;
        private readonly IHelperReadToAppConfigContainer helperReadToAppConfig;
        private readonly IUserService userService;
        private readonly IUserRoleService userRoleService;
        private readonly ISessionService sessionService;
        private readonly IOptions<JwtModel> jwtSettingsModel;

        public async Task<List<EnumInfo>> GetInfoOrderUserAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderUserFields>();

            return retList;
        }

        public async Task<OperationResultModel<PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>>>
            GetAllWithLastStateAsync(
                PagingOrderSettingModel<OrderUserFields> orderSettingModel,
                bool isFromDb = false)
        {

            orderSettingModel = DefaultOrder(orderSettingModel);

            if (isFromDb)
            {
                return GetOperationResult(new PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>(), ResultCode.Success, string.Empty);
            }
            else
            {
                var userListItemViewModel = mapper.Map<List<UserWithDeletedInfoModel>>(appConfiguration.UserList);

                var result = await GetListOfTypeAsync(orderSettingModel, userListItemViewModel);
                
                var model = new PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>()
                {
                    ValueList = result.ValueList,
                    OrderSetting = result.OrderSetting,
                    TotalCount = result.TotalCount
                };

                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
        }

        public async Task<OperationResultModel<PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>>> GetAllWithLastStateFromSearchAsync(
            SearchByOneLineFilter<OrderUserFields> filter, 
            bool isFromDb = false)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            
            if (isFromDb)
            {

                return GetOperationResult(new PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>(), ResultCode.Success, string.Empty);
            }
            else
            {
                filter.Search = filter.Search.TrimWithNull();
                var userListItemViewModel = mapper.Map<List<UserWithDeletedInfoModel>>(appConfiguration.UserList);

                var result = await GetListSearchByOneLineAsync( filter, userListItemViewModel);
                

                var model = new PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>()
                {
                    ValueList = result.ValueList,
                    OrderSetting = result.OrderSetting,
                    TotalCount = result.TotalCount
                };

                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            
        }


        public async Task<OperationResultModel<UserWithDeletedInfoModel>> GetByIdAsync(Guid userId, bool isFromDb = false)
        {
            if (isFromDb)
            {
                var result = await userService.GetByIdDetailsAsync(userId);
                if (result.IsSuccess)
                {
                    var modelRes = mapper.Map<UserWithDeletedInfoModel>(result.Entity);

                    return GetOperationResult<UserWithDeletedInfoModel>(modelRes,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<UserWithDeletedInfoModel>(
                        null, result.Code, result.ErrorMessage);
                }
            }
            else
            {
                var userViewModel = await appConfiguration.GetUserByIdAsync(userId);

                if (userViewModel != null)
                {
                    var responseModel = mapper.Map<UserWithDeletedInfoModel>(userViewModel);

                    return GetOperationResult<UserWithDeletedInfoModel>(responseModel,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<UserWithDeletedInfoModel>(
                        null, ResultCode.LineIsNotSearch, "Не найдено");
                }
            }
        }

        public async Task<OperationResultModel<UserCurrentInfoModel>> GetInfoCurrentUser(Guid sessionId, bool isFromDb = false)
        {
            if (isFromDb)
            {
                var result = await userService.GetUserIdBySessionIdAsync(sessionId);

                if (!result.IsSuccess)
                {
                    return GetOperationResult<UserCurrentInfoModel>(
                        null, result.Code, result.ErrorMessage);
                }

                var responseModel = await GetByIdAsync(result.Entity.Id, isFromDb);
                var model = mapper.Map<UserCurrentInfoModel>(responseModel);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                var userViewModel = await appConfiguration.GetUserBySessionIdAsync(sessionId);

                if (userViewModel != null)
                {
                    var responseModel = mapper.Map<UserCurrentInfoModel>(userViewModel);
                    
                    return GetOperationResult<UserCurrentInfoModel>(responseModel,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<UserCurrentInfoModel>(
                        null, ResultCode.LineIsNotSearch, "Не найдено");
                }
            }
        }

        public async Task<OperationResultModel<AuthenticateResponseModel>> LoginAsync(LoginModel loginModel, bool isFromDb = false)
        {
            var loginDto = mapper.Map<LoginDto>(loginModel);
            var jwtDto = mapper.Map<JwtDto>(jwtSettingsModel.Value); 

            var result = await userService.LoginAsync(loginDto, jwtDto);

            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    var session = mapper.Map<SessionWithTokenInfoModel>(result.Entity);

                    await appConfiguration.LoginAsync(session);

                }

                var responseModel = mapper.Map<AuthenticateResponseModel>(result.Entity);

                return GetOperationResult<AuthenticateResponseModel>(responseModel,
                    ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<AuthenticateResponseModel>(null,
                    result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<bool>> LogoutAsync(Guid sessionId, bool isFromDb = false)
        {
            var userIdResult = await GetUerIdAsync(sessionId, isFromDb, userService);
            if (!userIdResult.IsSuccess)
            {
                return GetOperationResult<bool>(false, userIdResult.Code, userIdResult.Message);
            }
            Guid userId = userIdResult.Result;
            
            var result = await userService.LogoutAsync(userId);

            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    await appConfiguration.LogoutAsync(sessionId);
                }

                return GetOperationResult<bool>(true, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<bool>(false, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<PageInfoListModel<RoleInfoModel, OrderRoleFields>>>
            GetRoleExcludeUserRoleAsync(RoleFilter filter, bool isFromDb = false)
        {
            filter.OrderSetting = DefaultOrder(filter.OrderSetting);
            

            if (!isFromDb)
            {
                var orderSet = mapper.Map<RoleFilterDto>(filter);

                var result = await userService.GetRoleExcludeUserRoleAsync(orderSet);
                
                if (result.IsSuccess)
                {
                    var modelRes = mapper.Map<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(result.Entity);

                    return GetOperationResult<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(modelRes,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(
                        new PageInfoListModel<RoleInfoModel, OrderRoleFields>() { OrderSetting = filter.OrderSetting },
                        result.Code, result.ErrorMessage);
                }
            }
            else
            {

                var roleIdList = appConfiguration.UserRoleList.Where(u => u.UserId == filter.UserId)
                    .Select(u => u.RoleId).ToList();

                var roleList = appConfiguration.RoleList;

                if (roleIdList.Count > 0)
                {
                    var roleAllIdList = appConfiguration.RoleList.Select(r => r.Id).ToList();

                    var excludeRoleIdList = roleAllIdList.Except(roleIdList).ToList();

                    roleList = roleList.Where(r => excludeRoleIdList.Any(e => e == r.Id)).ToList();
                }

                var roleInfoModelList = mapper.Map<List<RoleInfoModel>>(roleList);

                return GetOperationResult<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(
                    new PageInfoListModel<RoleInfoModel, OrderRoleFields>()
                    {
                        ValueList = roleInfoModelList,
                        OrderSetting = filter.OrderSetting,
                        TotalCount = roleInfoModelList.Count,
                    },
                    ResultCode.Success, string.Empty);
            }
        }

        public async Task<OperationResultModel<bool>> RoleConfirmAsync(LoginConfirmRoleModel confirmRoleModel, bool isFromDb = false)
        {
            var loginConfirmRoleDto = mapper.Map<LoginConfirmRoleDto>(confirmRoleModel);
            
            var result = await userService.RoleSelectAsync(loginConfirmRoleDto);

            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    await appConfiguration.RoleConfirmAsync(result.Entity.Id, result.Entity.RoleId);
                }

                return GetOperationResult<bool>(true, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<bool>(false, result.Code, result.ErrorMessage);
            }
        }
        
        public async Task<OperationResultModel<UserWithDeletedInfoModel>> RegistrationByAdminAsync(RegistrationByAdminViewModel registrationModel, bool isFromDb = false)
        {
            var registrationDto = mapper.Map<RegistrationDto>(registrationModel);
            var result = await userService.RegistrationByAdminAsync(registrationDto);

            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRoleAndUser();
                }

                var model = mapper.Map<UserWithDeletedInfoModel>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UserWithDeletedInfoModel>(null, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UserWithDeletedInfoModel>> EditAsync(UserEditModel editModel, bool isFromDb = false)
        {
            var userEditDto = mapper.Map<UserEditDto>(editModel);
            var result = await userService.EditAsync(userEditDto);

            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRoleAndUser();
                }

                var model = mapper.Map<UserWithDeletedInfoModel>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UserWithDeletedInfoModel>(null, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UserWithDeletedInfoModel>> EditCurrentAsync(CurrentUserEditModel userEditModel, bool isFromDb = false)
        {
            var userIdResult = await GetUerIdAsync(userEditModel.SessionId, isFromDb, userService);
            if (!userIdResult.IsSuccess)
            {
                return GetOperationResult<UserWithDeletedInfoModel>(null, userIdResult.Code, userIdResult.Message);
            }
            userEditModel.Id = userIdResult.Result;
            
            var userEditDto = mapper.Map<UserEditDto>(userEditModel);
            
            var result = await userService.EditAsync(userEditDto);
            
            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRoleAndUser();
                }

                var model = mapper.Map<UserWithDeletedInfoModel>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UserWithDeletedInfoModel>(null, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<bool>> ChangePasswordAsync(UserChangePasswordModel changePasswordModel)
        {
            var changePasswordDto = mapper.Map<UserChangePasswordDto>(changePasswordModel);

            var result = await userService.ChangePasswordAsync(changePasswordDto);

            if (result.IsSuccess)
            {
                return GetOperationResult<bool>(true, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<bool>(false, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<bool>> ChangePasswordCurrentAsync(
            CurrentUserChangePasswordModel currentChangePasswordModel, bool isFromDb = false)
        {
            var userIdResult = await GetUerIdAsync(currentChangePasswordModel.SessionId, isFromDb, userService);
            if (!userIdResult.IsSuccess)
            {
                return GetOperationResult<bool>(false, userIdResult.Code, userIdResult.Message);
            }
            currentChangePasswordModel.Id = userIdResult.Result;
            
            UserChangePasswordDto userChangePasswordDto = mapper.Map<UserChangePasswordDto>(currentChangePasswordModel);

            var result = await userService.ChangePasswordAsync(userChangePasswordDto);

            if (result.IsSuccess)
            {
                return GetOperationResult<bool>(true, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<bool>(false, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UserWithDeletedInfoModel>> DeleteAsync(Guid userId, bool isFromDb = false)
        {
            var result = await userService.DeleteItemAsync(userId);

            if (result.IsSuccess)
            {
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRoleAndUser();
                }

                var model = mapper.Map<UserWithDeletedInfoModel>(result.Entity);
                return GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UserWithDeletedInfoModel>(null, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UserRoleInfoModel>> AddRoleForUserAsync(AddRoleForUserModel addRoleForUserModel, bool isFromDb = false)
        {
            var userRoleDto = mapper.Map<UserRoleDto>(addRoleForUserModel);

            if (!isFromDb)
            {
                var userRole = appConfiguration.UserRoleList.FirstOrDefault(x =>
                    x.RoleId == addRoleForUserModel.RoleId && x.UserId == addRoleForUserModel.UserId);
                if (userRole != null)
                {
                    var resultUserRole = await DeleteRoleForUserAsync(userRole.Id, isFromDb);
                    return resultUserRole;
                }

            }
            var result = await userRoleService.CreateAsync(userRoleDto);
            
            if (result.IsSuccess)
            {
                
                var userRoleModel = mapper.Map<UserRoleInfoModel>(result.Entity);
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRoleAndUser();
                }

                return GetOperationResult(userRoleModel, ResultCode.Success, string.Empty);
            }
            else
            {
                
            }
            return GetOperationResult<UserRoleInfoModel>(null, result.Code, result.ErrorMessage);
        }

        public async Task<OperationResultModel<UserRoleInfoModel>> DeleteRoleForUserAsync(Guid userRoleId, bool isFromDb = false)
        {
            await sessionService.DeleteByUserRoleAsync(userRoleId);
            var result = await userRoleService.DeleteItemAsync(userRoleId);

            if (result.IsSuccess)
            {
                var userRoleInfoModel = mapper.Map<UserRoleInfoModel>(result.Entity);
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRoleAndUser();
                }

                return GetOperationResult(userRoleInfoModel, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<UserRoleInfoModel>(null, result.Code, result.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<UserCaptionModel>> GetUserCaptionAsync(Guid sessionId, bool isFromDb = false)
        {
            var userCaptionModel = new UserCaptionModel();

            if (isFromDb)
            {
                var result = await userService.GetUserCaptionAsync(sessionId);

                if (!result.IsSuccess)
                {
                    return GetOperationResult<UserCaptionModel>(null, result.Code, result.ErrorMessage);
                }
                userCaptionModel = mapper.Map<UserCaptionModel>(result.Entity);
            }
            else
            {
                var userModel = await appConfiguration.GetUserBySessionIdAsync(sessionId);

                if (userModel != null)
                {
                    userCaptionModel = mapper.Map<UserCaptionModel>(userModel);
                }
            }
            return GetOperationResult(userCaptionModel, ResultCode.Success, string.Empty);
        }

        public async Task<List<EnumInfo>> GetInfoOrderRoleAsync()
        {
            var retList = await GetInfoOfEnumAsync<OrderRoleFields>();

            return retList;
        }
        
        protected override IQueryable<UserWithDeletedInfoModel> OrderSort(IQueryable<UserWithDeletedInfoModel> query, PagingOrderSettingModel<OrderUserFields> pagingOrderSetting)
        {

            if (!pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderUserFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUserFields.SurName:
                    {
                        query = query.OrderBy(x => x.LastName);
                        break;
                    }
                    case OrderUserFields.Email:
                    {
                        query = query.OrderBy(x => x.Email);
                        break;
                    }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUserFields.SurName:
                    {
                        query = query.OrderByDescending(x => x.LastName);
                        break;
                    }
                    case OrderUserFields.Email:
                    {
                        query = query.OrderByDescending(x => x.Email);
                        break;
                    }
                }
            }

            return query;
        }

        protected override IQueryable<UserWithDeletedInfoModel> OrderSearchByOneLine(IQueryable<UserWithDeletedInfoModel> query, SearchByOneLineFilter<OrderUserFields> pagingOrderSetting)
        {
            query = query.Where(x => (x.Phone != null && x.Phone.ToLower().Contains(pagingOrderSetting.Search.ToLower())) ||
                                        x.FirstName.ToLower().Contains(pagingOrderSetting.Search.ToLower()) ||
                                        x.LastName.ToLower().Contains(pagingOrderSetting.Search.ToLower()));
            query = OrderSort(query, (PagingOrderSettingModel<OrderUserFields>)pagingOrderSetting.OrderSetting);

            return query;
        }
    }
}
