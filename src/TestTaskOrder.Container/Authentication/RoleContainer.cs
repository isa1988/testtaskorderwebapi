﻿using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Contract.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Container.Authentication
{
    public class RoleContainer : GeneralContainer<RoleWithDeletedInfoModel, OrderRoleFields>, IRoleContainer
    {
        public RoleContainer(IMapper mapper, 
            IAppConfiguration appConfiguration,
            IRoleService roleService,
            IUserService userService,
            IHelperReadToAppConfigContainer helperReadToAppConfig)
            : base(appConfiguration)
        {
            this.mapper = mapper;
            this.roleService = roleService;
            this.userService = userService;
            this.helperReadToAppConfig = helperReadToAppConfig;
        }

        private readonly IMapper mapper;
        private readonly IRoleService roleService;
        private readonly IUserService userService;
        private readonly IHelperReadToAppConfigContainer helperReadToAppConfig;

        public async Task<OperationResultModel<PageInfoListModel<RoleInfoModel, OrderRoleFields>>> GetAllAsync(
            PagingOrderSettingModel<OrderRoleFields> orderSettingModel, bool isFromDb = false)
        {
            orderSettingModel = DefaultOrder(orderSettingModel);

            if (isFromDb)
            {
                var orderSet = mapper.Map<PagingOrderSettingDto<OrderRoleFields>>(orderSettingModel);
                var result = await roleService.GetAllDetailsAsync(orderSet);
                if (result.IsSuccess)
                {
                    var modelRes = mapper.Map<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(result.Entity);

                    return GetOperationResult<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(modelRes,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(
                        new PageInfoListModel<RoleInfoModel, OrderRoleFields>() { OrderSetting = orderSettingModel },
                        result.Code, result.ErrorMessage);
                }
            }
            else
            {
                var roleViewModels = appConfiguration.RoleList;

                var result = await GetListOfTypeAsync(orderSettingModel, roleViewModels);

                var roleInfoModel = mapper.Map<List<RoleInfoModel>>(result.ValueList);

                return GetOperationResult<PageInfoListModel<RoleInfoModel, OrderRoleFields>>(new PageInfoListModel<RoleInfoModel, OrderRoleFields>
                    {
                        ValueList = roleInfoModel,
                        TotalCount = roleViewModels.Count,
                        OrderSetting = orderSettingModel,
                    },
                    ResultCode.Success, string.Empty);
            }
        }

        public async Task<OperationResultModel<PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>>>
            GetAllWithDeletedAsync(
                PagingOrderSettingModel<OrderRoleFields> orderSettingModel, bool isFromDb = false)
        {
            orderSettingModel = DefaultOrder(orderSettingModel);

            if (isFromDb)
            {
                var orderSet = mapper.Map<PagingOrderSettingDto<OrderRoleFields>>(orderSettingModel);
                var result = await roleService.GetAllDetailsAsync(orderSet);
                if (result.IsSuccess)
                {
                    var modelRes = mapper.Map<PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>>(result.Entity);

                    return GetOperationResult<PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>>(modelRes,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>>(
                        new PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>() { OrderSetting = orderSettingModel },
                        result.Code, result.ErrorMessage);
                }
            }
            else
            {
                var roleViewModels = appConfiguration.RoleList;

                var result = await GetListOfTypeAsync(orderSettingModel, roleViewModels);
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
        }

        public async Task<OperationResultModel<RoleWithDeletedInfoModel>> GetIdAsync(Guid id, bool isFromDb = false)
        {
            if (isFromDb)
            {
                var result = await roleService.GetByIdDetailsAsync(id);

                if (result.IsSuccess)
                {
                    var modelRes = mapper.Map<RoleWithDeletedInfoModel>(result.Entity);

                    return GetOperationResult<RoleWithDeletedInfoModel>(modelRes,
                        ResultCode.Success, string.Empty);
                }
                else
                {
                    return GetOperationResult<RoleWithDeletedInfoModel>(
                        null,
                        result.Code, result.ErrorMessage);
                }
            }
            else
            {
                var result = appConfiguration.RoleList.FirstOrDefault(x => x.Id == id);
                return GetOperationResult<RoleWithDeletedInfoModel>(result,
                    ResultCode.Success, string.Empty);
            }
        }

        public async Task<OperationResultModel<RoleWithDeletedInfoModel>> CreateAsync(RoleAddModel request, bool isFromDb = false)
        {
            var dto = mapper.Map<RoleDto>(request);

            var response = await roleService.CreateAsync(dto);
            if (response.IsSuccess)
            {
                var roleModel = mapper.Map<RoleWithDeletedInfoModel>(response.Entity);
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRole();
                }
                return GetOperationResult<RoleWithDeletedInfoModel>(roleModel, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<RoleWithDeletedInfoModel>(null,
                    response.Code, response.ErrorMessage);
            }
        }

        public async Task<OperationResultModel<RoleWithDeletedInfoModel>> EditAsync(RoleEditModel request, bool isFromDb = false)
        {
            var dto = mapper.Map<RoleDto>(request);

            var response = await roleService.EditAsync(dto);
            if (response.IsSuccess)
            {
                var roleModel = mapper.Map<RoleWithDeletedInfoModel>(response.Entity);
                if (!isFromDb)
                {
                    await helperReadToAppConfig.UpdateRole();
                }
                return GetOperationResult<RoleWithDeletedInfoModel>(roleModel, ResultCode.Success, string.Empty);
            }
            else
            {
                return GetOperationResult<RoleWithDeletedInfoModel>(null,
                    response.Code, response.ErrorMessage);
            }
        }

        protected override IQueryable<RoleWithDeletedInfoModel> OrderSort(IQueryable<RoleWithDeletedInfoModel> query, PagingOrderSettingModel<OrderRoleFields> pagingOrderSetting)
        {

            if (!pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderRoleFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderRoleFields.Name:
                    {
                        query = query.OrderBy(x => x.Name);
                        break;
                    }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderRoleFields.Name:
                    {
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    }
                }
            }

            return query;
        }
    }
}
