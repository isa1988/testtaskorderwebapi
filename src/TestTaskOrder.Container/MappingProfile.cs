﻿
using AutoMapper;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.Filters.Authentication;
using TestTaskOrder.ContainerContract.Filters.Order;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Session;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.ContainerContract.ViewModel.Order.Order;
using TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem;
using TestTaskOrder.ContainerContract.ViewModel.Order.Unit;
using TestTaskOrder.ContainerContract.ViewModel.Provider;
using TestTaskOrder.ContainerContract.ViewModel.Setting;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.ManagerContract.Dto.Provider;
using TestTaskOrder.ManagerContract.Dto.Setting;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.ManagerContract.Filters.Authentication;
using TestTaskOrder.ManagerContract.Filters.Order;

namespace TestTaskOrder.Container
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            GeneralMapping();
            UserMapping();
            RoleMapping();
            SessionMapping();
            OrderMapping();
            UnitMapping();
            ProviderMapping();
        }

        private void ProviderMapping()
        {
            CreateMap<ProviderDto, ProviderWithDeletedInfoModel>();
            CreateMap<UnitDto, ProviderInfoModel>();
            CreateMap<ProviderDto, ProviderInfoModel>();
            CreateMap<ProviderAddModel, ProviderDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.OrderList, p => p.Ignore());
            CreateMap<ProviderEditModel, ProviderDto>()
                .ForMember(x => x.OrderList, p => p.Ignore());
        }

        private void UnitMapping()
        {
            CreateMap<UnitDto, UnitWithDeletedInfoModel>();
            CreateMap<UnitDto, UnitInfoModel>();
            CreateMap<UnitDto, UnitInfoModel>();
            CreateMap<UnitAddModel, UnitDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.OrderItemList, p => p.Ignore());
            CreateMap<UnitEditModel, UnitDto>()
                .ForMember(x => x.OrderItemList, p => p.Ignore());
        }

        private void OrderMapping()
        {
            CreateMap<OrderDto, OrderWithDeletedInfoModel>();
            CreateMap<OrderDto, OrderInfoModel>();
            CreateMap<OrderDto, OrderInfoModel>();
            CreateMap<OrderAddModel, OrderDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.Provider, p => p.Ignore())
                .ForMember(x => x.Author, p => p.Ignore());
            CreateMap<OrderEditModel, OrderDto>()
                .ForMember(x => x.Provider, p => p.Ignore())
                .ForMember(x => x.Author, p => p.Ignore());

            CreateMap<OrderItemDto, OrderItemInfoModel>();
            CreateMap<OrderItemAddModel, OrderItemDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.Unit, p => p.Ignore())
                .ForMember(x => x.Order, p => p.Ignore());
            CreateMap<OrderItemEditModel, OrderItemDto>()
                .ForMember(x => x.Unit, p => p.Ignore())
                .ForMember(x => x.Order, p => p.Ignore());

            CreateMap<OrderFilterModel, OrderFilterDto>();
            CreateMap<OrderItemFilterModel, OrderItemFilterDto>();
        }

        private void SessionMapping()
        {
            CreateMap<SessionDto, SessionInfoModel>();
            CreateMap<SessionInfoModel, SessionDto>();
            CreateMap<SessionDto, SessionWithTokenInfoModel>();

            CreateMap<SessionWithTokenInfoModel, SessionInfoModel>();
            CreateMap<AuthenticateResponseDto, SessionWithTokenInfoModel>()
                .ForMember(x => x.Id, p => p.MapFrom(m => m.SessionId));
        }

        private void RoleMapping()
        {
            CreateMap<RoleDto, RoleWithDeletedInfoModel>();
            CreateMap<RoleInfoModel, RoleDto>();
            CreateMap<RoleDto, RoleInfoModel>();
            CreateMap<RoleWithDeletedInfoModel, RoleInfoModel>();
            

            CreateMap<RoleDto, RoleCaptionModel>()
                .ForMember(x => x.Title,
                    p => p.MapFrom(m => m.Name));

            CreateMap<UserRoleDto, UserRoleInfoModel>();

            CreateMap<RoleAddModel, RoleDto>()
                .ForMember(x => x.IsDeleted, p => p.Ignore())
                .ForMember(x => x.Id, p => p.Ignore());

            CreateMap<RoleEditModel, RoleDto>()
                .ForMember(x => x.IsDeleted, p => p.Ignore());
        }

        private void UserMapping()
        {
            CreateMap<JwtModel, JwtDto>();
            CreateMap<CurrentUserEditModel, UserEditDto>();
            CreateMap<UserEditModel, UserEditDto>();

            CreateMap<AddRoleForUserModel, UserRoleDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.User, p => p.Ignore())
                .ForMember(x => x.Role, p => p.Ignore());

            CreateMap<CurrentUserChangePasswordModel, UserChangePasswordDto>()
                .ForMember(x => x.IsCheck, p => p.MapFrom(n => true));
            CreateMap<UserChangePasswordModel, UserChangePasswordDto>()
                .ForMember(x => x.PasswordCurrent, p => p.Ignore())
                .ForMember(x => x.IsCheck, p => p.MapFrom(n => false));

            CreateMap<UserCaptionDto, UserCaptionModel>();

            CreateMap<UserDto, UserCaptionModel>()
                .ForMember(x => x.Title,
                    p => p.MapFrom(m => m.LastName + " " + m.FirstName + " " + m.MiddleName));

            CreateMap<UserInfoModel, UserWithDeletedInfoModel>();
            CreateMap<UserInfoModel, UserCurrentInfoModel>();

            CreateMap<UserInfoModel, UserCaptionModel>()
                .ForMember(x => x.Title,
                    p => p.MapFrom(m => m.LastName + " " + m.FirstName + " " + m.MiddleName));

            CreateMap<UserDto, UserWithDeletedInfoModel>();
            CreateMap<UserDto, UserInfoModel>();
            CreateMap<UserInfoModel, UserDto>();

            CreateMap<AuthenticateResponseDto, AuthenticateResponseModel>();
            CreateMap<LoginModel, LoginDto>();
            CreateMap<LoginConfirmRoleModel, LoginConfirmRoleDto>();


            CreateMap<RoleFilter, RoleFilterDto>();
            CreateMap<RegistrationByAdminViewModel, RegistrationDto>();
                           
        }

        private void GeneralMapping()
        {
            CreateMap(typeof(SearchByOneLineFilter<>), typeof(SearchByOneLineDtoFilter<>));
            CreateMap(typeof(PageInfoListDto<,>), typeof(PageInfoListModel<,>));
            CreateMap(typeof(PeriodFilterModel<>), typeof(PeriodFilterDto<>));

            CreateMap(typeof(PagingOrderSettingModel<>), typeof(PagingOrderSettingDto<>));
            CreateMap(typeof(PagingOrderSettingDto<>), typeof(PagingOrderSettingModel<>));
        }

    }
}
