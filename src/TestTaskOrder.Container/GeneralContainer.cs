﻿using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.Helper;

namespace TestTaskOrder.Container
{
    public abstract class GeneralContainer<T, TOrder> : IGeneralContainer 
        where T : class 
        where TOrder : Enum
    {
        public GeneralContainer(IAppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        protected readonly IAppConfiguration appConfiguration;

        protected OperationResultModel<T> GetOperationResult<T>(T model, ResultCode code, string error)
        {
            return new OperationResultModel<T>
            {
                Result = model,
                Code = code,
                Message = error,
            };
        }

        protected PagingOrderSettingModel<TEnum> DefaultOrder<TEnum>(PagingOrderSettingModel<TEnum> orderSettingModel)
            where TEnum : Enum
        {
            if (orderSettingModel == null)
            {
                return new PagingOrderSettingModel<TEnum>
                {
                    StartPosition = 0,
                    PageSize = appConfiguration.PageSize
                };
            }
            else if (orderSettingModel.PageSize == 0)
            {
                orderSettingModel.PageSize = appConfiguration.PageSize;
                return orderSettingModel;
            }
            else
            {
                return orderSettingModel;
            }
        }

        protected async Task<List<EnumInfo>> GetInfoOfEnumAsync<TEnum>()
            where TEnum : Enum
        {
            var values = Enum.GetValues(typeof(TEnum));
            var retList = new List<EnumInfo>();
            foreach (var value in values)
            {
                retList.Add(new EnumInfo
                {
                    Title = ((TEnum)value).GetEnumDisplayName(),
                    Code = (int)value,
                });
            }

            return retList;
        }

        protected async Task<PageInfoListModel<T, TOrder>> GetListOfTypeAsync(PagingOrderSettingModel<TOrder> orderSettingModel, List<T> list)
        {
            IQueryable<T> query = list.AsQueryable();

            List<T> entities = OrderSort(query, orderSettingModel).Skip(orderSettingModel.StartPosition).Take(orderSettingModel.PageSize).ToList();

            var result = new PageInfoListModel<T, TOrder>
            {
                ValueList = entities,
                OrderSetting = orderSettingModel,
                TotalCount = list.Count
            };

            return result;
        }

        protected async Task<PageInfoListModel<T, TOrder>> GetListSearchByOneLineAsync(SearchByOneLineFilter<TOrder> orderSettingModel, List<T> list)
        {
            IQueryable<T> query = list.AsQueryable();

            var entityCount = OrderSearchByOneLine(query, orderSettingModel).Count();

            List<T> entities = OrderSearchByOneLine(query, orderSettingModel)
                .Skip(orderSettingModel.OrderSetting.StartPosition).Take(orderSettingModel.OrderSetting.PageSize)
                .ToList();

            var result = new PageInfoListModel<T, TOrder>
            {
                ValueList = entities,
                OrderSetting = orderSettingModel.OrderSetting,
                TotalCount = entityCount
            };

            return result;
        }

        protected virtual async Task<OperationResultModel<Guid>> GetUerIdAsync(Guid sessionId, bool isFromDb,
            IUserService userService)
        {
            try
            {

                if (isFromDb)
                {
                    var userResult = await userService.GetUserIdBySessionIdAsync(sessionId);
                    if (!userResult.IsSuccess)
                    {
                        return GetOperationResult<Guid>(Guid.Empty, userResult.Code, userResult.ErrorMessage);
                    }

                    return GetOperationResult(userResult.Entity.Id, ResultCode.Success, string.Empty);
                }
                else
                {
                    var userId = await appConfiguration.GetUserIdBySessionIdAsync(sessionId);
                    return GetOperationResult(userId, ResultCode.Success, string.Empty);
                }
            }
            catch (RawDbNullException ex)
            {
                return GetOperationResult<Guid>(Guid.Empty, ResultCode.LineIsNotSearch, ex.Message);
            }

        }

        protected virtual IQueryable<T> OrderSort(IQueryable<T> query, PagingOrderSettingModel<TOrder> pagingOrderSetting)
        {
            return query;
        }

        protected virtual IQueryable<T> OrderSearchByOneLine(IQueryable<T> query, SearchByOneLineFilter<TOrder> pagingOrderSetting)
        {
            return query;
        }
    }
}
