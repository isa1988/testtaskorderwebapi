﻿using System.ComponentModel;
using System.Text.Json;
using AutoMapper;
using TestTaskOrder.ContainerContract.Contract;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Session;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.ManagerContract.Contract.Authentication;

namespace TestTaskOrder.Container
{
    public class HelperReadToAppConfigContainer : IHelperReadToAppConfigContainer
    {

        public HelperReadToAppConfigContainer(IAppConfiguration appConfiguration,
            IMapper mapper,
            IUserService userService,
            ISessionService sessionService,
            IUserRoleService userRoleService,
            IRoleService roleService)
        {
            this.appConfiguration = appConfiguration;
            this.mapper = mapper;
            this.userService = userService;
            this.sessionService = sessionService;
            this.userRoleService = userRoleService;
            this.roleService = roleService;
        }

        private readonly IAppConfiguration appConfiguration;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly ISessionService sessionService;
        private readonly IUserRoleService userRoleService;
        private readonly IRoleService roleService;
        public async Task LoadAsync()
        {
            await LoadAuth();
        }
        

        public async Task UpdateRoleAndUser()
        {
            var userResult = await userService.GetAllDetailsAsync(null);
            if (userResult.IsSuccess)
            {
                appConfiguration.UserList = mapper.Map<List<UserInfoModel>>(userResult.Entity.ValueList);

                var userIdList = userResult.Entity.ValueList.Select(u => u.Id).ToList();


                var sessionResult = await sessionService.GetActiveByUserListAsync(userIdList);
                if (sessionResult.IsSuccess)
                {
                    appConfiguration.SessionList =
                        mapper.Map<List<SessionWithTokenInfoModel>>(sessionResult.Entity.ValueList);
                    foreach (var session in appConfiguration.SessionList)
                    {
                        if (session.User == null)
                        {
                            session.User = appConfiguration.UserList.FirstOrDefault(x => x.Id == session.UserId);
                        }
                    }
                }
            }

            var userRoleResult = await userRoleService.GetAllDetailsAsync(null);
            if (userRoleResult.IsSuccess)
            {
                appConfiguration.UserRoleList = mapper.Map<List<UserRoleInfoModel>>(userRoleResult.Entity.ValueList);
            }
            
        }

        public async Task UpdateRole()
        {
            var roleResult = await roleService.GetAllWithDeletedAsync(null);
            if (roleResult.IsSuccess)
            {
                appConfiguration.RoleList = mapper.Map<List<RoleWithDeletedInfoModel>>(roleResult.Entity.ValueList);
            }
        }
        

        private async Task LoadAuth()
        {
            await UpdateRoleAndUser();
            await UpdateRole();
        }
    }
}
