﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestTaskOrder.Core.Contract;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Contract.Provider;
using TestTaskOrder.Core.DataBaseInitializer;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Entities.AreaProvider;
using TestTaskOrder.Core.Entities.Authentication;

namespace TestTaskOrder.DAL.Data
{
    public class DataBaseInitializerDeveloper : IDataBaseInitializer
    {
        public DataBaseInitializerDeveloper(IServiceProvider serviceProvider, 
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            IUserRoleRepository userRoleRepository,
            IOrderRepository orderRepository,
            IOrderItemRepository orderItemRepository,
            IUnitRepository unitRepository,
            IProviderRepository providerRepository)
        {
            this.serviceProvider = serviceProvider;
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.userRoleRepository = userRoleRepository;
            this.orderRepository = orderRepository;
            this.orderItemRepository = orderItemRepository;
            this.unitRepository = unitRepository;
            this.providerRepository = providerRepository;
        }
        private readonly IServiceProvider serviceProvider;
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;
        private readonly IUserRoleRepository userRoleRepository;
        private readonly IOrderRepository orderRepository;
        private readonly IOrderItemRepository orderItemRepository;
        private readonly IUnitRepository unitRepository;
        private readonly IProviderRepository providerRepository;

        public async Task InitializeForDeveloperAsync()
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<TestTaskOrderContext>();
                await context.Database.MigrateAsync();

                var isEquals = await roleRepository.IsEqualsNameAsync("Administrator", "Administrator");
                if (isEquals)
                    return;
                var roleList = await RoleAddAsync();
                var userList = await UserAddDBAsync();
                await UserRoleAddDBAsync(userList, roleList);

                var unitList = await UnitAddDBAsync();
                var providerList = await ProviderAddDBAsync();
                var oderList = await OrderAddDBAsync(providerList, userList);
                var oderItemList = await OrderItemAddDBAsync(oderList, unitList);
            }
        }
        

        private async Task<List<Role>> RoleAddAsync()
        {
            var rolList = new List<Role>();
            rolList.Add(new Role {SysName = "Administrator", Name = "Администратор"});
            rolList.Add(new Role {SysName = "User", Name = "Пользователь"});
            
            await SaveOperationAsync(rolList, roleRepository);

            return rolList;
        }

        private async Task<List<User>> UserAddDBAsync()
        {
            var userList = new List<User>();
            userList.Add(new User { FirstName = "Админ", LastName = "Админов", MiddleName = "", Login = "admin", Email = "user1@mail.ru", Phone = "1", Password = "12345" });
            
            await SaveOperationAsync(userList, userRepository);

            return userList;
        }

        private async Task<List<UserRole>> UserRoleAddDBAsync(List<User> userList, List<Role> rolelist)
        {
            var userRoleList = new List<UserRole>();
            userRoleList.Add(new UserRole { UserId = userList[0].Id, RoleId = rolelist[0].Id });
            userRoleList.Add(new UserRole { UserId = userList[0].Id, RoleId = rolelist[1].Id });

            await SaveOperationAsync(userRoleList, userRoleRepository);

            return userRoleList;
        }

        private async Task<List<Provider>> ProviderAddDBAsync()
        {
            var providerList = new List<Provider>();
            providerList.Add(new Provider { Name = "Первый поставщик", Comment = "ул" });
            providerList.Add(new Provider { Name = "Второй поставщик", Comment = "дом" });

            await SaveOperationAsync(providerList, providerRepository);

            return providerList;
        }

        private async Task<List<Unit>> UnitAddDBAsync()
        {
            var unitList = new List<Unit>();
            unitList.Add(new Unit { Name = "шт", });
            unitList.Add(new Unit { Name = "кг", });

            await SaveOperationAsync(unitList, unitRepository);

            return unitList;
        }

        private async Task<List<Order>> OrderAddDBAsync(List<Provider> providerList, List<User> userList)
        {
            var orderList = new List<Order>();
            orderList.Add(new Order
            {
                Name = "Первый заказ", Date = DateTime.Now, AuthorId = userList[0].Id, ProviderId = providerList[0].Id
            });
            orderList.Add(new Order
            {
                Name = "Второй заказ", Date = DateTime.Now, AuthorId = userList[0].Id, ProviderId = providerList[1].Id
            });

            await SaveOperationAsync(orderList, orderRepository);

            return orderList;
        }

        private async Task<List<OrderItem>> OrderItemAddDBAsync(List<Order> orderList, List<Unit> unitList)
        {
            var orderItemList = new List<OrderItem>();
            orderItemList.Add(new OrderItem
                { Name = "Первый товар", OrderId = orderList[0].Id, UnitId = unitList[0].Id, Quantity = 15.256m });
            orderItemList.Add(new OrderItem
                { Name = "Второй товар", OrderId = orderList[0].Id, UnitId = unitList[1].Id, Quantity = 17.256m });
            orderItemList.Add(new OrderItem
                { Name = "Первый товар", OrderId = orderList[1].Id, UnitId = unitList[0].Id, Quantity = 25.267m });
            orderItemList.Add(new OrderItem
                { Name = "Второй товар", OrderId = orderList[1].Id, UnitId = unitList[1].Id, Quantity = 19.246m });

            await SaveOperationAsync(orderItemList, orderItemRepository);

            return orderItemList;
        }

        private async Task SaveOperationAsync<TEntity, TOrder>(List<TEntity> entities, IRepository<TEntity, TOrder> repository)
            where TEntity : class, IEntity
            where TOrder: Enum
        {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i] = await repository.AddAsync(entities[i]);
            }

            await repository.SaveAsync();
        }

        private async Task SaveOperationAsync<TEntity, TId, TOrder>(List<TEntity> entities, IRepository<TEntity, TId, TOrder> repository)
            where TEntity : class, IEntity<TId>
            where TId : IEquatable<TId>
            where TOrder : Enum
        {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i] = await repository.AddAsync(entities[i]);
            }

            await repository.SaveAsync();
        }
    }
}
