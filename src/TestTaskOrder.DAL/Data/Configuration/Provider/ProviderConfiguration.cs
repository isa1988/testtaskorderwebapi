﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTaskOrder.DAL.Data.Configuration.Provider
{
    internal class ProviderConfiguration : IEntityTypeConfiguration<Core.Entities.AreaProvider.Provider>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.AreaProvider.Provider> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();

            builder.Property(e => e.Name).IsRequired().HasMaxLength(4000);
        }
    }
}
