﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskOrder.Core.Entities.Authentication;

namespace TestTaskOrder.DAL.Data.Configuration.Authentication
{
    class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();
            builder.HasOne(e => e.Role)
                .WithMany(p => p.UserRoleList)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(e => e.User)
                .WithMany(p => p.RoleList)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
