﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskOrder.Core.Entities.Authentication;

namespace TestTaskOrder.DAL.Data.Configuration.Authentication
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();

            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(256);
            builder.Property(e => e.MiddleName).HasMaxLength(256);
            builder.Property(e => e.LastName).IsRequired().HasMaxLength(256);

            builder.Property(e => e.Login).HasMaxLength(256);
            builder.Property(e => e.Password).IsRequired().HasMaxLength(4000);

            builder.Property(e => e.Email).IsRequired().HasMaxLength(256);
            builder.Property(e => e.Phone).HasMaxLength(100);
        }
    }
}
