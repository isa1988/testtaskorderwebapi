﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskOrder.Core.Entities.Authentication;

namespace TestTaskOrder.DAL.Data.Configuration.Authentication
{
    class SessionConfiguration : IEntityTypeConfiguration<Session>
    {
        public void Configure(EntityTypeBuilder<Session> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();
            builder.Property(e => e.CloseDate);
            builder.Property(e => e.Token).IsRequired().HasMaxLength(4000); 
            builder.Property(e => e.Info).HasMaxLength(4000);
            builder.HasOne(e => e.User)
                .WithMany(p => p.SessionList)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
