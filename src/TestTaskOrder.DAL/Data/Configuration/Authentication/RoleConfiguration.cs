﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskOrder.Core.Entities.Authentication;

namespace TestTaskOrder.DAL.Data.Configuration.Authentication
{
    class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();
            builder.Property(e => e.Name).IsRequired().HasMaxLength(100);
            builder.Property(e => e.SysName).IsRequired().HasMaxLength(100);
            builder.HasIndex(e => e.SysName).IsUnique();
        }
    }
}
