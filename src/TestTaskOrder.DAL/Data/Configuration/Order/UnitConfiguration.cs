﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskOrder.Core.Entities.AreaOrder;

namespace TestTaskOrder.DAL.Data.Configuration.Order
{
    internal class UnitConfiguration : IEntityTypeConfiguration<Unit>
    {
        public void Configure(EntityTypeBuilder<Unit> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();

            builder.Property(e => e.Name).IsRequired().HasMaxLength(4000);
        }
    }
}
