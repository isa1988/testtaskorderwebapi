﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestTaskOrder.Core.Entities.AreaOrder;

namespace TestTaskOrder.DAL.Data.Configuration.Order
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();

            builder.Property(e => e.Name).IsRequired().HasMaxLength(4000);
            builder.Property(e => e.Quantity).HasColumnType("decimal(18,3)");

            builder.HasOne(e => e.Order)
                .WithMany(p => p.OrderItemList)
                .HasForeignKey(d => d.OrderId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(e => e.Unit)
                .WithMany(p => p.OrderItemList)
                .HasForeignKey(d => d.UnitId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
