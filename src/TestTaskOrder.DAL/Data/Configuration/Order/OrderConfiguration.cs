﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestTaskOrder.DAL.Data.Configuration.Order
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Core.Entities.AreaOrder.Order>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.AreaOrder.Order> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");
            builder.Property(e => e.CreateDate).IsRequired()
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.ModifyDate).IsRequired();

            builder.Property(e => e.NumberString)
                .IsRequired()
                .HasMaxLength(256)
                .HasComputedColumnSql("'O' || LPAD(CAST(\"Number\" AS VARCHAR), 10, '0')", stored: true); 
            builder.Property(e => e.Number)
                .HasColumnType("bigint")
                .ValueGeneratedOnAdd();
            builder.Property(e => e.Date).IsRequired();

            builder.Property(e => e.Name).IsRequired().HasMaxLength(4000);

            builder.HasOne(e => e.Author)
                .WithMany(p => p.OrderList)
                .HasForeignKey(d => d.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(e => e.Provider)
                .WithMany(p => p.OrderList)
                .HasForeignKey(d => d.ProviderId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
