﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Entities.AreaProvider;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.DAL.Data.Configuration.Authentication;
using TestTaskOrder.DAL.Data.Configuration.Order;
using TestTaskOrder.DAL.Data.Configuration.Provider;

namespace TestTaskOrder.DAL.Data
{
    public class TestTaskOrderContext : DbContext
    {
        public TestTaskOrderContext(DbContextOptions<TestTaskOrderContext> options)
            : base(options)
        {
        }

        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<User> User { get; set; }

        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<Unit> Unit { get; set; }
        public virtual DbSet<Provider> Provider { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new SessionConfiguration());

            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderItemConfiguration());
            modelBuilder.ApplyConfiguration(new UnitConfiguration());
            modelBuilder.ApplyConfiguration(new ProviderConfiguration());
        }

    }
}
