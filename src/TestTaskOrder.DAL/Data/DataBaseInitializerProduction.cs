﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestTaskOrder.Core.Contract;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.DataBaseInitializer;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Entities.Authentication;

namespace TestTaskOrder.DAL.Data
{
    public class DataBaseInitializerProduction : IDataBaseInitializer
    {
        public DataBaseInitializerProduction(IServiceProvider serviceProvider,
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            IUserRoleRepository userRoleRepository)
        {
            this.serviceProvider = serviceProvider;
            this.roleRepository = roleRepository;
            this.userRepository = userRepository;
            this.userRoleRepository = userRoleRepository;
        }
        private readonly IServiceProvider serviceProvider;
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;
        private readonly IUserRoleRepository userRoleRepository;

        public async Task InitializeForDeveloperAsync()
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<TestTaskOrderContext>();
                await context.Database.MigrateAsync();
                
                var isEquals = await roleRepository.IsEqualsNameAsync("Administrator", "Administrator");
                if (isEquals)
                    return;
                var roleList = await RoleAddAsync();
                var userList = await UserAddDBAsync();
                await UserRoleAddDBAsync(userList, roleList);
            }
        }

        private async Task<List<Role>> RoleAddAsync()
        {
            var rolList = new List<Role>();
            rolList.Add(new Role { SysName = "Administrator", Name = "Администратор" });
            rolList.Add(new Role { SysName = "User", Name = "Пользователь" });
            await SaveOperationAsync(rolList, roleRepository);

            return rolList;
        }

        private async Task<List<User>> UserAddDBAsync()
        {
            var userList = new List<User>();
            userList.Add(new User { FirstName = "Админ", LastName = "Админов", MiddleName = "", Login = "admin", Email = "user1@mail.ru", Phone = "1", Password = "12345" });

            await SaveOperationAsync(userList, userRepository);

            return userList;
        }

        private async Task<List<UserRole>> UserRoleAddDBAsync(List<User> userList, List<Role> rolelist)
        {
            var userRoleList = new List<UserRole>();
            userRoleList.Add(new UserRole { UserId = userList[0].Id, RoleId = rolelist[0].Id });
            userRoleList.Add(new UserRole { UserId = userList[0].Id, RoleId = rolelist[1].Id });

            await SaveOperationAsync(userRoleList, userRoleRepository);

            return userRoleList;
        }

        private async Task SaveOperationAsync<TEntity, TOrder>(List<TEntity> entities, IRepository<TEntity, TOrder> repository)
            where TEntity : class, IEntity
            where TOrder : Enum
        {
            for (int i = 0; i < entities.Count; i++)
            {
                await repository.AddAsync(entities[i]);
            }

            await repository.SaveAsync();
        }

        private async Task SaveOperationAsync<TEntity, TId, TOrder>(List<TEntity> entities, IRepository<TEntity, TId, TOrder> repository)
            where TEntity : class, IEntity<TId>
            where TId : IEquatable<TId>
            where TOrder : Enum
        {
            for (int i = 0; i < entities.Count; i++)
            {
                await repository.AddAsync(entities[i]);
            }

            await repository.SaveAsync();
        }
    }
}
