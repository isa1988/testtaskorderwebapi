﻿using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.DAL.Repository.Authentication
{
    public class SessionRepository : RepositoryGuid<Session, OrderSessionFields>, ISessionRepository
    {
        public SessionRepository(TestTaskOrderContext contextDB) 
            : base(contextDB)
        {
        }

        public async Task<List<Session>> GetActiveByUserListAsync(List<Guid> userListId, ResolveOptions resolveOptions = null)
        {
            var sessionList = await ResolveInclude(resolveOptions)
                .Where(x => userListId.Any(u => u == x.UserId) && x.Status == SessionStatus.Active).ToListAsync();
            return sessionList;
        }
        public async Task<Session> GetActiveByIdAsync(Guid id, ResolveOptions resolveOptions = null)
        {
            var session = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id == id && x.Status == SessionStatus.Active);
            if (session == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(session);
            return session;
        }

        public async Task<Session> GetActiveByTokenAsync(string token, ResolveOptions resolveOptions = null)
        {
            var session = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Token.Trim() == token.Trim() && x.Status == SessionStatus.Active);
            if (session == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(session);
            return session;
        }

        public async Task CloseAllOpenSesionAsync(Guid userId)
        {
            var sessionList = await ResolveInclude(null).Where(x => x.UserId == userId && x.Status == SessionStatus.Active).ToListAsync();
            if (sessionList?.Count > 0)
            {
                foreach (var session in sessionList)
                {
                    session.Status = SessionStatus.Closed;
                    session.CloseDate = DateTime.UtcNow;
                    Update(session);
                }
            }
        }
        
        public async Task<List<Session>> GetAllByUserRoleAsync(Guid userRoleId,
            PagingOrderSetting<OrderSessionFields> pagingOrderSetting,
            ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => x.RoleId == userRoleId);

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }
        
        
        public async Task<Guid> GetUserIdByActiveSession(Guid sessionId)
        {
            var userId = await ResolveInclude(null)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == sessionId && x.Status == SessionStatus.Active);
            if (userId == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            return userId.UserId;
        }

        protected override void ClearDbSetForInclude(Session entity)
        {

        }

        protected override IQueryable<Session> OrderSort(IQueryable<Session> query, PagingOrderSetting<OrderSessionFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderSessionFields.None)
                return query;
            
            return query;
        }

        protected override IQueryable<Session> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Session> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.IsUser)
            {
                query = query.Include(x => x.User);
            }

            if (resolveOptions.IsRole)
            {
                query = query.Include(x => x.UserRole);
            }

            return query;
        }
    }
}
