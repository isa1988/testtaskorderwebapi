﻿using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.DAL.Repository.Authentication
{
    public class RoleRepository : RepositoryForDeletedGuid<Role, OrderRoleFields>, IRoleRepository
    {
        public RoleRepository(TestTaskOrderContext contextDB) : base(contextDB)
        {
        }

        public async Task<Role> GetBySysNameAsync(string sysName)
        {
            var role = await ResolveInclude(null).FirstOrDefaultAsync(x => x.SysName == sysName && x.IsDeleted == Deleted.UnDeleted);
            if (role == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(role);
            return role;
        }

        public async Task<bool> IsEqualsNameAsync(string name, string sysName, Guid? id = null)
        {
            var query = ResolveInclude(null).Where(x => x.Name == name || x.SysName == sysName);
            if (id.HasValue)
            {
                query = query.Where(x => x.Id != id.Value);
            }
            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(Role entity)
        {

        }

        protected override IQueryable<Role> OrderSort(IQueryable<Role> query, PagingOrderSetting<OrderRoleFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderRoleFields.None)
                return query;
            return query;
        }

        protected override IQueryable<Role> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Role> query = dbSetQueryable;
            
            return query;
        }
    }
}
