﻿using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.DAL.Repository.Authentication
{
    public class UserRoleRepository : RepositoryGuid<UserRole, OrderUserRoleFields>, IUserRoleRepository
    {
        public UserRoleRepository(TestTaskOrderContext contextDB) : base(contextDB)
        {
        }

        public async Task<UserRole> GetByRoleForUserAsync(Guid userId, Guid roleId, ResolveOptions resolveOption = null)
        {
            var role = await ResolveInclude(resolveOption).FirstOrDefaultAsync(x => x.UserId == userId && x.RoleId == roleId);
            if (role == null)
                throw new RawDbNullException("Не найдена роль для пользователя");
            return role;
        }

        public async Task<int> GetByUserCountAsync(Guid userId)
        {
            var count = await ResolveInclude(null).Where(x => x.UserId == userId).CountAsync();
            return count;
        }

        public async Task<List<UserRole>> GetByUserListAsync(Guid userId, ResolveOptions resolveOption = null)
        {
            var roleList = await ResolveInclude(resolveOption).Where(x =>x.UserId == userId).ToListAsync();
            return roleList;
        }

        public async Task<List<UserRole>> GetByUserListAsync(Guid userId, PagingOrderSetting<OrderUserRoleFields> pagingOrderSetting, ResolveOptions resolveOption = null)
        {
            var query = ResolveInclude(resolveOption).Where(x => x.UserId == userId);
            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }

        protected override void ClearDbSetForInclude(UserRole entity)
        {

        }

        protected override IQueryable<UserRole> OrderSort(IQueryable<UserRole> query, PagingOrderSetting<OrderUserRoleFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderUserRoleFields.None)
                return query;

            return query;
        }

        protected override IQueryable<UserRole> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<UserRole> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.IsUser)
            {
                query = query.Include(x => x.User);
            }

            if (resolveOptions.IsRole)
            {
                query = query.Include(x => x.Role);
            }

            return query;
        }
    }
}
