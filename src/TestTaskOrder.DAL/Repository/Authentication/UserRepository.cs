﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.DAL.Repository.Authentication
{
    public class UserRepository : RepositoryForDeletedGuid<User, OrderUserFields>, IUserRepository
    {
        public UserRepository(TestTaskOrderContext contextDB)
            : base(contextDB)
        {
        }

        public async Task<List<User>> GetAllFromSearchAsync(string search, PagingOrderSetting<OrderUserFields> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);

            query = query.Where(x => x.IsDeleted == Deleted.UnDeleted);
            query = query.Where(x => x.Phone.Contains(search) || x.FirstName.Contains(search) ||
                                          x.LastName.Contains(search));
            var entities = await GetListAsync(query, pagingOrderSetting);
            
            return entities;
        }

        public async Task<int> GetAllFromSearchCountAsync(string search)
        {
            var query = ResolveInclude(null);

            query = query.Where(x => x.IsDeleted == Deleted.UnDeleted);
            query = query.Where(x => x.Phone.Contains(search) || x.FirstName.Contains(search) ||
                                      x.LastName.Contains(search));


            var count = await query.CountAsync();
            return count;
        }

        public async Task<bool> IsEqualEmailAsync(string email, Guid excludingId = default(Guid))
        {
            var isEqual = false;
            if (excludingId == Guid.Empty)
            {
                isEqual = await dbSetQueryable.AnyAsync(x => x.Email == email);
            }
            else
            {
                isEqual = await dbSetQueryable.AnyAsync(x => x.Email == email && x.Id != excludingId);
            }

            return isEqual;
        }

        public async Task<User> GetUserAsync(string login, string password)
        {
            password = HashPassword(password);
            var entity = await ResolveInclude(null).FirstOrDefaultAsync(x => x.IsDeleted == Deleted.UnDeleted &&
                                                                                         EF.Functions.ILike(x.Login, login) && x.Password == password);
            
            return entity;
        }
        
        public override Task<User> AddAsync(User entity)
        {
            entity.Password = HashPassword(entity.Password);
            return base.AddAsync(entity);
        }

        string HashPassword(string password)
        {
            var sha1 = new MD5CryptoServiceProvider();
            var data = Encoding.ASCII.GetBytes(password);
            var sha1data = sha1.ComputeHash(data);
            
            return Convert.ToBase64String(sha1data);
        }
        
        public string GetEncryptedPassword(string password)
        {
            var retVal = HashPassword(password);
            return retVal;
        }
        
        protected override void ClearDbSetForInclude(User entity)
        {

        }

        protected override IQueryable<User> OrderSort(IQueryable<User> query, PagingOrderSetting<OrderUserFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderUserFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUserFields.SurName:
                    {
                        query = query.OrderBy(x => x.LastName);
                        break;
                    }
                    case OrderUserFields.Email:
                    {
                        query = query.OrderBy(x => x.Email);
                        break;
                    }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUserFields.SurName:
                    {
                        query = query.OrderByDescending(x => x.FirstName);
                        break;
                    }
                    case OrderUserFields.Email:
                    {
                        query = query.OrderByDescending(x => x.Email);
                        break;
                    }
                }
            }

            return query;
        }

        protected override IQueryable<User> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<User> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }
            
            return query;
        }

    }
}
