﻿
using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Filters.Order;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.DAL.Repository.Order
{
    public class OrderRepository : RepositoryForDeletedGuid<Core.Entities.AreaOrder.Order, OrderOrderFields>, IOrderRepository
    {
        public OrderRepository(TestTaskOrderContext contextDB) : base(contextDB)
        {
        }


        public async Task<List<Core.Entities.AreaOrder.Order>> GetAllAsync(OrderFilter filter,
            SettingSelectForDelete settingSelectForDelete,
            PagingOrderSetting<OrderOrderFields> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            if (filter == null)
            {
                var entitiesRet = await GetListAsync(query, null);

                return entitiesRet;
            }

            query = await GetByFilter(filter, query);
            var entities = await GetListAsync(query, pagingOrderSetting);
            ClearDbSetForInclude(entities);
            return entities;

        }

        public async Task<int> GetAllCountAsync(OrderFilter filter, SettingSelectForDelete settingSelectForDelete)
        {
            var query = ResolveInclude(null);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            if (filter == null)
            {
                var entitiesCountRet = await query.CountAsync();

                return entitiesCountRet;
            }

            query = await GetByFilter(filter, query);
            var entitiesCount = await query.CountAsync();

            return entitiesCount;
        }

        private async Task<IQueryable<Core.Entities.AreaOrder.Order>> GetByFilter(OrderFilter filter,
            IQueryable<Core.Entities.AreaOrder.Order> query)
        {

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                query = query.Where(x => EF.Functions.ILike(x.Name, "%" + filter.Name + "%"));
            }

            if (!string.IsNullOrWhiteSpace(filter.Number))
            {
                query = query.Where(x => EF.Functions.ILike(x.NumberString, "%" + filter.Number + "%"));
            }

            if (filter.CreateDate != null)
            {
                if (filter.CreateDate.Start.HasValue)
                {
                    query = query.Where(x => x.CreateDate >= filter.CreateDate.Start.Value);
                }

                if (filter.CreateDate.End.HasValue)
                {
                    query = query.Where(x => x.CreateDate < filter.CreateDate.End.Value);
                }
            }

            if (filter.ModifyDate != null)
            {
                if (filter.ModifyDate.Start.HasValue)
                {
                    query = query.Where(x => x.ModifyDate >= filter.ModifyDate.Start.Value);
                }

                if (filter.ModifyDate.End.HasValue)
                {
                    query = query.Where(x => x.ModifyDate < filter.ModifyDate.End.Value);
                }
            }

            if (filter.Date != null)
            {
                if (filter.Date.Start.HasValue)
                {
                    query = query.Where(x => x.Date >= filter.Date.Start.Value);
                }

                if (filter.Date.End.HasValue)
                {
                    query = query.Where(x => x.ModifyDate < filter.Date.End.Value);
                }
            }

            if (filter.AuthorId.HasValue)
            {
                query = query.Where(x => x.AuthorId == filter.AuthorId.Value);
            }
            if (filter.IsCurrentUser)
            {
                query = query.Where(x => x.AuthorId == filter.CurrentUserId);
            }

            if (filter.ProviderId.HasValue)
            {
                query = query.Where(x => x.ProviderId == filter.ProviderId.Value);
            }

            return query;
        }

        public async Task<bool> EqualNameAsync(string name, Guid providerId, Guid? id = null)
        {
            var query = ResolveInclude(null)
                .Where(x => x.ProviderId == providerId && EF.Functions.ILike(x.Name, name));
            if (id.HasValue)
            {
                query = query.Where(x => x.Id != id.Value);
            }
            var isEqual = await query.AsNoTracking().AnyAsync();

            return isEqual;
        }


        protected override void ClearDbSetForInclude(Core.Entities.AreaOrder.Order entity)
        {
            
        }

        protected override IQueryable<Core.Entities.AreaOrder.Order> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Core.Entities.AreaOrder.Order> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.IsUser)
            {
                query = query.Include(x => x.Author);
            }

            if (resolveOptions.IsProvider)
            {
                query = query.Include(x => x.Provider);
            }

            return query;
        }

        protected override IQueryable<Core.Entities.AreaOrder.Order> OrderSort(
            IQueryable<Core.Entities.AreaOrder.Order> query,
            PagingOrderSetting<OrderOrderFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderOrderFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderOrderFields.CreateDate:
                    {
                        query = query.OrderBy(x => x.CreateDate);
                        break;
                    }
                    case OrderOrderFields.ModifyDate:
                    {
                        query = query.OrderBy(x => x.ModifyDate);
                        break;
                    }
                    case OrderOrderFields.Date:
                    {
                        query = query.OrderBy(x => x.Date);
                        break;
                    }
                    case OrderOrderFields.Provider:
                    {
                        query = query.OrderBy(x => x.Provider.Name);
                        break;
                    }
                    case OrderOrderFields.Name:
                    {
                        query = query.OrderBy(x => x.Name);
                        break;
                    }
                    case OrderOrderFields.Number:
                    {
                        query = query.OrderBy(x => x.NumberString);
                        break;
                    }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderOrderFields.CreateDate:
                    {
                        query = query.OrderByDescending(x => x.CreateDate);
                        break;
                    }
                    case OrderOrderFields.ModifyDate:
                    {
                        query = query.OrderByDescending(x => x.ModifyDate);
                        break;
                    }
                    case OrderOrderFields.Date:
                    {
                        query = query.OrderByDescending(x => x.Date);
                        break;
                    }
                    case OrderOrderFields.Provider:
                    {
                        query = query.OrderByDescending(x => x.Provider.Name);
                        break;
                    }
                    case OrderOrderFields.Name:
                    {
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    }
                    case OrderOrderFields.Number:
                    {
                        query = query.OrderByDescending(x => x.NumberString);
                        break;
                    }
                }
            }

            return query;
        }

    }
}
