﻿

using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.DAL.Repository.Order
{
    public class UnitRepository : RepositoryForDeletedGuid<Unit, OrderUnitFields>, IUnitRepository
    {
        public UnitRepository(TestTaskOrderContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<Unit>> GetAllAsync(string search,
            PagingOrderSetting<OrderUnitFields> pagingOrderSetting,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => EF.Functions.ILike(x.Name, "%" + search + "%"));
            }

            var entities = await GetListAsync(query, pagingOrderSetting);
            ClearDbSetForInclude(entities);
            return entities;
        }

        public async Task<int> GetAllCountAsync(string search,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete)
        {
            var query = ResolveInclude(null);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => EF.Functions.ILike(x.Name,  "%" + search + "%"));
            }

            var entitiesCount = await query.CountAsync();

            return entitiesCount;
        }

        public async Task<bool> EqualNameAsync(string name, Guid? id = null)
        {
            var query = ResolveInclude(new ResolveOptions { IsOrder = true })
                .Where(x => EF.Functions.ILike(x.Name, name));
            if (id.HasValue)
            {
                query = query.Where(x => x.Id != id.Value);
            }
            var isEqual = await query.AsNoTracking().AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(Unit entity)
        {

        }


        protected override IQueryable<Unit> OrderSort(IQueryable<Unit> query, PagingOrderSetting<OrderUnitFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderUnitFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUnitFields.Name:
                    {
                        query = query.OrderBy(x => x.Name);
                        break;
                    }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderUnitFields.Name:
                    {
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    }
                }
            }

            return query;
        }

        protected override IQueryable<Unit> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Unit> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }
            

            return query;
        }
        
    }
}
