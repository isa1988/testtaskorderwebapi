﻿
using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.DAL.Repository.Order
{
    public class OrderItemRepository : RepositoryGuid<OrderItem, OrderOrderItemFields>, IOrderItemRepository
    {
        public OrderItemRepository(TestTaskOrderContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<OrderItem>> GetAll(Guid orderId, PagingOrderSetting<OrderOrderItemFields> pagingOrderSetting,
            ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions)
                .Where(x => x.OrderId == orderId);

            var entities = await GetListAsync(query, pagingOrderSetting);
            ClearDbSetForInclude(entities);
            return entities;
        }

        public async Task<int> GetAllCountAsync(Guid orderId)
        {
            var query = ResolveInclude(null)
                .Where(x => x.OrderId == orderId);

            var entitiesCount = await query.CountAsync();

            return entitiesCount;
        }
        public async Task<bool> EqualNameAsync(string name, Guid orderId, Guid? id = null)
        {
            var query = ResolveInclude(new ResolveOptions { IsOrder = true })
                .Where(x => x.OrderId == orderId && (EF.Functions.ILike(x.Name, name)
                                                     || EF.Functions.ILike(x.Order.Name, name)
                                                     || EF.Functions.ILike(x.Order.NumberString, name)));
            if (id.HasValue)
            {
                query = query.Where(x => x.Id != id.Value);
            }
            var isEqual = await query.AsNoTracking().AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(OrderItem entity)
        {
            
        }
        protected override IQueryable<OrderItem> OrderSort(IQueryable<OrderItem> query, PagingOrderSetting<OrderOrderItemFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder || pagingOrderSetting.OrderField == OrderOrderItemFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderOrderItemFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderOrderItemFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<OrderItem> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<OrderItem> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.IsOrder)
            {
                query = query.Include(x => x.Order);
            }

            if (resolveOptions.IsUnit)
            {
                query = query.Include(x => x.Unit);
            }

            return query;
        }



    }
}
