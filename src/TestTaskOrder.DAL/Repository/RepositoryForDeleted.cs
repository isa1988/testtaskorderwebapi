﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestTaskOrder.Core.Contract;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.DAL.Repository
{
    public abstract class RepositoryForDeleted<T, TOrder> : Repository<T, TOrder>, IRepositoryForDeleted<T, TOrder>
        where T : class, IEntityForDelete
        where TOrder : Enum
    {
        public RepositoryForDeleted(TestTaskOrderContext contextDB)
            : base(contextDB)
        {
        }

        public override async Task<List<T>> GetAllAsync(PagingOrderSetting<TOrder> order, ResolveOptions resolveOptions = null)
        {
            var entities = await GetAllAsync(SettingSelectForDelete.OnlyUnDelete, order, resolveOptions);

            return entities;
        }

        public override async Task<int> GetAllCountAsync()
        {
            var count = await GetAllCountAsync(SettingSelectForDelete.OnlyUnDelete);
            return count;
        }

        public virtual async Task<List<T>> GetAllAsync(SettingSelectForDelete settingSelectForDelete,
            PagingOrderSetting<TOrder> order, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);
            query = SetDBSetQueryable(query, settingSelectForDelete);
            var entities = await GetListAsync(query, order);
            ClearDbSetForInclude(entities);
            return entities;
        }

        public virtual async Task<int> GetAllCountAsync(SettingSelectForDelete settingSelectForDelete)
        {
            var query = ResolveInclude(null);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            var count = await query.CountAsync();
            return count;
        }

        public override EntityEntry<T> Delete(T entity)
        {
            if(entity.IsDeleted == Deleted.UnDeleted)
            {
                entity.IsDeleted = Deleted.Deleted;
            }
            else
            {
                entity.IsDeleted = Deleted.UnDeleted;
            }
            EntityEntry<T> entryResult = dbSet.Update(entity);
            
            return entryResult;
        }

        public override async Task<EntityEntry<T>> DeleteAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                if (entity.IsDeleted == Deleted.UnDeleted)
                {
                    entity.IsDeleted = Deleted.Deleted;
                }
                else
                {
                    entity.IsDeleted = Deleted.UnDeleted;
                }
                return dbSet.Update(entity);
            });
            var entry = await task;
            return entry;
        }

        public EntityEntry<T> DeleteFromDbAsync(T entity)
        {  
            return dbSet.Remove(entity);
        }

        protected IQueryable<T> SetDBSetQueryable(IQueryable<T> query, SettingSelectForDelete settingSelectForDelete)
        {
            if (settingSelectForDelete == SettingSelectForDelete.OnlyDelete)
            {
                query = query.Where(x => x.IsDeleted == Deleted.Deleted);
            }
            else if (settingSelectForDelete == SettingSelectForDelete.OnlyUnDelete)
            {
                query = query.Where(x => x.IsDeleted == Deleted.UnDeleted);
            }

            return query;
        }
    }

    public abstract class RepositoryForDeleted<T, TId, TOrder> : RepositoryForDeleted<T, TOrder>, IRepositoryForDeleted<T, TId, TOrder>
        where T : class, IEntityForDelete<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public RepositoryForDeleted(TestTaskOrderContext contextDB)
            : base(contextDB)
        {
        }

        public virtual async Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null)
        {
            var entity = await GetByIdAsync(id, SettingSelectForDelete.OnlyUnDelete, resolveOptions);

            return entity;
        }

        public virtual async Task<List<T>> GetAllByIdAsync(List<TId> idList,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => idList.Any(id => x.Id.Equals(id)));

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }

        public virtual async Task<T> GetByIdAsync(TId id, SettingSelectForDelete settingSelectForDelete, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);
            query = SetDBSetQueryable(query, settingSelectForDelete);
            query = query.Where(x => x.Id.Equals(id));
            var entity = await query.FirstOrDefaultAsync();
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

        public virtual async Task<List<T>> GetAllByIdAsync(List<TId> idList, SettingSelectForDelete settingSelectForDelete,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => idList.Any(id => x.Id.Equals(id)));
            query = SetDBSetQueryable(query, settingSelectForDelete);
            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }
    }

    public abstract class RepositoryForDeletedGuid<T, TOrder> : RepositoryForDeleted<T, Guid, TOrder>, IRepositoryForDeleted<T, Guid, TOrder>
        where T : class, IEntityForDelete<Guid>
        where TOrder : Enum
    {
        public RepositoryForDeletedGuid(TestTaskOrderContext contextDB)
            : base(contextDB)
        {
        }

        public override async Task<T> GetByIdAsync(Guid id, ResolveOptions resolveOptions = null)
        {
            var entity = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

        public override async Task<List<T>> GetAllByIdAsync(List<Guid> idList,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => idList.Any(id => x.Id == id));

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }

        public override async Task<T> GetByIdAsync(Guid id, SettingSelectForDelete settingSelectForDelete, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);
            query = SetDBSetQueryable(query, settingSelectForDelete);
            query = query.Where(x => x.Id == id);
            var entity = await query.FirstOrDefaultAsync();
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }
        
        public virtual async Task<List<T>> GetAllByIdAsync(List<Guid> idList, SettingSelectForDelete settingSelectForDelete,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => idList.Any(id => x.Id == id));
            query = SetDBSetQueryable(query, settingSelectForDelete);
            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }
    }
}
