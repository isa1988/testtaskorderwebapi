﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestTaskOrder.Core.Contract;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.DAL.Repository
{
    public abstract class RepositoryBase<T, TOrder>
        where T : class
        where TOrder : Enum
    {
        protected virtual void ClearDbSetForInclude(List<T> entities)
        {
            foreach (var entity in entities)
            {
                ClearDbSetForInclude(entity);
            }
        }

        protected async Task<List<T>> GetListAsync(IQueryable<T> query, PagingOrderSetting<TOrder> pagingOrderSetting)
        {
            query = OrderSort(query, pagingOrderSetting);
            if (pagingOrderSetting != null && pagingOrderSetting.PageSetting != null)
            {
                query = query.Skip(pagingOrderSetting.PageSetting.StartPosition)
                    .Take(pagingOrderSetting.PageSetting.PageSize);
            }

            List<T> entities = await query.ToListAsync();
            ClearDbSetForInclude(entities);
            return entities;
        }

        protected abstract void ClearDbSetForInclude(T entity);

        protected abstract IQueryable<T> ResolveInclude(ResolveOptions resolveOptions);
        protected abstract IQueryable<T> OrderSort(IQueryable<T> query, PagingOrderSetting<TOrder> pagingOrderSetting = null);
    }


    public abstract class Repository<T, TOrder> : RepositoryBase<T, TOrder>, IRepository<T, TOrder> 
        where T : class, IEntity
        where TOrder: Enum
    {
        public Repository(TestTaskOrderContext contextDB)
        {
            this.contextDB = contextDB;
            this.dbSet = this.contextDB.Set<T>();
            this.dbSetQueryable = dbSet;
        }

        protected TestTaskOrderContext contextDB;
        protected DbSet<T> dbSet { get; set; }
        protected IQueryable<T> dbSetQueryable { get; set; }
        public virtual async Task<T> AddAsync(T entity)
        {
            var entry = await dbSet.AddAsync(entity);
            entity.ModifyDate = DateTime.Now;

            return entry.Entity;
        }
        public virtual async Task<List<T>> GetAllAsync(PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }

        public virtual async Task<int> GetAllCountAsync()
        {
            var count = await ResolveInclude(null).CountAsync();
            return count;
        }

        public virtual async Task<EntityEntry<T>> UpdateAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Update(entity);
            });
            var entry = await task;
            return entry;
        }

        public virtual async Task<EntityEntry<T>> DeleteAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Remove(entity);
            });
            var entry = await task;
            return entry;
        }

        public virtual EntityEntry<T> Update(T entity)
        {
            entity.ModifyDate = DateTime.UtcNow;
            return dbSet.Update(entity);
        }

        public virtual EntityEntry<T> Delete(T entity)
        {
            return dbSet.Remove(entity);
        }

        public virtual async Task SaveAsync()
        {
            await contextDB.SaveChangesAsync();
        }
    }

    public abstract class Repository<T, TId, TOrder> : Repository<T, TOrder>, IRepository<T, TId, TOrder>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
        where TOrder: Enum
    {
        public Repository(TestTaskOrderContext contextDB)
            : base(contextDB)
        {
        }

        public virtual async Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null)
        {
            var entity = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

        public virtual async Task<List<T>> GetAllByIdAsync(List<TId> idList,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => idList.Any(id => x.Id.Equals(id)));

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }
    }

    public abstract class RepositoryGuid<T, TOrder> : Repository<T, Guid, TOrder>, IRepository<T, Guid, TOrder>
        where T : class, IEntity<Guid>
        where TOrder : Enum
    {
        public RepositoryGuid(TestTaskOrderContext contextDB)
            : base(contextDB)
        {
        }

        public override async Task<T> GetByIdAsync(Guid id, ResolveOptions resolveOptions = null)
        {
            var entity = await ResolveInclude(resolveOptions).FirstOrDefaultAsync(x => x.Id == id);
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

        public override async Task<List<T>> GetAllByIdAsync(List<Guid> idList,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions).Where(x => idList.Any(id => x.Id == id));

            var entities = await GetListAsync(query, pagingOrderSetting);

            return entities;
        }
    }
}
