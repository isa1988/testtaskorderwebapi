﻿
using Microsoft.EntityFrameworkCore;
using TestTaskOrder.Core.Contract.Provider;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.DAL.Data;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;

namespace TestTaskOrder.DAL.Repository.Provider
{
    public class ProviderRepository : RepositoryForDeletedGuid<Core.Entities.AreaProvider.Provider, OrderProviderFields>, IProviderRepository
    {
        public ProviderRepository(TestTaskOrderContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<Core.Entities.AreaProvider.Provider>> GetAllAsync(string search,
            PagingOrderSetting<OrderProviderFields> pagingOrderSetting,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            ResolveOptions resolveOptions = null)
        {
            var query = ResolveInclude(resolveOptions);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => EF.Functions.ILike(x.Name, "%" + search + "%"));
            }
            
            var entities = await GetListAsync(query, pagingOrderSetting);
            ClearDbSetForInclude(entities);
            return entities;
        }

        public async Task<int> GetAllCountAsync(string search, 
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete)
        {
            var query = ResolveInclude(null);
            query = SetDBSetQueryable(query, settingSelectForDelete);

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => EF.Functions.ILike(x.Name, "%" + search + "%"));
            }

            var entitiesCount = await query.CountAsync();

            return entitiesCount;
        }

        public async Task<bool> EqualNameAsync(string name, Guid? id = null)
        {
            var query = ResolveInclude(new ResolveOptions { IsOrder = true })
                .Where(x => EF.Functions.ILike(x.Name, name));
            if (id.HasValue)
            {
                query = query.Where(x => x.Id != id.Value);
            }
            var isEqual = await query.AsNoTracking().AnyAsync();

            return isEqual;
        }
        protected override void ClearDbSetForInclude(Core.Entities.AreaProvider.Provider entity)
        {

        }


        protected override IQueryable<Core.Entities.AreaProvider.Provider> OrderSort(
            IQueryable<Core.Entities.AreaProvider.Provider> query,
            PagingOrderSetting<OrderProviderFields> pagingOrderSetting = null)
        {
            if (pagingOrderSetting == null || !pagingOrderSetting.IsOrder ||
                pagingOrderSetting.OrderField == OrderProviderFields.None)
                return query;

            if (pagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderProviderFields.Name:
                    {
                        query = query.OrderBy(x => x.Name);
                        break;
                    }
                }
            }
            else
            {
                switch (pagingOrderSetting.OrderField)
                {
                    case OrderProviderFields.Name:
                    {
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    }
                }
            }

            return query;
        }

        protected override IQueryable<Core.Entities.AreaProvider.Provider> ResolveInclude(ResolveOptions resolveOptions)
        {
            IQueryable<Core.Entities.AreaProvider.Provider> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }


            return query;
        }

    }
}
