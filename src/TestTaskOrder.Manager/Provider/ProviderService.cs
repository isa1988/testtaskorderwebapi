﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Provider;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Provider;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Provider;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;

namespace TestTaskOrder.Manager.Provider
{
    public class ProviderService : GeneralServiceForDeleteGuid<Core.Entities.AreaProvider.Provider, ProviderDto, OrderProviderFields>,
        IProviderService
    {
        public ProviderService(IProviderRepository providerRepository,
            IMapper mapper)
        : base(mapper, providerRepository)
        {
            this.providerRepository = providerRepository;
        }

        private readonly IProviderRepository providerRepository;

        public async Task<EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>> GetAllWithDelAsync(
            SearchByOneLineDtoFilter<OrderProviderFields> filter)
        {
            try
            {
                var orderSet = mapper.Map<PagingOrderSetting<OrderProviderFields>>(filter.OrderSetting);
                var entityList = await providerRepository.GetAllAsync(filter.Search, orderSet,
                    SettingSelectForDelete.All, GetOptionsForDetails());
                var dtoList = mapper.Map<List<ProviderDto>>(entityList);
                int totalCount = await providerRepository.GetAllCountAsync(filter.Search, SettingSelectForDelete.All);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>> GetAllAsync(
            SearchByOneLineDtoFilter<OrderProviderFields> filter)
        {
            try
            {
                var orderSet = mapper.Map<PagingOrderSetting<OrderProviderFields>>(filter.OrderSetting);
                var entityList = await providerRepository.GetAllAsync(filter.Search, orderSet,
                    resolveOptions: GetOptionsForDetails());
                var dtoList = mapper.Map<List<ProviderDto>>(entityList);
                int totalCount = await providerRepository.GetAllCountAsync(filter.Search);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<ProviderDto>> EditAsync(ProviderDto editDto)
        {
            await editDto.ClearSpace();
            var error = await CheckBeforeModificationAsync(editDto, false);
            if (!error.IsSuccess)
            {
                return EntityOperationResult<ProviderDto>.Failure(error.Message, error.Code);
            }

            try
            {
                var entity = await providerRepository.GetByIdAsync(editDto.Id);
                entity.Name = editDto.Name;

                providerRepository.Update(entity);
                await providerRepository.SaveAsync();

                editDto = mapper.Map<ProviderDto>(entity);

                return EntityOperationResult<ProviderDto>.Success(editDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<ProviderDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<ProviderDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions();
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(ProviderDto value, bool isNew = true)
        {
            if (value == null)
            {
                return new ErrorDto { Message = "Пустой объект", Code = ResultCode.FieldIsNull };
            }

            if (string.IsNullOrWhiteSpace(value.Name))
            {
                return new ErrorDto { Message = "Не заполненно Наименование", Code = ResultCode.FieldIsNull };
            }

            if (isNew && await providerRepository.EqualNameAsync(value.Name))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }
            else if (!isNew && await providerRepository.EqualNameAsync(value.Name, value.Id))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }

            return new ErrorDto { Code = ResultCode.Success };
        }
        
        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(Core.Entities.AreaProvider.Provider entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }

    }
}
