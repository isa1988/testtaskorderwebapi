﻿using AutoMapper;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Filters;
using TestTaskOrder.Core.Filters.Order;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.ManagerContract.Dto.Provider;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Manager
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            GeneralMapping();
            UserMapping();
            RoleMapping();
            SesionMapping();
            OrderMapping();
            ProviderMapping();
        }

        private void ProviderMapping()
        {
            CreateMap<Core.Entities.AreaProvider.Provider, ProviderDto>();
            CreateMap<ProviderDto, Core.Entities.AreaProvider.Provider>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.OrderList, p => p.Ignore());
        }

        private void OrderMapping()
        {
            CreateMap<Core.Entities.AreaOrder.Order, OrderDto>();
            CreateMap<OrderDto, Core.Entities.AreaOrder.Order>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.Provider, p => p.Ignore())
                .ForMember(x => x.OrderItemList, p => p.Ignore())
                .ForMember(x => x.Author, p => p.Ignore());

            CreateMap<OrderItem, OrderItemDto>();
            CreateMap<OrderItemDto, OrderItem>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.Order, p => p.Ignore())
                .ForMember(x => x.Unit, p => p.Ignore());

            CreateMap<Unit, UnitDto>();
            CreateMap<UnitDto, Unit>().ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.OrderItemList, p => p.Ignore());

            CreateMap<OrderFilterDto, OrderFilter>();
        }

        private void SesionMapping()
        {
            CreateMap<Session, SessionDto>();
            CreateMap<SessionDto, Session>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.UserRole, p => p.Ignore())
                .ForMember(x => x.User, p => p.Ignore());
        }

        private void RoleMapping()
        {
            CreateMap<UserRole, UserRoleDto>();
            CreateMap<UserRoleDto, UserRole>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore());

            CreateMap<Role, RoleDto>();
            CreateMap<RoleDto, Role>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore());
        }

        private void UserMapping()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.RoleList, p => p.Ignore())
                .ForMember(x => x.SessionList, p => p.Ignore());

            CreateMap<Session, AuthenticateResponseDto>()
                .ForMember(x => x.SessionId, p => p.MapFrom(m => m.Id))
                .ForMember(x => x.Roles, p => p.Ignore());

            CreateMap<RegistrationDto, UserDto>()
                .ForMember(x => x.RoleList, p => p.Ignore());

            CreateMap<RegistrationDto, User>()
                .ForMember(x => x.CreateDate, p => p.Ignore())
                .ForMember(x => x.ModifyDate, p => p.Ignore())
                .ForMember(x => x.RoleList, p => p.Ignore())
                .ForMember(x => x.SessionList, p => p.Ignore());

            CreateMap<User, UserCaptionDto>()
                .ForMember(x => x.Title,
                    p => p.MapFrom(m => m.LastName + " " + m.FirstName));
        }

        private void GeneralMapping()
        {
            CreateMap(typeof(PagingOrderSettingDto<>), typeof(PagingOrderSetting<>))
                .ForMember("PageSetting",
                    p => p.MapFrom(
                        n => new PagingPageSetting
                        {
                            PageSize = Reflection.GetPropValue<int>(n, "PageSize"),
                            StartPosition = Reflection.GetPropValue<int>(n, "StartPosition")
                        }));
            CreateMap(typeof(PagingOrderSettingWithStrFieldDto<>), typeof(PagingOrderSetting<>))
                .ForMember("PageSetting",
                    p => p.MapFrom(
                        n => new PagingPageSetting
                        {
                            PageSize = Reflection.GetPropValue<int>(n, "PageSize"),
                            StartPosition = Reflection.GetPropValue<int>(n, "StartPosition")
                        }));
            CreateMap(typeof(PeriodFilterDto<>), typeof(PeriodFilter<>));
        }
    }
}
