﻿using System.Security.Claims;
using System.Text;
using AutoMapper;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.ManagerContract.Dto.Setting;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using TestTaskOrder.ManagerContract.Filters.Authentication;
using TestTaskOrder.Primitive.PagingOrderSettings;

namespace TestTaskOrder.Manager.Authentication
{
    public class UserService : GeneralServiceForDeleteGuid<User, UserDto, OrderUserFields>, IUserService
    {
        public UserService(IMapper mapper, 
                           IUserRepository repository, 
                           IUserRoleRepository userRoleRepository, 
                           ISessionRepository sessionRepository,
                           IRoleRepository roleRepository) 
            : base(mapper, repository)
        {
            this.userRepository = repository;
            this.userRoleRepository = userRoleRepository;
            this.sessionRepository = sessionRepository;
            this.roleRepository = roleRepository;
        }

        private readonly IUserRepository userRepository;
        private readonly IUserRoleRepository userRoleRepository;
        private readonly ISessionRepository sessionRepository;
        private readonly IRoleRepository roleRepository;

        public async Task<EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>> GetAllFromSearchAsync(
            SearchByOneLineDtoFilter<OrderUserFields> filter)
        {
            try
            {

                var orderSet = mapper.Map<PagingOrderSetting<OrderUserFields>>(filter.OrderSetting);
                var entityList = await userRepository.GetAllFromSearchAsync(filter.Search, orderSet);
                var dtoList = mapper.Map<List<UserDto>>(entityList);
                int totalCount = await userRepository.GetAllFromSearchCountAsync(filter.Search);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>> GetAllFromSearchDetailAsync(SearchByOneLineDtoFilter<OrderUserFields> filter)
        {
            try
            {

                var orderSet = mapper.Map<PagingOrderSetting<OrderUserFields>>(filter.OrderSetting);
                var entityList =
                    await userRepository.GetAllFromSearchAsync(filter.Search, orderSet, GetOptionsForDetails());
                var dtoList = mapper.Map<List<UserDto>>(entityList);
                int totalCount = await userRepository.GetAllFromSearchCountAsync(filter.Search);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }
        
        public async Task<EntityOperationResult<AuthenticateResponseDto>> LoginAsync(LoginDto login, JwtDto jwtDto)
        {
            try
            {
                await login.ClearSpace();
                User user = await userRepository.GetUserAsync(login.Login, login.Password);

                if (user == null)
                {
                    throw new RawDbNullException("Неверный логин или пароль");
                }
                
                var roleList = await userRoleRepository.GetByUserListAsync(user.Id, new ResolveOptions{IsRole = true});

                if (roleList.Count == 0)
                {
                    throw new RawDbNullException($"There no roles for this user {user.Id}");
                }

                await sessionRepository.CloseAllOpenSesionAsync(user.Id);

                var session = new Session
                {
                    UserId = user.Id,
                    Token = GenerateToken(Guid.NewGuid().ToString(), jwtDto.Key, jwtDto.Issuer),
                    Info = string.Empty
                };

                var roleDtoList = new List<RoleDto>();

                if (roleList.Count == 1)
                {
                    session.RoleId = roleList[0].Id;
                }
                else
                {
                    var roleIdInfoList = roleList.Select(x => x.Role).ToList();
                    
                    roleDtoList = mapper.Map<List<RoleDto>>(roleIdInfoList);

                }

                session = await sessionRepository.AddAsync(session);
                await sessionRepository.SaveAsync();

                var userDto = mapper.Map<AuthenticateResponseDto>(session);

                if (roleDtoList.Count > 0)
                {
                    userDto.Roles = roleDtoList;
                }

                return EntityOperationResult<AuthenticateResponseDto>.Success(userDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<AuthenticateResponseDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<AuthenticateResponseDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<SessionDto>> RoleSelectAsync(LoginConfirmRoleDto model)
        {
            try
            {
                await model.ClearSpace();
                var session = await sessionRepository.GetActiveByIdAsync(model.SessionId);
                var role = await roleRepository.GetBySysNameAsync(model.RoleSys);
                var userRole = await userRoleRepository.GetByRoleForUserAsync(session.UserId, role.Id);
                session.RoleId = userRole.Id;
                sessionRepository.Update(session);
                await sessionRepository.SaveAsync();
                var sessionDto = mapper.Map<SessionDto>(session);

                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }
        

        public async Task<EntityOperationResult<UserDto>> RegistrationByAdminAsync(RegistrationDto registration)
        {
            try
            {
                await registration.ClearSpace();
                var error = await GeneralCheckBeforeRegAsync(registration);
                
                if (!error.IsSuccess)
                    return EntityOperationResult<UserDto>.Failure(error.Message, error.Code);

                var user = mapper.Map<User>(registration);
                user = await userRepository.AddAsync(user);
                await userRepository.SaveAsync();

                var result = await AddRolesToUserAsync(user.Id, registration.RoleList);

                if (!result.IsSuccess)
                {
                    return EntityOperationResult<UserDto>.Failure(result.ErrorMessage, result.Code);
                }

                var userDto = mapper.Map<UserDto>(user);

                userDto.RoleList = result.Entity.UserRoleList;

                return EntityOperationResult<UserDto>.Success(userDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }
        
        private async Task<EntityOperationResult<UserRoleListDto>> AddRolesToUserAsync(Guid userId,
            List<RoleDto> roleList)
        {
            try
            {
                foreach (var roleRec in roleList)
                {
                    await roleRec.ClearSpace();
                }
                UserRole retUserRole = null;
                Role role = null;

                List<UserRole> userRoleList = new List<UserRole>();

                foreach (var roleItem in roleList)
                {
                    role = await roleRepository.GetBySysNameAsync(roleItem.SysName);
                    retUserRole = await userRoleRepository.AddAsync(new UserRole
                    {
                        UserId = userId,
                        RoleId = role.Id
                    });

                    userRoleList.Add(retUserRole);
                }

                await userRoleRepository.SaveAsync();

                var roleDto = new UserRoleListDto();

                roleDto.UserRoleList = mapper.Map<List<UserRoleDto>>(userRoleList);

                return EntityOperationResult<UserRoleListDto>.Success(roleDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserRoleListDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserRoleListDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }


        private async Task<ErrorDto> GeneralCheckBeforeRegAsync(RegistrationDto registrationDto)
        {
            var userDto = mapper.Map<UserDto>(registrationDto);
            var error = await CheckBeforeModificationAsync(userDto);
            if (!error.IsSuccess)
            {
                return error;
            }

            if (string.IsNullOrWhiteSpace(registrationDto.Password))
            {
                return new ErrorDto { Message = "Не заполнен пароль", Code = ResultCode.FieldIsNull };
            }
            if (registrationDto.Password != registrationDto.PasswordConfirm)
            {
                return new ErrorDto { Message = "Пароль не совпадает с подверждением", Code = ResultCode.DoesNotMatch };
            }
            if (registrationDto.RoleList == null || registrationDto.RoleList.Count == 0)
            {
                return new ErrorDto { Message = "Не указана не одна из ролей", Code = ResultCode.FieldIsNull };
            }    
            return new ErrorDto{Code = ResultCode.Success};
        }
        
        private string GenerateToken(string tokenRequest, string key, string issuer)
        {
            tokenRequest = tokenRequest.TrimWithNull();
            key = key.TrimWithNull();
            issuer = issuer.TrimWithNull();
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(issuer,
                issuer,
                new Claim[] { new Claim(ClaimTypes.Name, tokenRequest), },
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<EntityOperationResult<PageInfoListDto<RoleDto, OrderRoleFields>>> GetRoleExcludeUserRoleAsync(RoleFilterDto filter)
        {
            try
            {

                var orderSet = mapper.Map<PagingOrderSetting<OrderRoleFields>>(filter.OrderSetting);
                var roleList = await roleRepository.GetAllAsync(orderSet);
                var orderRoleUser = new PagingOrderSetting<OrderUserRoleFields>()
                {
                    OrderField = OrderUserRoleFields.None,
                    IsOrder = false,
                    PageSetting = orderSet.PageSetting,
                    OrderDirection = OrderType.ASC
                };
                var userRoleList = await userRoleRepository.GetByUserListAsync(filter.UserId, orderRoleUser);

                var totalCount = await roleRepository.GetAllCountAsync();
                if (userRoleList.Count > 0)
                {
                    var roleIdList = roleList.Select(x => x.Id).ToList();
                    var userRoleIdList = userRoleList.Select(x => x.RoleId).ToList();
                    var resultIdList = roleIdList.Except(userRoleIdList).ToList();
                    roleList = roleList.Where(x => resultIdList.Any(m => x.Id == m)).ToList();

                    var totalUserRoleCount = await userRoleRepository.GetByUserCountAsync(filter.UserId);
                    totalCount -= totalUserRoleCount;
                }

                var dtoList = mapper.Map<List<RoleDto>>(roleList);

                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<RoleDto, OrderRoleFields>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<RoleDto, OrderRoleFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<RoleDto, OrderRoleFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<UserDto>> GetUserIdBySessionIdAsync(Guid sessionId)
        {
            try
            {
                var userId = await sessionRepository.GetUserIdByActiveSession(sessionId);
                return EntityOperationResult<UserDto>.Success(new UserDto{Id = userId});
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.General);
            }

        }

        public async Task<EntityOperationResult<UserDto>> LogoutAsync(Guid userId)
        {
            try
            {
                await sessionRepository.CloseAllOpenSesionAsync(userId);

                await sessionRepository.SaveAsync();

                return EntityOperationResult<UserDto>.Success(new UserDto());
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<UserDto>> EditAsync(UserEditDto editDto)
        {
            var user = await userRepository.GetByIdAsync(editDto.Id);
            var result = await EditCurrentAsync(user, editDto);
            return result;
        }

        public async Task<EntityOperationResult<UserDto>> EditCurrentAsync(UserEditDto editDto)
        {
            var user = await userRepository.GetByIdAsync(editDto.Id);
            var result = await EditCurrentAsync(user, editDto);
            return result;
        }

        private async Task<EntityOperationResult<UserDto>> EditCurrentAsync(User user, UserEditDto editDto)
        {
            try
            {
                await editDto.ClearSpace();
                if (user == null)
                {
                    return EntityOperationResult<UserDto>.Failure("Не найден пользователь", ResultCode.FieldIsNull);
                }
                
                user.FirstName = editDto.FirstName;
                user.MiddleName = editDto.MiddleName;
                user.LastName = editDto.LastName;
                user.Phone = editDto.Phone;
                
                userRepository.Update(user);

                await userRepository.SaveAsync();
                var userDto = mapper.Map<UserDto>(user);

                return EntityOperationResult<UserDto>.Success(userDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<UserDto>> ChangePasswordAsync(UserChangePasswordDto changePasswordDto)
        {
            var user = await userRepository.GetByIdAsync(changePasswordDto.Id);
            var result = await ChangePasswordAsync(user, changePasswordDto);
            return result;
        }

        public async Task<EntityOperationResult<UserCaptionDto>> GetUserCaptionAsync(Guid sessionId)
        {
            try
            {
                var user = await userRepository.GetByIdAsync(sessionId);
                if (user == null)
                {
                    return EntityOperationResult<UserCaptionDto>.Failure("Не найден пользователь", ResultCode.LineIsNotSearch);
                }

                var userCaption = new UserCaptionDto
                {
                    Title = user.LastName + " " + user.FirstName + " " + user.MiddleName,
                };


                return EntityOperationResult<UserCaptionDto>.Success(userCaption);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserCaptionDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserCaptionDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<UserDto>> ChangePasswordCurrentAsync(
            UserChangePasswordDto changePasswordDto)
        {
            var user = await userRepository.GetByIdAsync(changePasswordDto.Id);
            var result = await ChangePasswordAsync(user, changePasswordDto);
            return result;
        }

        private async Task<EntityOperationResult<UserDto>> ChangePasswordAsync(User user, UserChangePasswordDto changePassword)
        {
            try
            {
                await changePassword.ClearSpace();
                if (user == null)
                {
                    return EntityOperationResult<UserDto>.Failure("Не найден пользователь", ResultCode.Success);
                }

                if (changePassword.IsCheck)
                {
                    var passwordCheck = userRepository.GetEncryptedPassword(changePassword.PasswordCurrent);
                    if (passwordCheck != user.Password)
                    {
                        return EntityOperationResult<UserDto>.Failure("Текущий пароль неверен", ResultCode.LineIsNotSearch);
                    }
                }

                if (changePassword.PasswordNew != changePassword.PasswordNewConfirm)
                {
                    return EntityOperationResult<UserDto>.Failure("Новый пароль не совпадает с подвержденным", ResultCode.DoesNotMatch);
                }

                user.Password = userRepository.GetEncryptedPassword(changePassword.PasswordNew);

                userRepository.Update(user);

                await userRepository.SaveAsync();
                var userDto = mapper.Map<UserDto>(user);

                return EntityOperationResult<UserDto>.Success(userDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UserDto>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions
            {

            };
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(UserDto value, bool isNew = true)
        {
            if (string.IsNullOrWhiteSpace(value.FirstName))
            {
                return new ErrorDto { Message = "Не заполнено поле Имя", Code = ResultCode.FieldIsNull };
            }
            if (string.IsNullOrWhiteSpace(value.LastName))
            {
                return new ErrorDto { Message = "Не заполнено поле Фамилия", Code = ResultCode.FieldIsNull };
            }
            if (string.IsNullOrWhiteSpace(value.Email))
            {
                return new ErrorDto { Message = "Не заполнено поле Электронный ящик", Code = ResultCode.FieldIsNull };
            }
            if (string.IsNullOrWhiteSpace(value.Password))
            {
                return new ErrorDto { Message = "Не заполнено поле Пароль", Code = ResultCode.FieldIsNull };
            }
            if (isNew)
            {
                if (await userRepository.IsEqualEmailAsync(value.Email))
                {
                    return new ErrorDto { Message = "Такой email уже используется", Code = ResultCode.EqualInDb };
                }
            }
            else
            {
                if (await userRepository.IsEqualEmailAsync(value.Email, value.Id))
                {
                    return new ErrorDto { Message = "Такой email уже используется", Code = ResultCode.EqualInDb };
                }
            }
            return new ErrorDto { Code = ResultCode.Success };
        }

        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(User entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }
    }
}
