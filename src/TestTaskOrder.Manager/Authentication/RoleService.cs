﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Manager.Authentication
{
    public class RoleService : GeneralServiceForDeleteGuid<Role, RoleDto, OrderRoleFields>, IRoleService
    {
        public RoleService(IMapper mapper, IRoleRepository repository) : base(mapper, repository)
        {
            roleRepository = repository;
        }

        private readonly IRoleRepository roleRepository;

        public async Task<EntityOperationResult<RoleDto>> EditAsync(RoleDto editDto)
        {
            await editDto.ClearSpace();
            var error = await CheckBeforeModificationAsync(editDto, false);
            if (!error.IsSuccess)
            {
                return EntityOperationResult<RoleDto>.Failure(error.Message, error.Code);
            }

            try
            {
                var entity = await roleRepository.GetByIdAsync(editDto.Id);
                entity.Name = editDto.Name;
                entity.SysName= editDto.SysName;

                roleRepository.Update(entity);
                await roleRepository.SaveAsync();

                editDto = mapper.Map<RoleDto>(entity);

                return EntityOperationResult<RoleDto>.Success(editDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<RoleDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<RoleDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions();
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(RoleDto value, bool isNew = true)
        {
            if (value == null)
            {
                return new ErrorDto { Message = "Пустой объект", Code = ResultCode.FieldIsNull };
            }

            if (string.IsNullOrWhiteSpace(value.Name))
            {
                return new ErrorDto { Message = "Не заполненно Наименование", Code = ResultCode.FieldIsNull };
            }
            if (string.IsNullOrWhiteSpace(value.SysName))
            {
                return new ErrorDto { Message = "Не заполненно Системное наименование", Code = ResultCode.FieldIsNull };
            }

            if (isNew && await roleRepository.IsEqualsNameAsync(value.Name, value.SysName))
            {
                return new ErrorDto { Message = "Уже есть инн с данным кпп", Code = ResultCode.EqualInDb};
            }
            else if (!isNew && await roleRepository.IsEqualsNameAsync(value.Name, value.SysName, value.Id))
            {
                return new ErrorDto { Message = "Уже есть инн с данным кпп", Code = ResultCode.EqualInDb };
            }

            return new ErrorDto {Code = ResultCode.Success};
        }

        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(Role entity)
        {
            return new ErrorDto();
        }
    }
}
