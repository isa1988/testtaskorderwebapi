﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Manager.Authentication
{
    public class SessionService : GeneralServiceGuid<Session, SessionDto, OrderSessionFields>, ISessionService
    {
        public SessionService(IMapper mapper, ISessionRepository repository)
            : base(mapper, repository)
        {
            this.sessionRepository = repository;
        }

        private readonly ISessionRepository sessionRepository;

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions
            {
                IsRole = true,
                IsUser = true
            };
        }

        public async Task<EntityOperationResult<PageInfoListDto<SessionDto, OrderSessionFields>>> GetActiveByUserListAsync(List<Guid> userListId)
        {
            try
            {
                var sessionList = await sessionRepository.GetActiveByUserListAsync(userListId);
                var sessionDtoList = mapper.Map<List<SessionDto>>(sessionList);
                return EntityOperationResult<PageInfoListDto<SessionDto, OrderSessionFields>>.Success(new PageInfoListDto<SessionDto, OrderSessionFields>
                {
                    OrderSetting = null,
                    ValueList = sessionDtoList,
                    TotalCount = sessionDtoList.Count,
                });
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<SessionDto, OrderSessionFields>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<SessionDto, OrderSessionFields>>.Failure(ex.Message, ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<SessionDto>> GetActiveByIdAsync(Guid id)
        {
            try
            {
                var session = await sessionRepository.GetActiveByIdAsync(id);
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<SessionDto>> GetActiveByIdDetailsAsync(Guid id)
        {
            try
            {
                var session = await sessionRepository.GetActiveByIdAsync(id, GetOptionsForDetails());
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.General);
            }
        }
        public async Task<EntityOperationResult<SessionDto>> GetActiveByTokenAsync(string token)
        {
            try
            {
                var session = await sessionRepository.GetActiveByTokenAsync(token);
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<SessionDto>> GetActiveByTokenDetailAsync(string token)
        {
            try
            {
                var session = await sessionRepository.GetActiveByTokenAsync(token, GetOptionsForDetails());
                var sessionDto = mapper.Map<SessionDto>(session);
                return EntityOperationResult<SessionDto>.Success(sessionDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<SessionDto>> DeleteByUserRoleAsync(Guid userRoleId)
        {
            try
            {
                var sessionList = await sessionRepository.GetAllByUserRoleAsync(userRoleId, null);
                if (sessionList?.Count > 0)
                {
                    foreach (var session in sessionList)
                    {
                        sessionRepository.Delete(session);
                    }

                    await sessionRepository.SaveAsync();
                }

                return EntityOperationResult<SessionDto>.Success(new SessionDto());
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<SessionDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(SessionDto value, bool isNew = true)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }

        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(Session entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }
    }
}
