﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Authentication;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Authentication;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Manager.Authentication
{
    public class UserRoleService : GeneralServiceGuid<UserRole, UserRoleDto, OrderUserRoleFields>, IUserRoleService
    {
        public UserRoleService(IMapper mapper, IUserRoleRepository repository) : base(mapper, repository)
        {
            userRoleRepository = repository;
        }

        private readonly IUserRoleRepository userRoleRepository;
        

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions
            {
                IsUser = true,
                IsRole = true,
            };
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(UserRoleDto value, bool isNew = true)
        {
            try
            {
                var roleUser = await userRoleRepository.GetByRoleForUserAsync(value.UserId, value.RoleId);
                if (roleUser != null)
                {
                    return new ErrorDto
                        { Message = "Уже есть такая роль на данного пользователя", Code = ResultCode.EqualInDb };
                }
            }
            catch (Exception ex)
            {
            }

            return new ErrorDto { Code = ResultCode.Success };
        }

        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(UserRole entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }
    }
}
