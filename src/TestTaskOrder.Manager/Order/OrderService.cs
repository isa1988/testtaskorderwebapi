﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Filters.Order;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Manager.Order
{
    public class OrderService : GeneralServiceForDeleteGuid<Core.Entities.AreaOrder.Order, OrderDto, OrderOrderFields>,
        IOrderService
    {
        public OrderService(IOrderRepository orderRepository,
            IMapper mapper)
            : base(mapper, orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        private readonly IOrderRepository orderRepository;

        public async Task<EntityOperationResult<PageInfoListDto<OrderDto, OrderOrderFields>>> GetAllWithDelAsync(
            OrderFilterDto filter)
        {
            try
            {
                await filter.ClearSpace();
                var filterEntity = mapper.Map<OrderFilter>(filter);
                var orderSet = mapper.Map<PagingOrderSetting<OrderOrderFields>>(filter.OrderSetting);
                var entityList = await orderRepository.GetAllAsync(filterEntity, SettingSelectForDelete.All, orderSet,
                    GetOptionsForDetails());
                var dtoList = mapper.Map<List<OrderDto>>(entityList);
                int totalCount = await orderRepository.GetAllCountAsync(filterEntity, SettingSelectForDelete.All);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<OrderDto, OrderOrderFields>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<OrderDto, OrderOrderFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<OrderDto, OrderOrderFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<OrderDto>> EditAsync(OrderDto editDto)
        {
            await editDto.ClearSpace();
            var error = await CheckBeforeModificationAsync(editDto, false);
            if (!error.IsSuccess)
            {
                return EntityOperationResult<OrderDto>.Failure(error.Message, error.Code);
            }

            try
            {
                var entity = await orderRepository.GetByIdAsync(editDto.Id);
                entity.Name = editDto.Name;
                entity.Date = editDto.Date;
                entity.ProviderId = editDto.ProviderId;

                orderRepository.Update(entity);
                await orderRepository.SaveAsync();

                editDto = mapper.Map<OrderDto>(entity);

                return EntityOperationResult<OrderDto>.Success(editDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<OrderDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<OrderDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions { IsUser = true, IsProvider = true };
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(OrderDto value, bool isNew = true)
        {
            if (value == null)
            {
                return new ErrorDto { Message = "Пустой объект", Code = ResultCode.FieldIsNull };
            }

            if (string.IsNullOrWhiteSpace(value.Name))
            {
                return new ErrorDto { Message = "Не заполненно Наименование", Code = ResultCode.FieldIsNull };
            }

            if (isNew && await orderRepository.EqualNameAsync(value.Name, value.ProviderId))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }
            else if (!isNew && await orderRepository.EqualNameAsync(value.Name, value.ProviderId, value.Id))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }

            return new ErrorDto { Code = ResultCode.Success };
        }

        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(Core.Entities.AreaOrder.Order entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }


    }
}
