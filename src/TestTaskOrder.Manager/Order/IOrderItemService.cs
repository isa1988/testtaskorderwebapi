﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Manager.Order
{
    public class OrderItemService : GeneralServiceGuid<OrderItem, OrderItemDto, OrderOrderItemFields>, IOrderItemService
    {

        public OrderItemService(IOrderItemRepository orderItemRepository,
            IMapper mapper)
        : base(mapper, orderItemRepository)
        {
            this.orderItemRepository = orderItemRepository;
        }
        private readonly IOrderItemRepository orderItemRepository;

        public async Task<EntityOperationResult<PageInfoListDto<OrderItemDto, OrderOrderItemFields>>> GetAllAsync(
            OrderItemFilterDto filter)
        {
            try
            {
                await filter.ClearSpace();
                var orderSet = mapper.Map<PagingOrderSetting<OrderOrderItemFields>>(filter.OrderSetting);
                var entityList = await orderItemRepository.GetAll(filter.OrderId, orderSet,
                    GetOptionsForDetails());
                var dtoList = mapper.Map<List<OrderItemDto>>(entityList);
                int totalCount = await orderItemRepository.GetAllCountAsync(filter.OrderId);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<OrderItemDto, OrderOrderItemFields>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<OrderItemDto, OrderOrderItemFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<OrderItemDto, OrderOrderItemFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<OrderItemDto>> EditAsync(OrderItemDto editDto)
        {
            await editDto.ClearSpace();
            var error = await CheckBeforeModificationAsync(editDto, false);
            if (!error.IsSuccess)
            {
                return EntityOperationResult<OrderItemDto>.Failure(error.Message, error.Code);
            }

            try
            {
                var entity = await orderItemRepository.GetByIdAsync(editDto.Id);
                entity.Name = editDto.Name;
                entity.Quantity = editDto.Quantity;
                entity.UnitId = editDto.UnitId;

                orderItemRepository.Update(entity);
                await orderItemRepository.SaveAsync();

                editDto = mapper.Map<OrderItemDto>(entity);

                return EntityOperationResult<OrderItemDto>.Success(editDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<OrderItemDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<OrderItemDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions { IsUnit = true };
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(OrderItemDto value, bool isNew = true)
        {
            if (value == null)
            {
                return new ErrorDto { Message = "Пустой объект", Code = ResultCode.FieldIsNull };
            }

            if (string.IsNullOrWhiteSpace(value.Name))
            {
                return new ErrorDto { Message = "Не заполненно Наименование", Code = ResultCode.FieldIsNull };
            }

            if (isNew && await orderItemRepository.EqualNameAsync(value.Name, value.OrderId))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }
            else if (!isNew && await orderItemRepository.EqualNameAsync(value.Name, value.OrderId, value.Id))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }

            return new ErrorDto { Code = ResultCode.Success };
        }
        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(OrderItem entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }

    }
}
