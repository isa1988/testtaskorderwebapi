﻿using AutoMapper;
using TestTaskOrder.Core.Contract.Order;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Contract.Order;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Manager.Order
{
    public class UnitService : GeneralServiceForDeleteGuid<Unit, UnitDto, OrderUnitFields>, IUnitService
    {
        public UnitService(IUnitRepository unitRepository,
            IMapper mapper)
        : base(mapper, unitRepository)
        {
            this.unitRepository = unitRepository;
        }
        
        private readonly IUnitRepository unitRepository;

        public async Task<EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>> GetAllWithDelAsync(
            SearchByOneLineDtoFilter<OrderUnitFields> filter)
        {
            try
            {
                var orderSet = mapper.Map<PagingOrderSetting<OrderUnitFields>>(filter.OrderSetting);
                var entityList = await unitRepository.GetAllAsync(filter.Search, orderSet,
                    SettingSelectForDelete.All, GetOptionsForDetails());
                var dtoList = mapper.Map<List<UnitDto>>(entityList);
                int totalCount = await unitRepository.GetAllCountAsync(filter.Search, SettingSelectForDelete.All);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>> GetAllAsync(
            SearchByOneLineDtoFilter<OrderUnitFields> filter)
        {
            try
            {
                var orderSet = mapper.Map<PagingOrderSetting<OrderUnitFields>>(filter.OrderSetting);
                var entityList = await unitRepository.GetAllAsync(filter.Search, orderSet,
                    resolveOptions: GetOptionsForDetails());
                var dtoList = mapper.Map<List<UnitDto>>(entityList);
                int totalCount = await unitRepository.GetAllCountAsync(filter.Search);
                var retVal = GetPageInfoList(dtoList, totalCount, filter.OrderSetting);
                return EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>.Failure(ex.Message,
                    ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>.Failure(ex.Message,
                    ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<UnitDto>> EditAsync(UnitDto editDto)
        {
            await editDto.ClearSpace();
            var error = await CheckBeforeModificationAsync(editDto, false);
            if (!error.IsSuccess)
            {
                return EntityOperationResult<UnitDto>.Failure(error.Message, error.Code);
            }

            try
            {
                var entity = await unitRepository.GetByIdAsync(editDto.Id);
                entity.Name = editDto.Name;

                unitRepository.Update(entity);
                await unitRepository.SaveAsync();

                editDto = mapper.Map<UnitDto>(entity);

                return EntityOperationResult<UnitDto>.Success(editDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<UnitDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<UnitDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public override ResolveOptions GetOptionsForDetails()
        {
            return new ResolveOptions();
        }

        protected override async Task<ErrorDto> CheckBeforeModificationAsync(UnitDto value, bool isNew = true)
        {
            if (value == null)
            {
                return new ErrorDto { Message = "Пустой объект", Code = ResultCode.FieldIsNull };
            }

            if (string.IsNullOrWhiteSpace(value.Name))
            {
                return new ErrorDto { Message = "Не заполненно Наименование", Code = ResultCode.FieldIsNull };
            }

            if (isNew && await unitRepository.EqualNameAsync(value.Name))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }
            else if (!isNew && await unitRepository.EqualNameAsync(value.Name, value.Id))
            {
                return new ErrorDto { Message = "Уже есть наименование", Code = ResultCode.EqualInDb };
            }

            return new ErrorDto { Code = ResultCode.Success };
        }

        protected override async Task<ErrorDto> CheckBeforeDeleteAsync(Unit entity)
        {
            return new ErrorDto { Code = ResultCode.Success };
        }
    }
}
