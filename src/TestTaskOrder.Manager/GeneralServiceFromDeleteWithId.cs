﻿using AutoMapper;
using TestTaskOrder.Core.Contract;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Manager
{
    public abstract class GeneralServiceFromDelete<TEntity, TDto, TOrder, TUserId> 
        : GeneralService<TEntity, TDto, TOrder, TUserId>, 
            IGeneralServiceFromDelete<TDto, TOrder>
        where TEntity : class, IEntityForDelete
        where TDto : class, IServiceForDeleteDto
        where TOrder : Enum
        where TUserId : IEquatable<TUserId>
    {
        public GeneralServiceFromDelete(IMapper mapper, IRepositoryForDeleted<TEntity, TOrder> repository)
            :base(mapper, repository)
        {
            this.repositoryBaseForDeleted = repository;
        }

        protected readonly IRepositoryForDeleted<TEntity, TOrder> repositoryBaseForDeleted;
        
        public override async Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            try
            {
                PagingOrderSetting<TOrder> orderSet = null;
                if (orderSetting != null)
                {
                    orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
                }
                var entityList = await repositoryBaseForDeleted.GetAllAsync(orderSet);
                var dtoList = mapper.Map<List<TDto>>(entityList);
                int totalCount = await repositoryBaseForDeleted.GetAllCountAsync();
                var retVal = GetPageInfoList(dtoList, totalCount, orderSetting);
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.General);
            }
        }

        public override async Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            try
            {
                PagingOrderSetting<TOrder> orderSet = null;
                if (orderSetting != null)
                {
                    orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
                }
                var entityList = await repositoryBaseForDeleted.GetAllAsync(orderSet, GetOptionsForDetails());
                int totalCount = await repositoryBaseForDeleted.GetAllCountAsync();
                var retVal = GeneralTransferFromEntityFromDto<TEntity, TDto, TOrder>(entityList, totalCount, orderSetting);
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllWithDeletedAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            try
            {
                PagingOrderSetting<TOrder> orderSet = null;
                if (orderSetting != null)
                {
                    orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
                }
                var entityList = await repositoryBaseForDeleted.GetAllAsync(SettingSelectForDelete.All, orderSet);
                var dtoList = mapper.Map<List<TDto>>(entityList);
                var totalCount = await repositoryBaseForDeleted.GetAllCountAsync(SettingSelectForDelete.All);
                var dto = GetPageInfoList(dtoList, totalCount, orderSetting);
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Success(dto);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.General);
            }
        }

        public async Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllWithDeletedDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            try
            {
                PagingOrderSetting<TOrder> orderSet = null;
                if (orderSetting != null)
                {
                    orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
                }
                var entityList = await repositoryBaseForDeleted.GetAllAsync(SettingSelectForDelete.All, orderSet, GetOptionsForDetails());
                var dtoList = mapper.Map<List<TDto>>(entityList);
                var totalCount = await repositoryBaseForDeleted.GetAllCountAsync(SettingSelectForDelete.All);
                var dto = GetPageInfoList(dtoList, totalCount, orderSetting);
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Success(dto);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.General);
            }
        }

        protected async Task<List<TEntityMy>> GetListWhereAsync<TEntityMy>(List<TEntityMy> valueList, SettingSelectForDelete settingSelectFor)
        where TEntityMy : class, IEntityForDelete
        {
            if (settingSelectFor == SettingSelectForDelete.OnlyUnDelete)
            {
                valueList = valueList.Where(x => x.IsDeleted == Deleted.UnDeleted).ToList();
            }
            else if (settingSelectFor == SettingSelectForDelete.OnlyDelete)
            {
                valueList = valueList.Where(x => x.IsDeleted == Deleted.Deleted).ToList();
            }

            return valueList;
        }

    }

    public abstract class GeneralServiceFromDeleteWithId<TEntity, TDto, TId, TOrder>
        : GeneralServiceFromDelete<TEntity, TDto, TOrder, TId>, 
            IGeneralServiceFromDelete<TDto, TId, TOrder>
        where TEntity : class, IEntityForDelete<TId>
        where TDto : class, IServiceForDeleteDto<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public GeneralServiceFromDeleteWithId(IMapper mapper, IRepositoryForDeleted<TEntity, TId, TOrder> repository) 
            : base(mapper, repository)
        {
            repositoryBaseId = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        protected readonly IRepositoryForDeleted<TEntity, TId, TOrder> repositoryBaseId;

        protected abstract Task<ErrorDto> CheckBeforeDeleteAsync(TEntity entity);

        public virtual async Task<EntityOperationResult<TDto>> GetByIdAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id, SettingSelectForDelete.OnlyUnDelete);
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> GetByIdDetailsAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id, SettingSelectForDelete.OnlyUnDelete, GetOptionsForDetails());
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> DeleteItemAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id, SettingSelectForDelete.All);
                
                var error = await CheckBeforeDeleteAsync(entity);
                if (!error.IsSuccess)
                {
                    return EntityOperationResult<TDto>.Failure(error.Message, error.Code);
                }

                repositoryBaseId.Delete(entity);
                await repositoryBaseId.SaveAsync();
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> DeleteItemFromDbAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);
                
                var error = await CheckBeforeDeleteAsync(entity);
                if (!error.IsSuccess)
                {
                    return EntityOperationResult<TDto>.Failure(error.Message, error.Code);
                }
                var dto = mapper.Map<TDto>(entity);
                repositoryBaseId.DeleteFromDbAsync(entity);
                await repositoryBaseId.SaveAsync();
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }
    }

    public abstract class GeneralServiceForDeleteGuid<TEntity, TDto, TOrder> : GeneralServiceFromDeleteWithId<TEntity, TDto, Guid, TOrder>,
        IGeneralServiceFromDelete<TDto, Guid, TOrder>
        where TEntity : class, IEntityForDelete<Guid>
        where TDto : class, IServiceForDeleteDto<Guid>
        where TOrder : Enum
    {
        public GeneralServiceForDeleteGuid(IMapper mapper, IRepositoryForDeleted<TEntity, Guid, TOrder> repository)
            : base(mapper, repository)
        {
        }
    }
}
