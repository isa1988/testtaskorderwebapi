﻿using AutoMapper;
using TestTaskOrder.Core.Contract;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.ManagerContract.Contract;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Manager
{
    public abstract class GeneralService<TEntity, TDto, TOrder, TUserId> : IGeneralService<TDto, TOrder>
        where TEntity : class, IEntity
        where TDto : class, IServiceDto
        where TOrder : Enum
        where TUserId : IEquatable<TUserId>
    {
        public GeneralService(IMapper mapper, IRepository<TEntity, TOrder> repository)
        {
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            repositoryBase = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        protected readonly IMapper mapper;
        protected readonly IRepository<TEntity, TOrder> repositoryBase;
        protected abstract Task<ErrorDto> CheckBeforeModificationAsync(TDto value, bool isNew = true);

        public virtual async Task<EntityOperationResult<TDto>> CreateAsync(TDto createDto)
        {
            await createDto.ClearSpace();
            var error = await CheckBeforeModificationAsync(createDto);
            if (!error.IsSuccess)
            {
                return EntityOperationResult<TDto>.Failure(error.Message, error.Code);
            }

            try
            {
                TEntity value = mapper.Map<TEntity>(createDto);
                var entity = await repositoryBase.AddAsync(value);
                await repositoryBase.SaveAsync();

                var dto = mapper.Map<TDto>(entity);
                

                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public abstract ResolveOptions GetOptionsForDetails();
        
        public virtual async Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            try
            {
                PagingOrderSetting<TOrder> orderSet = null;
                if (orderSetting != null)
                {
                    orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
                }
                var entityList = await repositoryBase.GetAllAsync(orderSet);
                var dtoList = mapper.Map<List<TDto>>(entityList);
                int totalCount = await repositoryBase.GetAllCountAsync();
                var retVal = GetPageInfoList(dtoList, totalCount, orderSetting);
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Success(retVal);
            }

            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.General);
            }
        }

        public virtual async Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting)
        {
            try
            {
                PagingOrderSetting<TOrder> orderSet = null;
                if (orderSetting != null)
                {
                    orderSet = mapper.Map<PagingOrderSetting<TOrder>>(orderSetting);
                }
                var entityList = await repositoryBase.GetAllAsync(orderSet, GetOptionsForDetails());
                int totalCount = await repositoryBase.GetAllCountAsync();
                var retVal = GeneralTransferFromEntityFromDto<TEntity, TDto, TOrder>(entityList, totalCount, orderSetting);
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<TDto, TOrder>>.Failure(ex.Message, ResultCode.General);
            }
        }

        protected PageInfoListDto<TDtoC, TOrderField> GeneralTransferFromEntityFromDto<T, TDtoC, TOrderField>(List<T> entityList, int totalCount,
            PagingOrderSettingDto<TOrderField> orderSetting)
            where T : class
            where TDtoC : class, IServiceDto
            where TOrderField : Enum
        {
            var dtoList = mapper.Map<List<TDtoC>>(entityList);
            var retVal = GetPageInfoList(dtoList, totalCount, orderSetting);
            return retVal;
        }

        

        protected PageInfoListDto<T, TOrderField> GetPageInfoList<T, TOrderField>(List<T> value, int totalCount, PagingOrderSettingDto<TOrderField> orderSetting)
            where T : class, IServiceDto
            where TOrderField : Enum
        {
            var dto = new PageInfoListDto<T, TOrderField>
            {
                ValueList = value,
                TotalCount = totalCount,
                OrderSetting = orderSetting
            };
            return dto;
        }

    }

    public abstract class GeneralServiceWithId<TEntity, TDto, TId, TOrder> : GeneralService<TEntity, TDto, TOrder, TId>, IGeneralService<TDto, TId, TOrder>
        where TEntity : class, IEntity<TId>
        where TDto : class, IServiceDto<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public GeneralServiceWithId(IMapper mapper, IRepository<TEntity, TId, TOrder> repository) : base(mapper, repository)
        {
            repositoryBaseId = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        protected readonly IRepository<TEntity, TId, TOrder> repositoryBaseId;

        protected abstract Task<ErrorDto> CheckBeforeDeleteAsync(TEntity entity);

        public virtual async Task<EntityOperationResult<TDto>> GetByIdAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> GetByIdDetailsAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id, GetOptionsForDetails());
                var dto = mapper.Map<TDto>(entity);
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }

        public virtual async Task<EntityOperationResult<TDto>> DeleteItemAsync(TId id)
        {
            try
            {
                TEntity entity = await repositoryBaseId.GetByIdAsync(id);
                var error = await CheckBeforeDeleteAsync(entity);
                if (!error.IsSuccess)
                {
                    return EntityOperationResult<TDto>.Failure(error.Message, error.Code);
                }

                var dto = mapper.Map<TDto>(entity);
                repositoryBaseId.Delete(entity);
                await repositoryBaseId.SaveAsync();
                return EntityOperationResult<TDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<TDto>.Failure(ex.Message, ResultCode.General);
            }
        }
    }

    public abstract class GeneralServiceGuid<TEntity, TDto, TOrder> : GeneralServiceWithId<TEntity, TDto, Guid, TOrder>, IGeneralService<TDto, Guid, TOrder>
        where TEntity : class, IEntity<Guid>
        where TDto : class, IServiceDto<Guid>
        where TOrder : Enum
    {
        public GeneralServiceGuid(IMapper mapper, IRepository<TEntity, Guid, TOrder> repository) : base(mapper, repository)
        {
        }
    }
}
