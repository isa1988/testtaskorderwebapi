﻿namespace TestTaskOrder.Core.Helper
{
    public class ResolveOptions
    {
        public bool IsUser { get; set; }
        public bool IsRole { get; set; }
        public bool IsUnit { get; set; }
        public bool IsProvider { get; set; }
        public bool IsOrder { get; set; }
    }
}
