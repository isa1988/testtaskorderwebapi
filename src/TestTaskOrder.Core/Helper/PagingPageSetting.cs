﻿namespace TestTaskOrder.Core.Helper
{
    public class PagingPageSetting
    {
        public int StartPosition { get; set; }
        public int PageSize { get; set; }
    }
}
