﻿
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;

namespace TestTaskOrder.Core.Contract.Provider
{
    public interface IProviderRepository : IRepositoryForDeleted<Entities.AreaProvider.Provider, Guid, OrderProviderFields>
    {
        Task<List<Entities.AreaProvider.Provider>> GetAllAsync(string search,
            PagingOrderSetting<OrderProviderFields> pagingOrderSetting,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            ResolveOptions resolveOptions = null);

        Task<int> GetAllCountAsync(string search,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete);
        Task<bool> EqualNameAsync(string name, Guid? id = null);
    }
}
