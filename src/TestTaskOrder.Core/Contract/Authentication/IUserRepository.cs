﻿using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Core.Contract.Authentication
{
    public interface IUserRepository : IRepositoryForDeleted<User, Guid, OrderUserFields>
    {
        Task<List<User>> GetAllFromSearchAsync(string search, PagingOrderSetting<OrderUserFields> pagingOrderSetting, ResolveOptions resolveOptions = null);
        Task<int> GetAllFromSearchCountAsync(string search);
        Task<bool> IsEqualEmailAsync(string email, Guid excludingId = default(Guid));
        Task<User> GetUserAsync(string login, string password);
        string GetEncryptedPassword(string password);
    }
}
