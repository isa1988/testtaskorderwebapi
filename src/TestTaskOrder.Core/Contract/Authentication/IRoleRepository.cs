﻿using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Core.Contract.Authentication
{
    public interface IRoleRepository : IRepositoryForDeleted<Role, Guid, OrderRoleFields>
    {
        Task<Role> GetBySysNameAsync(string sysName);
        Task<bool> IsEqualsNameAsync(string name, string sysName, Guid? id = null);
    }
}
