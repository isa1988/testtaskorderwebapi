﻿using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Core.Contract.Authentication
{
    public interface ISessionRepository : IRepository<Session, Guid, OrderSessionFields>
    {
        Task<List<Session>> GetActiveByUserListAsync(List<Guid> userListId, ResolveOptions resolveOptions = null);
        Task<Session> GetActiveByIdAsync(Guid id, ResolveOptions resolveOptions = null);

        Task<Session> GetActiveByTokenAsync(string token, ResolveOptions resolveOptions = null);
        Task<Guid> GetUserIdByActiveSession(Guid sessionId);

        Task CloseAllOpenSesionAsync(Guid userId);

        Task<List<Session>> GetAllByUserRoleAsync(Guid userRoleId,
            PagingOrderSetting<OrderSessionFields> pagingOrderSetting, ResolveOptions resolveOptions = null);
    }
}
