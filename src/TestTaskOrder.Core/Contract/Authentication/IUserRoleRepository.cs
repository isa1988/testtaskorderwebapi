﻿using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.Core.Contract.Authentication
{
    public interface IUserRoleRepository : IRepository<UserRole, Guid, OrderUserRoleFields>
    {
        Task<UserRole> GetByRoleForUserAsync(Guid userId, Guid roleId, ResolveOptions resolveOption = null);
        Task<List<UserRole>> GetByUserListAsync(Guid userId, ResolveOptions resolveOption = null);
        Task<List<UserRole>> GetByUserListAsync(Guid userId, PagingOrderSetting<OrderUserRoleFields> pagingOrderSetting, ResolveOptions resolveOption = null);
        Task<int> GetByUserCountAsync(Guid userId);
    }
}
