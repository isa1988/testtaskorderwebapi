﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using TestTaskOrder.Core.Entities;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Contract
{
    public interface IRepositoryForDeleted<T, TOrder> : IRepository<T, TOrder>
        where T : class, IEntityForDelete
        where TOrder : Enum
    {
        Task<List<T>> GetAllAsync(SettingSelectForDelete settingSelectForDelete, PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null);
        Task<int> GetAllCountAsync(SettingSelectForDelete settingSelectForDelete);
        EntityEntry<T> DeleteFromDbAsync(T entity);
    }
    public interface IRepositoryForDeleted<T, TId, TOrder> : IRepositoryForDeleted<T, TOrder>
        where T : class, IEntityForDelete<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        Task<T> GetByIdAsync(TId id, ResolveOptions resolveOptions = null);

        Task<List<T>> GetAllByIdAsync(List<TId> idList,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null);
        Task<T> GetByIdAsync(TId id, SettingSelectForDelete settingSelectForDelete, ResolveOptions resolveOptions = null);
        
        Task<List<T>> GetAllByIdAsync(List<TId> idList, SettingSelectForDelete settingSelectForDelete,
            PagingOrderSetting<TOrder> pagingOrderSetting, ResolveOptions resolveOptions = null);
    }
}
