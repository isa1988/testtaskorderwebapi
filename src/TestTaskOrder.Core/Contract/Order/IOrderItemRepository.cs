﻿using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Core.Contract.Order
{
    public interface IOrderItemRepository : IRepository<OrderItem, Guid, OrderOrderItemFields>
    {
        Task<List<OrderItem>> GetAll(Guid orderId, PagingOrderSetting<OrderOrderItemFields> pagingOrderSetting,
            ResolveOptions resolveOptions = null);
        Task<int> GetAllCountAsync(Guid orderId);
        Task<bool> EqualNameAsync(string name, Guid orderId, Guid? id = null);
    }
}
