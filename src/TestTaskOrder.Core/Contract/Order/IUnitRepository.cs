﻿using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Core.Contract.Order
{
    public interface IUnitRepository : IRepositoryForDeleted<Unit, Guid, OrderUnitFields>
    {
        Task<List<Unit>> GetAllAsync(string search,
            PagingOrderSetting<OrderUnitFields> pagingOrderSetting,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            ResolveOptions resolveOptions = null);

        Task<int> GetAllCountAsync(string search,
            SettingSelectForDelete settingSelectForDelete = SettingSelectForDelete.OnlyUnDelete);
        Task<bool> EqualNameAsync(string name, Guid? id = null);
    }
}
