﻿using TestTaskOrder.Core.Filters.Order;
using TestTaskOrder.Core.Helper;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.Core.Contract.Order
{
    public interface IOrderRepository : IRepositoryForDeleted<Entities.AreaOrder.Order, Guid, OrderOrderFields>
    {
        Task<bool> EqualNameAsync(string name, Guid providerId, Guid? id = null);

        Task<List<Entities.AreaOrder.Order>> GetAllAsync(OrderFilter filter,
            SettingSelectForDelete settingSelectForDelete, PagingOrderSetting<OrderOrderFields> pagingOrderSetting,
            ResolveOptions resolveOptions = null);
        Task<int> GetAllCountAsync(OrderFilter filter, SettingSelectForDelete settingSelectForDelete);
    }
}
