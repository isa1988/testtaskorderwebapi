﻿
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Entities.AreaProvider
{
    public class Provider : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<Order> OrderList { get; set; } = new List<Order>();
    }
}
