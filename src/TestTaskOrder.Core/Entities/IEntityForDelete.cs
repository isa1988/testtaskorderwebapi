﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Entities
{
    public interface IEntityForDelete : IEntity
    {
        Deleted IsDeleted { get; set; }
    }

    public interface IEntityForDelete<TId> : IEntityForDelete
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
