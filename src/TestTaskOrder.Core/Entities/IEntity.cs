﻿namespace TestTaskOrder.Core.Entities
{
    public interface IEntity
    {
        DateTime CreateDate { get; set; }
        DateTime ModifyDate { get; set; }
    }

    public interface IEntity<TId> : IEntity 
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
