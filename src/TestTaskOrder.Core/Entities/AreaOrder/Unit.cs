﻿
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Entities.AreaOrder
{
    public class Unit : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<OrderItem> OrderItemList { get; set; } = new List<OrderItem>();
    }
}
