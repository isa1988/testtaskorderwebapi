﻿
using TestTaskOrder.Core.Entities.AreaProvider;
using TestTaskOrder.Core.Entities.Authentication;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Entities.AreaOrder
{
    public class Order : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public User Author { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public long Number { get; set; }
        public string NumberString { get; set; }
        public Provider Provider { get; set; }
        public Guid ProviderId { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<OrderItem> OrderItemList { get; set; } = new List<OrderItem>();
    }
}
