﻿
namespace TestTaskOrder.Core.Entities.AreaOrder
{
    public class OrderItem : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public Unit Unit { get; set; }
        public Guid UnitId { get; set; }
        public Order Order { get; set; }
        public Guid OrderId { get; set; }
    }
}
