﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Entities.Authentication
{
    public class Role : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public string SysName { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<UserRole> UserRoleList { get; set; } = new List<UserRole>();
    }
}
