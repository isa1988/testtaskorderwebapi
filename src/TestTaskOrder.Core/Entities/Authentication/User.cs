﻿using System.ComponentModel.DataAnnotations.Schema;
using TestTaskOrder.Core.Entities.AreaOrder;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.Core.Entities.Authentication
{
    public class User : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Login { get; set; }
        public Deleted IsDeleted { get; set; }
        
        public List<UserRole> RoleList { get; set; } = new List<UserRole>();
        public List<Session> SessionList { get; set; } = new List<Session>();
        public List<Order> OrderList { get; set; } = new List<Order>();
    }
}
