﻿namespace TestTaskOrder.Core.Filters.Order
{
    public class OrderFilter
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public Guid? ProviderId { get; set; }
        public PeriodFilter<DateTime> CreateDate { get; set; }
        public PeriodFilter<DateTime> ModifyDate { get; set; }
        public PeriodFilter<DateTime> Date { get; set; }
        public Guid? AuthorId { get; set; }
        public bool IsCurrentUser { get; set; }
        public Guid CurrentUserId { get; set; }
    }
}
