﻿namespace TestTaskOrder.Core.Filters
{
    public class PeriodFilter<T>
    where T: struct
    {
        public T? Start { get; set; }
        public T? End { get; set; }
    }
}
