﻿namespace TestTaskOrder.Core.DataBaseInitializer
{
    public interface IDataBaseInitializer
    {
        Task InitializeForDeveloperAsync();
    }
}
