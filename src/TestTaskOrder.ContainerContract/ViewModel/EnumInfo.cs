﻿namespace TestTaskOrder.ContainerContract.ViewModel
{
    public class EnumInfo
    {
        public string Title { get; set; }
        public int Code { get; set; }
    }
}
