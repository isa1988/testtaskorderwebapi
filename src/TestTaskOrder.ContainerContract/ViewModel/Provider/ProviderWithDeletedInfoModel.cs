﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.ViewModel.Provider
{
    public class ProviderWithDeletedInfoModel 
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
