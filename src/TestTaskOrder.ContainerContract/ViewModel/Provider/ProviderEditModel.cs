﻿
namespace TestTaskOrder.ContainerContract.ViewModel.Provider
{
    public class ProviderEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}
