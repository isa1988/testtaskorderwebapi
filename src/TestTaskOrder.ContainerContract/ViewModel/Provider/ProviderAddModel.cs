﻿
namespace TestTaskOrder.ContainerContract.ViewModel.Provider
{
    public class ProviderAddModel
    {
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}
