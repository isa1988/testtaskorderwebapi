﻿
namespace TestTaskOrder.ContainerContract.ViewModel.Provider
{
    public class ProviderInfoModel
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }
}
