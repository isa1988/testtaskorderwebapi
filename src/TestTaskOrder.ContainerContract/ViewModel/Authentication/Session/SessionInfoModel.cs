﻿using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.Session
{
    public class SessionInfoModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserInfoModel User { get; set; }
        public Guid? RoleId { get; set; }
        public RoleInfoModel Role { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }
    }
}
