﻿using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.Session
{
    public class SessionWithTokenInfoModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserInfoModel User { get; set; }
        public Guid? RoleId { get; set; }
        public RoleInfoModel Role { get; set; }
        public SessionStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }
        public string Token { get; set; }
    }
}
