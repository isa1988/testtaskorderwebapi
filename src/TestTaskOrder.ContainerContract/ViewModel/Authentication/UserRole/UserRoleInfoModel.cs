﻿using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole
{
    public class UserRoleInfoModel
    {
        public Guid Id { get; set; }
        public Guid RoleId { get; set; }
        public RoleInfoModel Role { get; set; }
        public Guid UserId { get; set; }
        public UserInfoModel User { get; set; }
    }
}
