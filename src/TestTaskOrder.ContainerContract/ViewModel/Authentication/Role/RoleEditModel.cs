﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.Role
{
    public class RoleEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SysName { get; set; }
    }
}
