﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.Role
{
    public class RoleAddModel
    {
        public string Name { get; set; }
        public string SysName { get; set; }
    }
}
