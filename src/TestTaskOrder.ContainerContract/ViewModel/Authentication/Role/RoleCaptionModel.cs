﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.Role
{
    public class RoleCaptionModel
    {
        public string Title { get; set; }
    }
}
