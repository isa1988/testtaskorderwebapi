﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.Role
{
    public class RoleWithDeletedInfoModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SysName { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
