﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class LoginModel
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
