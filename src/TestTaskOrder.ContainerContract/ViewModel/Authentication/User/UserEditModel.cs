﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class UserEditModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
