﻿using System.Text.Json.Serialization;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class LoginConfirmRoleModel
    {
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public string RoleName { get; set; }
        public string RoleSys { get; set; }
    }
}
