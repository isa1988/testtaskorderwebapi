﻿using System.Text.Json.Serialization;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class CurrentUserEditModel
    {
        [JsonIgnore]
        public Guid Id { get; set; }
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
