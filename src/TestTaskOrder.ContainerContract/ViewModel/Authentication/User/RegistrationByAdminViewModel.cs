﻿using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class RegistrationByAdminViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Login { get; set; }
        public List<RoleInfoModel> RoleList { get; set; }
    }
}
