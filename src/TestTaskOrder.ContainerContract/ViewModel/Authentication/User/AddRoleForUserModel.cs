﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class AddRoleForUserModel
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}
