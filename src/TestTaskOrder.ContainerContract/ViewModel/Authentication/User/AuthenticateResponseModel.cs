﻿using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class AuthenticateResponseModel
    {
        public AuthenticateResponseModel()
        {
            Roles = new List<RoleInfoModel>();
        }
        public List<RoleInfoModel> Roles { get; set; }
        public string Token { get; set; }
    }
}
