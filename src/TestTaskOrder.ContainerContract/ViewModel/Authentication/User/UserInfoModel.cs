﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class UserInfoModel
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
    }
}
