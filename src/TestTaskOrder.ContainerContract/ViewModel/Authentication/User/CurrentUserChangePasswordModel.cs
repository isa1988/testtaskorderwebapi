﻿using System.Text.Json.Serialization;

namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class CurrentUserChangePasswordModel
    {
        [JsonIgnore]
        public Guid Id { get; set; }
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public string PasswordCurrent { get; set; }
        public string PasswordNew { get; set; }
        public string PasswordNewConfirm { get; set; }
    }
}
