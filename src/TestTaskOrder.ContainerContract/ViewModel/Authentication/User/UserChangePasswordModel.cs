﻿namespace TestTaskOrder.ContainerContract.ViewModel.Authentication.User
{
    public class UserChangePasswordModel
    {
        public Guid Id { get; set; }
        public string PasswordNew { get; set; }
        public string PasswordNewConfirm { get; set; }
    }
}
