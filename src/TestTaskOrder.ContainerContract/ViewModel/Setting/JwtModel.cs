﻿
namespace TestTaskOrder.ContainerContract.ViewModel.Setting
{
    public class JwtModel
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }
}
