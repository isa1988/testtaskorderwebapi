﻿
namespace TestTaskOrder.ContainerContract.ViewModel.Order.Unit
{
    public class UnitEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
