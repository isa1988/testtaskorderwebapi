﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.ViewModel.Order.Unit
{
    public class UnitWithDeletedInfoModel 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
