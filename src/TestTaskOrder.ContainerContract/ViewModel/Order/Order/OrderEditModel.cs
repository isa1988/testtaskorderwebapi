﻿
namespace TestTaskOrder.ContainerContract.ViewModel.Order.Order
{
    public class OrderEditModel
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public Guid ProviderId { get; set; }
    }
}
