﻿using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Provider;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.ViewModel.Order.Order
{
    public class OrderWithDeletedInfoModel 
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public UserInfoModel Author { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public long Number { get; set; }
        public string NumberString { get; set; }
        public ProviderInfoModel Provider { get; set; }
        public Guid ProviderId { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
