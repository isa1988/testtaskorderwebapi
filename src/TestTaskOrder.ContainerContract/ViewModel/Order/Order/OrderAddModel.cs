﻿using System.Text.Json.Serialization;

namespace TestTaskOrder.ContainerContract.ViewModel.Order.Order
{
    public class OrderAddModel
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public Guid AuthorId { get; set; }
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public Guid ProviderId { get; set; }
    }
}
