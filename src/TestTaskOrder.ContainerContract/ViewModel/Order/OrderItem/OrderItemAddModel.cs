﻿namespace TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem
{
    public class OrderItemAddModel
    {
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public Guid UnitId { get; set; }
        public Guid OrderId { get; set; }
    }
}
