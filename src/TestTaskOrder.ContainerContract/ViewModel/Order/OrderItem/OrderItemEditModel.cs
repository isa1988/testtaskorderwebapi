﻿namespace TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem
{
    public class OrderItemEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public Guid UnitId { get; set; }
    }
}
