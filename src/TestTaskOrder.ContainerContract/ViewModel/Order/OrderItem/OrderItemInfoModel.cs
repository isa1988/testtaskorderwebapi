﻿using TestTaskOrder.ContainerContract.ViewModel.Order.Order;
using TestTaskOrder.ContainerContract.ViewModel.Order.Unit;

namespace TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem
{
    public class OrderItemInfoModel 
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public UnitInfoModel Unit { get; set; }
        public Guid UnitId { get; set; }
        public OrderInfoModel Order { get; set; }
        public Guid OrderId { get; set; }
    }
}
