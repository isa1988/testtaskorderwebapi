﻿using TestTaskOrder.ContainerContract.Filters.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ContainerContract.Contract.Authentication
{
    public interface IUserRoleContainer
    {
        Task<OperationResultModel<PageInfoListModel<UserRoleInfoModel, OrderUserRoleFields>>> GetAllForUserAsync(
            UserRoleFilter filter, bool isFromDb = false);
    }
}
