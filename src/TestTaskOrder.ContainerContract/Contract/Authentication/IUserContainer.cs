﻿using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.Filters.Authentication;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ContainerContract.Contract.Authentication
{
    public interface IUserContainer
    {
        Task<List<EnumInfo>> GetInfoOrderUserAsync();

        Task<OperationResultModel<PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>>> GetAllWithLastStateAsync(
            PagingOrderSettingModel<OrderUserFields> orderSettingModel, bool isFromDb = false);

        Task<OperationResultModel<PageInfoListModel<UserWithDeletedInfoModel, OrderUserFields>>> GetAllWithLastStateFromSearchAsync(
            SearchByOneLineFilter<OrderUserFields> filter, bool isFromDb = false);
        Task<OperationResultModel<UserWithDeletedInfoModel>> GetByIdAsync(Guid id, bool isFromDb = false);
        Task<OperationResultModel<UserCurrentInfoModel>> GetInfoCurrentUser(Guid sessionId, bool isFromDb = false);
        Task<OperationResultModel<AuthenticateResponseModel>> LoginAsync(LoginModel request, bool isFromDb = false);
        Task<OperationResultModel<bool>> LogoutAsync(Guid sessionId, bool isFromDb = false);

        Task<OperationResultModel<PageInfoListModel<RoleInfoModel, OrderRoleFields>>> GetRoleExcludeUserRoleAsync(
            RoleFilter filter, bool isFromDb = false);
        Task<OperationResultModel<bool>> RoleConfirmAsync(LoginConfirmRoleModel request, bool isFromDb = false);
        
        Task<OperationResultModel<UserWithDeletedInfoModel>> RegistrationByAdminAsync(RegistrationByAdminViewModel registrationModel, bool isFromDb = false);

        Task<OperationResultModel<UserWithDeletedInfoModel>> EditAsync(UserEditModel editModel, bool isFromDb = false);
        Task<OperationResultModel<UserWithDeletedInfoModel>> EditCurrentAsync(CurrentUserEditModel editModel, bool isFromDb = false);
        Task<OperationResultModel<bool>> ChangePasswordAsync(UserChangePasswordModel changePasswordModel);
        Task<OperationResultModel<bool>> ChangePasswordCurrentAsync(CurrentUserChangePasswordModel changePasswordModel, bool isFromDb = false);
        Task<OperationResultModel<UserWithDeletedInfoModel>> DeleteAsync(Guid id, bool isFromDb = false);
        Task<OperationResultModel<UserRoleInfoModel>> AddRoleForUserAsync(AddRoleForUserModel addRoleForUserModel, bool isFromDb = false);
        Task<OperationResultModel<UserRoleInfoModel>> DeleteRoleForUserAsync(Guid userRoleId, bool isFromDb = false);
        Task<OperationResultModel<UserCaptionModel>> GetUserCaptionAsync(Guid sessionId, bool isFromDb = false);

        Task<List<EnumInfo>> GetInfoOrderRoleAsync();
    }
}
