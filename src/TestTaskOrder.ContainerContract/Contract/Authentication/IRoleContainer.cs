﻿using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ContainerContract.Contract.Authentication
{
    public interface IRoleContainer
    {
        Task<OperationResultModel<PageInfoListModel<RoleInfoModel, OrderRoleFields>>> GetAllAsync(
            PagingOrderSettingModel<OrderRoleFields> orderSettingModel, bool isFromDb = false);
        Task<OperationResultModel<PageInfoListModel<RoleWithDeletedInfoModel, OrderRoleFields>>> GetAllWithDeletedAsync(
            PagingOrderSettingModel<OrderRoleFields> orderSettingModel, bool isFromDb = false);
        Task<OperationResultModel<RoleWithDeletedInfoModel>> GetIdAsync(Guid id, bool isFromDb = false);

        Task<OperationResultModel<RoleWithDeletedInfoModel>> CreateAsync(RoleAddModel request, bool isFromDb = false);
        Task<OperationResultModel<RoleWithDeletedInfoModel>> EditAsync(RoleEditModel request, bool isFromDb = false);
    }
}
