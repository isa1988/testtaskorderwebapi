﻿using TestTaskOrder.ContainerContract.Filters.Order;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.Order;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ContainerContract.Contract.Order
{
    public interface IOrderContainer
    {
        Task<List<EnumInfo>> GetInfoOrderAsync();

        Task<OperationResultModel<PageInfoListModel<OrderWithDeletedInfoModel, OrderOrderFields>>>
            GetAllWithDeletedAsync(OrderFilterModel filter);
        Task<OperationResultModel<OrderWithDeletedInfoModel>> GetByIdAsync(Guid id);

        Task<OperationResultModel<OrderInfoModel>> AddAsync(OrderAddModel request);
        Task<OperationResultModel<OrderInfoModel>> EditAsync(OrderEditModel request);
        Task<OperationResultModel<OrderWithDeletedInfoModel>> DeleteAsync(Guid id);
    }
}
