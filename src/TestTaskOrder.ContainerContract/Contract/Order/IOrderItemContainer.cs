﻿using TestTaskOrder.ContainerContract.Filters.Order;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.OrderItem;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ContainerContract.Contract.Order
{
    public interface IOrderItemContainer
    {
        Task<List<EnumInfo>> GetInfoOrderItemAsync();
        
        Task<OperationResultModel<PageInfoListModel<OrderItemInfoModel, OrderOrderItemFields>>>
            GetAllAsync(OrderItemFilterModel filter);
        Task<OperationResultModel<OrderItemInfoModel>> GetByIdAsync(Guid id);

        Task<OperationResultModel<OrderItemInfoModel>> AddAsync(OrderItemAddModel request);
        Task<OperationResultModel<OrderItemInfoModel>> EditAsync(OrderItemEditModel request);
        Task<OperationResultModel<OrderItemInfoModel>> DeleteAsync(Guid id);
    }
}
