﻿using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Order.Unit;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ContainerContract.Contract.Order
{
    public interface IUnitContainer
    {
        Task<List<EnumInfo>> GetInfoUnitAsync();

        Task<OperationResultModel<PageInfoListModel<UnitWithDeletedInfoModel, OrderUnitFields>>>
            GetAllWithDeletedAsync(SearchByOneLineFilter<OrderUnitFields> order);
        Task<OperationResultModel<PageInfoListModel<UnitInfoModel, OrderUnitFields>>>
            GetAllAsync(SearchByOneLineFilter<OrderUnitFields> order);
        Task<OperationResultModel<UnitWithDeletedInfoModel>> GetByIdAsync(Guid id);

        Task<OperationResultModel<UnitInfoModel>> AddAsync(UnitAddModel request);
        Task<OperationResultModel<UnitInfoModel>> EditAsync(UnitEditModel request);
        Task<OperationResultModel<UnitWithDeletedInfoModel>> DeleteAsync(Guid id);
    }
}
