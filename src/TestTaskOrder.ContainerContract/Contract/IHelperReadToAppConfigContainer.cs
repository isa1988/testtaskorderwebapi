﻿namespace TestTaskOrder.ContainerContract.Contract
{
    public interface IHelperReadToAppConfigContainer
    {
        Task LoadAsync();

        Task UpdateRoleAndUser();
        Task UpdateRole();
    }
}
