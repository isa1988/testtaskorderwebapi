﻿using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Role;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.Session;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.User;
using TestTaskOrder.ContainerContract.ViewModel.Authentication.UserRole;

namespace TestTaskOrder.ContainerContract.Contract
{
    public interface IAppConfiguration
    {
        int PageSize { get; set; }
        List<UserInfoModel> UserList { get; set; }
        List<UserRoleInfoModel> UserRoleList { get; set; }
        List<RoleWithDeletedInfoModel> RoleList { get; set; }
        List<SessionWithTokenInfoModel> SessionList { get; set; }
        
        Task LoginAsync(SessionWithTokenInfoModel email);
        Task LogoutAsync(Guid sessionId);
        Task<UserInfoModel> GetUserByIdAsync(Guid userId);
        Task<UserInfoModel> GetUserBySessionIdAsync(Guid sessionId);
        Task<RoleWithDeletedInfoModel> GetRoleBySessionIdAsync(Guid sessionId);
        Task<Guid> GetUserIdBySessionIdAsync(Guid sessionId);
        Task<SessionInfoModel> GetSessionByTokenAsync(string token);
        Task<SessionInfoModel> GetSessionByIdAsync(Guid id);
        Task SetPageSize(int pageSize);
        Task RoleConfirmAsync(Guid sessionId, Guid? roleId);
        Task RegistrationAsync(UserInfoModel user, List<UserRoleInfoModel> userRoleList);
        Task EditUserAsync(UserInfoModel user);
        Task DeleteUserAsync(Guid userId);
        Task AddUserRoleAsync(UserRoleInfoModel userRoleInfoModel);
        Task DeleteUserRoleAsync(Guid userRoleId);
        Task SetRoleAsync(RoleWithDeletedInfoModel roleModel);
    }
}