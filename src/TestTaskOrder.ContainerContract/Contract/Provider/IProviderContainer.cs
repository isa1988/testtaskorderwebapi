﻿
using TestTaskOrder.ContainerContract.Filters;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.ContainerContract.ViewModel.Provider;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;

namespace TestTaskOrder.ContainerContract.Contract.Provider
{
    public interface IProviderContainer
    {
        Task<List<EnumInfo>> GetInfoProviderAsync();

        Task<OperationResultModel<PageInfoListModel<ProviderWithDeletedInfoModel, OrderProviderFields>>>
            GetAllWithDeletedAsync(SearchByOneLineFilter<OrderProviderFields> filter);
        Task<OperationResultModel<PageInfoListModel<ProviderInfoModel, OrderProviderFields>>>
            GetAllAsync(SearchByOneLineFilter<OrderProviderFields> filter);
        Task<OperationResultModel<ProviderWithDeletedInfoModel>> GetByIdAsync(Guid id);

        Task<OperationResultModel<ProviderInfoModel>> AddAsync(ProviderAddModel request);
        Task<OperationResultModel<ProviderInfoModel>> EditAsync(ProviderEditModel request);
        Task<OperationResultModel<ProviderWithDeletedInfoModel>> DeleteAsync(Guid id);
    }
}
