﻿namespace TestTaskOrder.ContainerContract.Filters
{
    public class PeriodFilterModel<T>
    where T: struct
    {
        public T? Start { get; set; }
        public T? End { get; set; }
    }
}
