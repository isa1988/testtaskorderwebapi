﻿using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ContainerContract.Filters.Authentication
{
    public class RoleFilter
    {
        public Guid UserId { get; set; }
        public PagingOrderSettingModel<OrderRoleFields> OrderSetting { get; set; }
    }
}
