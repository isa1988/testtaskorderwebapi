﻿using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ContainerContract.Filters.Authentication
{
    public class UserRoleFilter 
    {
        public Guid UserId { get; set; }
        public PagingOrderSettingModel<OrderUserRoleFields> OrderSetting { get; set; }
    }
}
