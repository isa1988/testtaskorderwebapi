﻿using System.Text.Json.Serialization;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.Filters.Authentication
{
    public class UserPropertyFilter
    {
        public string Name { get; set; }
        
        [JsonIgnore]
        public Guid UserId { get; set; }
        [JsonIgnore]
        public Guid SessionId { get; set; }
        
    }
}
