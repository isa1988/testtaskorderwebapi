﻿using System.Text.Json.Serialization;
using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ContainerContract.Filters.Order
{
    public class OrderFilterModel
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public Guid? ProviderId { get; set; }
        public PeriodFilterModel<DateTime> CreateDate { get; set; }
        public PeriodFilterModel<DateTime> ModifyDate { get; set; }
        public PeriodFilterModel<DateTime> Date { get; set; }
        public Guid? AuthorId { get; set; }
        public bool IsCurrentUser { get; set; }
        [JsonIgnore]
        public Guid CurrentUserId { get; set; }
        [JsonIgnore]
        public Guid SessionId { get; set; }
        public PagingOrderSettingModel<OrderOrderFields> OrderSetting { get; set; }
        public async Task ClearSpace()
        {
            Number = Number.TrimWithNull();
            Name = Name.TrimWithNull();
        }
    }
}
