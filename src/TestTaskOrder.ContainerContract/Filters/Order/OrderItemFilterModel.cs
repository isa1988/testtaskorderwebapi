﻿using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ContainerContract.Filters.Order
{
    public class OrderItemFilterModel
    {
        public Guid OrderId { get; set; }
        public PagingOrderSettingModel<OrderOrderItemFields> OrderSetting { get; set; }
    }
}
