﻿using TestTaskOrder.ContainerContract.ViewModel;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ContainerContract.Filters
{
    public class SearchByOneLineFilter<T> 
    where T : Enum
    {
        public string Search { get; set; }
        public PagingOrderSettingModel<T> OrderSetting { get; set; }
    }
}
