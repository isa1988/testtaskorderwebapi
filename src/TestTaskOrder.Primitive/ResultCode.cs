﻿namespace TestTaskOrder.Primitive
{
    public enum ResultCode
    {
        Success = 0,
        FieldIsNull = 1,
        LineIsNotSearch = 2,
        EqualInDb = 3,
        DoesNotMatch = 4,
        General = 5,
        Information = 6,
    }
}
