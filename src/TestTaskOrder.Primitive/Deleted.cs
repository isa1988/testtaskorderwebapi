﻿using System.ComponentModel;

namespace TestTaskOrder.Primitive
{
    public enum Deleted : int
    {
        [Description("Не помечанный на удаление")]
        UnDeleted = 0,

        [Description("Помечанный на удаление")]
        Deleted = 1,
    }
}
