﻿
namespace TestTaskOrder.Primitive
{
    public enum SettingSelectForDelete : int
    {
        All = 0,
        OnlyDelete,
        OnlyUnDelete,
    }
}
