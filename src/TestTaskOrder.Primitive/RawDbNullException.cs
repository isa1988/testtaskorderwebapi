﻿namespace TestTaskOrder.Primitive
{
    public class RawDbNullException : NullReferenceException
    {
        public RawDbNullException(string error) : base(error)
        {
            
        }
    }
}
