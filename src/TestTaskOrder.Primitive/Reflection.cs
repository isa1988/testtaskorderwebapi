﻿
namespace TestTaskOrder.Primitive
{
    public static class Reflection
    {
        public static T GetPropValue<T>(object src, string propName)
        {
            return (T)src.GetType().GetProperty(propName).GetValue(src, null);
        }
    }
}
