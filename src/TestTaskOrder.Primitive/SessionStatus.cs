﻿
namespace TestTaskOrder.Primitive
{
    public enum SessionStatus : int
    {
        Active = 0,
        Closed = 1
    }
}
