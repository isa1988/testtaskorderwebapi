﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskOrder.Primitive.PagingOrderSettings.Order
{
    public enum OrderUnitFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По имени")]
        Name
    }
}
