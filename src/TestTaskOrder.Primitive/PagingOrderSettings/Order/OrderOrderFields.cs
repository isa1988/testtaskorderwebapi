﻿

using System.ComponentModel.DataAnnotations;

namespace TestTaskOrder.Primitive.PagingOrderSettings.Order
{
    public enum OrderOrderFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "Наименоание")]
        Name,

        [Display(Name = "Номер")]
        Number,

        [Display(Name = "Дата создания")]
        CreateDate,

        [Display(Name = "Дата редакирования")]
        ModifyDate,

        [Display(Name = "Дата")]
        Date,

        [Display(Name = "Пользователь")]
        User,

        [Display(Name = "Поставщик")]
        Provider,

    }
}
