﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskOrder.Primitive.PagingOrderSettings.Order
{
    public enum OrderOrderItemFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "Наименоание")]
        Name
    }
}
