﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskOrder.Primitive.PagingOrderSettings.Authentication
{
    public enum OrderRoleFields 
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "Наименоание")]
        Name
    }
}
