﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskOrder.Primitive.PagingOrderSettings.Authentication
{
    public enum OrderUserFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "Фамилия")]
        SurName,
        
        [Display(Name = "По эл. почте")]
        Email
    }
}
