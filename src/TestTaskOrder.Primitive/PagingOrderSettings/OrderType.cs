﻿namespace TestTaskOrder.Primitive.PagingOrderSettings
{
    public enum OrderType
    {
        ASC,
        DESC
    }
}
