﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskOrder.Primitive.PagingOrderSettings.Provider
{
    public enum OrderProviderFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "Наименоание")]
        Name
    }
}
