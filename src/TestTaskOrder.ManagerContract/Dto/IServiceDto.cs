﻿namespace TestTaskOrder.ManagerContract.Dto
{
    public interface IServiceDto
    {
        Task ClearSpace();
    }
    public interface IServiceDto<TId> : IServiceDto 
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
