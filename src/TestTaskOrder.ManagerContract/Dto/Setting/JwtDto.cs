﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Setting
{
    public class JwtDto : IServiceDto
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public async Task ClearSpace()
        {
            Key = Key.TrimWithNull();
            Issuer = Issuer.TrimWithNull();
        }
    }
}
