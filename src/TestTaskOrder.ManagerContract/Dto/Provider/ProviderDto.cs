﻿using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Provider
{
    public class ProviderDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<OrderDto> OrderList { get; set; } = new List<OrderDto>();
        public async Task ClearSpace()
        {
            Name = Name.TrimWithNull();
            Comment = Comment.TrimWithNull();
        }
    }
}
