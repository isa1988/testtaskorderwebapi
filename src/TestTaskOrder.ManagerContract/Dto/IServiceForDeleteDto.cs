﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto
{
    public interface IServiceForDeleteDto : IServiceDto
    {
        Deleted IsDeleted { get; set; }
    }
    public interface IServiceForDeleteDto<TId> : IServiceForDeleteDto, IServiceDto<TId>
        where TId : IEquatable<TId>
    {
    }
}
