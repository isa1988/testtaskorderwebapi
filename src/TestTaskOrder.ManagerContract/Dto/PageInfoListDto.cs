﻿using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;

namespace TestTaskOrder.ManagerContract.Dto
{
    public class PageInfoListDto<T, TOrder> : IServiceDto
        where T : class
        where TOrder : Enum
    {
        public int TotalCount { get; set; }
        public List<T> ValueList { get; set; }
        public PagingOrderSettingDto<TOrder> OrderSetting { get; set; }

        public async Task ClearSpace()
        {

        }
    }
}
