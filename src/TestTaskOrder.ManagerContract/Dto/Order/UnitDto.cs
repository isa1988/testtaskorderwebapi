﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Order
{
    public class UnitDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<OrderItemDto> OrderItemList { get; set; } = new List<OrderItemDto>();
        public async Task ClearSpace()
        {
            Name = Name.TrimWithNull();
        }
    }
}
