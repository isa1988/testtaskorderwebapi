﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Order
{
    public class OrderItemDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public UnitDto Unit { get; set; }
        public Guid UnitId { get; set; }
        public OrderDto Order { get; set; }
        public Guid OrderId { get; set; }
        public async Task ClearSpace()
        {
            Name = Name.TrimWithNull();
        }
    }
}
