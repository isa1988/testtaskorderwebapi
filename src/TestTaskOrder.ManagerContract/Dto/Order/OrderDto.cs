﻿using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.ManagerContract.Dto.Provider;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Order
{
    public class OrderDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public UserDto Author { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public long Number { get; set; }
        public string NumberString { get; set; }
        public ProviderDto Provider { get; set; }
        public Guid ProviderId { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<OrderItemDto> OrderItemList { get; set; } = new List<OrderItemDto>();
        public async Task ClearSpace()
        {
            Name = Name.TrimWithNull();
            NumberString = NumberString.TrimWithNull();
        }
    }
}
