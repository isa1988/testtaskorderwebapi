﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto
{
    public class ErrorDto : IServiceDto
    {
        public string Message { get; set; }
        public ResultCode Code { get; set; }

        public bool IsSuccess
        {
            get
            {
                return Code == ResultCode.Success;
            }
        }

        public async Task ClearSpace()
        {
            Message = Message.TrimWithNull();
        }
    }
}
