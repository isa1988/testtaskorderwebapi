﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class AuthenticateResponseDto : IServiceDto
    {
        public Guid SessionId { get; set; }
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public Guid? RoleId { get; set; }
        public List<RoleDto> Roles { get; set; } = new List<RoleDto>();
        public async Task ClearSpace()
        {
            Token = Token.TrimWithNull();

            if (Roles?.Count > 0)
            {
                foreach (var role in Roles)
                {
                    await role.ClearSpace();
                }
            }
        }
    }
}
