﻿

using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class UserChangePasswordDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public Guid SessionId { get; set; }
        public bool IsCheck { get; set; }
        public string PasswordCurrent { get; set; }
        public string PasswordNew { get; set; }
        public string PasswordNewConfirm { get; set; }
        public async Task ClearSpace()
        {
            PasswordCurrent = PasswordCurrent.TrimWithNull();
            PasswordNew = PasswordNew.TrimWithNull();
            PasswordNewConfirm = PasswordNewConfirm.TrimWithNull();
        }
    }
}
