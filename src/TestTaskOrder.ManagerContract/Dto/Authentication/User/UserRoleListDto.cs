﻿namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class UserRoleListDto : IServiceDto
    {
        public List<UserRoleDto> UserRoleList { get; set; } = new List<UserRoleDto>();

        public async Task ClearSpace()
        {
            if (UserRoleList?.Count > 0)
            {
                foreach (var userRole in UserRoleList)
                {
                    await userRole.ClearSpace();
                }
            }
        }
    }
}