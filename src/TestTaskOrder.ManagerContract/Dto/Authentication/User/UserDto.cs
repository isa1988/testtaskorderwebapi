﻿using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class UserDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Login { get; set; }
        public Deleted IsDeleted { get; set; }
        
        public List<UserRoleDto> RoleList { get; set; } = new List<UserRoleDto>();
        public List<SessionDto> SessionList { get; set; } = new List<SessionDto>();
        public List<OrderDto> OrderList { get; set; } = new List<OrderDto>();


        public async Task ClearSpace()
        {
            FirstName = FirstName.TrimWithNull();
            MiddleName = MiddleName.TrimWithNull();
            LastName = LastName.TrimWithNull();
            Phone = Phone.TrimWithNull();
            Email = Email.TrimWithNull();
            Password = Password.TrimWithNull();
            Login = Login.TrimWithNull();
        }
    }
}
