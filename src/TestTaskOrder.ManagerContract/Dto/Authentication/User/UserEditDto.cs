﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class UserEditDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public async Task ClearSpace()
        {
            FirstName = FirstName.TrimWithNull();
            MiddleName = MiddleName.TrimWithNull();
            LastName = LastName.TrimWithNull();
            Phone = Phone.TrimWithNull();
        }
    }
}
