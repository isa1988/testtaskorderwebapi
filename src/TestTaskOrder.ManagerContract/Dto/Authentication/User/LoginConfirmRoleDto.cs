﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class LoginConfirmRoleDto : IServiceDto
    {
        public Guid SessionId { get; set; }
        public string RoleSys { get; set; }

        public async Task ClearSpace()
        {
            RoleSys = RoleSys.TrimWithNull();
        }
    }
}
