﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class LoginDto : IServiceDto
    {
        public string Login { get; set; }

        public string Password { get; set; }
        public async Task ClearSpace()
        {
            Login = Login.TrimWithNull();
            Password = Password.TrimWithNull();
        }
    }
}
