﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class RegistrationDto : IServiceDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Login { get; set; }
        public List<RoleDto> RoleList { get; set; } = new List<RoleDto>();
        public Guid SessionId { get; set; }
        public async Task ClearSpace()
        {
            Email = Email.TrimWithNull();
            Password = Password.TrimWithNull();
            PasswordConfirm = PasswordConfirm.TrimWithNull();
            FirstName = FirstName.TrimWithNull();
            MiddleName = MiddleName.TrimWithNull();
            LastName = LastName.TrimWithNull();
            Phone = Phone.TrimWithNull();
            Login = Login.TrimWithNull();

            if (RoleList?.Count > 0)
            {
                foreach (var role in RoleList)
                {
                    await role.ClearSpace();
                }
            }
        }
    }
}
