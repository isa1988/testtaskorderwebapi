﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication.User
{
    public class UserCaptionDto : IServiceDto
    {
        public string Title { get; set; }
        public async Task ClearSpace()
        {
            Title = Title.TrimWithNull();
        }
    }
}
