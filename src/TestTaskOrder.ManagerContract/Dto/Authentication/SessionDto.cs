﻿using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication
{
    public class SessionDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid UserId { get; set; }
        public UserDto User { get; set; }
        public Guid? RoleId { get; set; }
        public UserRoleDto UserRole { get; set; }
        public string Token { get; set; }
        public SessionStatus Status { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Info { get; set; }


        public async Task ClearSpace()
        {
            Token = Token.TrimWithNull();
            Info = Info.TrimWithNull();
        }
    }
}
