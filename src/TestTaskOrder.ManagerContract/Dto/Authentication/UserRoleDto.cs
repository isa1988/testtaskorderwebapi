﻿
using TestTaskOrder.ManagerContract.Dto.Authentication.User;

namespace TestTaskOrder.ManagerContract.Dto.Authentication
{
    public class UserRoleDto : IServiceDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid RoleId { get; set; }
        public RoleDto Role { get; set; }
        public Guid UserId { get; set; }
        public UserDto User { get; set; }
        public async Task ClearSpace()
        {
            
        }
    }
}
