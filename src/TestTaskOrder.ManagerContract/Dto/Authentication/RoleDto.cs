﻿using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Dto.Authentication
{
    public class RoleDto : IServiceForDeleteDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; }
        public string SysName { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<UserRoleDto> UserRoleList { get; set; } = new List<UserRoleDto>();
        public async Task ClearSpace()
        {
            Name = Name.TrimWithNull();
            SysName = SysName.TrimWithNull();
        }
    }
}
