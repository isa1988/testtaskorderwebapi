﻿using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ManagerContract.Contract.Authentication
{
    public interface IUserRoleService : IGeneralService<UserRoleDto, Guid, OrderUserRoleFields>
    {
    }
}
