﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ManagerContract.Contract.Authentication
{
    public interface ISessionService : IGeneralService<SessionDto, Guid, OrderSessionFields>
    {
        Task<EntityOperationResult<PageInfoListDto<SessionDto, OrderSessionFields>>> GetActiveByUserListAsync(List<Guid> userListId);
        /// <summary>
        /// Активная сессия
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<SessionDto>> GetActiveByIdAsync(Guid id);

        /// <summary>
        /// Активная сессия со всеми зависимостями
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<SessionDto>> GetActiveByIdDetailsAsync(Guid id);

        /// <summary>
        /// Активная сессия
        /// </summary>
        /// <param name="token">Токен</param>
        /// <returns></returns>
        
        Task<EntityOperationResult<SessionDto>> GetActiveByTokenAsync(string token);
        /// <summary>
        /// Активная сессия со всеми зависимостями
        /// </summary>
        /// <param name="token">Токен</param>
        /// <returns></returns>
        Task<EntityOperationResult<SessionDto>> GetActiveByTokenDetailAsync(string token);
        
        Task<EntityOperationResult<SessionDto>> DeleteByUserRoleAsync(Guid userRoleId);
    }
}
