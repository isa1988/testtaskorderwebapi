﻿using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ManagerContract.Contract.Authentication
{
    public interface IRoleService : IGeneralServiceFromDelete<RoleDto, Guid, OrderRoleFields>
    {
        Task<EntityOperationResult<RoleDto>> EditAsync(RoleDto editDto);
    }
}
