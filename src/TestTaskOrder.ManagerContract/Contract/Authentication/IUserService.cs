﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Authentication;
using TestTaskOrder.ManagerContract.Dto.Authentication.User;
using TestTaskOrder.ManagerContract.Dto.Setting;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.ManagerContract.Filters.Authentication;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ManagerContract.Contract.Authentication
{
    public interface IUserService : IGeneralServiceFromDelete<UserDto, Guid, OrderUserFields>
    {
        Task<EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>> GetAllFromSearchAsync(
            SearchByOneLineDtoFilter<OrderUserFields> filter);

        Task<EntityOperationResult<PageInfoListDto<UserDto, OrderUserFields>>> GetAllFromSearchDetailAsync(
            SearchByOneLineDtoFilter<OrderUserFields> filter);

        Task<EntityOperationResult<AuthenticateResponseDto>> LoginAsync(LoginDto login, JwtDto jwtDto);
        Task<EntityOperationResult<UserDto>> LogoutAsync(Guid sessionId);
        Task<EntityOperationResult<SessionDto>> RoleSelectAsync(LoginConfirmRoleDto model);
        Task<EntityOperationResult<UserDto>> RegistrationByAdminAsync(RegistrationDto registration);
        Task<EntityOperationResult<UserDto>> EditAsync(UserEditDto editDto);
        Task<EntityOperationResult<UserDto>> ChangePasswordAsync(UserChangePasswordDto changePasswordDto);
        Task<EntityOperationResult<UserCaptionDto>> GetUserCaptionAsync(Guid sessionId);

        Task<EntityOperationResult<PageInfoListDto<RoleDto, OrderRoleFields>>> GetRoleExcludeUserRoleAsync(
            RoleFilterDto filter);

        Task<EntityOperationResult<UserDto>> GetUserIdBySessionIdAsync(Guid sessionId);
    }
}
