﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Contract
{
    public class EntityOperationResult<T>
        where T : IServiceDto
    {
        private EntityOperationResult(T entity)
        {
            Entity = entity;
        }

        private EntityOperationResult()
        {
        }

        public bool IsSuccess { get; private set; }
        
        public T Entity { get; }

        public string Message
        {
            get;
            private set;
        }

        public string ErrorMessage
        {
            get;
            private set;
        }

        public ResultCode Code
        {
            get;
            private set;
        }

        public static EntityOperationResult<T> Success(T entity)
        {
            return new EntityOperationResult<T>(entity)
            {
                IsSuccess = true,
                Code = ResultCode.Success
            };
        }
        
        public static EntityOperationResult<T> Success(T entity, string message)
        {
            return new EntityOperationResult<T>(entity)
            {
                IsSuccess = true,
                Code = ResultCode.Success,
                Message = message,
            };
        }

        public static EntityOperationResult<T> Failure(string errorMessages, ResultCode code)
        {
            var result = new EntityOperationResult<T>
            {
                IsSuccess = false,
                ErrorMessage = errorMessages,
                Code = code
            };

            return result;
        }
    }
}
