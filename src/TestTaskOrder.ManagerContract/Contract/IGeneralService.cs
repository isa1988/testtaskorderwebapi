﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;

namespace TestTaskOrder.ManagerContract.Contract
{
    public interface IGeneralService<TDto, TOrder>
       where TDto : class, IServiceDto
       where TOrder : Enum
    {
        /// <summary>
        /// Добавить запись в базу
        /// </summary>
        /// <param name="createDto">Объект добавление</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> CreateAsync(TDto createDto);
        /// <summary>
        /// Вернуть все записи
        /// </summary>
        /// <returns></returns>
        Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllAsync(PagingOrderSettingDto<TOrder> orderSetting);

        /// <summary>
        /// Вернуть все записи со всеми зависимостями
        /// </summary>
        /// <returns></returns>
        Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting);
    }

    public interface IGeneralService<TDto, TId, TOrder> : IGeneralService<TDto, TOrder>
        where TDto : class, IServiceDto<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        /// <summary>
        /// Вернуть конкретный объект из базы
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> GetByIdAsync(TId id);

        /// <summary>
        /// Вернуть конкретный объект из базы со всеми зависимостями
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> GetByIdDetailsAsync(TId id);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> DeleteItemAsync(TId id);
        
    }
}
