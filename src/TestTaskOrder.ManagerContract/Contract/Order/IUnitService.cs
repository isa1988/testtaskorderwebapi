﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ManagerContract.Contract.Order
{
    public interface IUnitService : IGeneralServiceFromDelete<UnitDto, Guid, OrderUnitFields>
    {
        Task<EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>> GetAllWithDelAsync(
            SearchByOneLineDtoFilter<OrderUnitFields> filter);
        Task<EntityOperationResult<PageInfoListDto<UnitDto, OrderUnitFields>>> GetAllAsync(
            SearchByOneLineDtoFilter<OrderUnitFields> filter);
        Task<EntityOperationResult<UnitDto>> EditAsync(UnitDto editDto);
    }
}
