﻿
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ManagerContract.Contract.Order
{
    public interface IOrderService : IGeneralServiceFromDelete<OrderDto, Guid, OrderOrderFields>
    {
        Task<EntityOperationResult<PageInfoListDto<OrderDto, OrderOrderFields>>> GetAllWithDelAsync(
            OrderFilterDto filter);
        Task<EntityOperationResult<OrderDto>> EditAsync(OrderDto orderDto);
    }
}
