﻿
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Order;
using TestTaskOrder.ManagerContract.Filters.Order;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ManagerContract.Contract.Order
{
    public interface IOrderItemService : IGeneralService<OrderItemDto, Guid, OrderOrderItemFields>
    {
        Task<EntityOperationResult<PageInfoListDto<OrderItemDto, OrderOrderItemFields>>> GetAllAsync(
            OrderItemFilterDto filter);
        Task<EntityOperationResult<OrderItemDto>> EditAsync(OrderItemDto editDto);
    }
}
