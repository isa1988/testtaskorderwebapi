﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.Provider;
using TestTaskOrder.ManagerContract.Filters;
using TestTaskOrder.Primitive.PagingOrderSettings.Provider;

namespace TestTaskOrder.ManagerContract.Contract.Provider
{
    public interface IProviderService : IGeneralServiceFromDelete<ProviderDto, Guid, OrderProviderFields>
    {
        Task<EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>> GetAllWithDelAsync(
            SearchByOneLineDtoFilter<OrderProviderFields> filter);
        Task<EntityOperationResult<PageInfoListDto<ProviderDto, OrderProviderFields>>> GetAllAsync(
            SearchByOneLineDtoFilter<OrderProviderFields> filter);
        Task<EntityOperationResult<ProviderDto>> EditAsync(ProviderDto editDto);
    }
}
