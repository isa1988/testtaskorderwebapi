﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;

namespace TestTaskOrder.ManagerContract.Contract
{
    public interface IGeneralServiceFromDelete<TDto, TOrder> : IGeneralService<TDto, TOrder>
        where TDto : class, IServiceForDeleteDto
        where TOrder : Enum
    {
        /// <summary>
        /// Вернуть все записи
        /// </summary>
        /// <returns></returns>
        Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllWithDeletedAsync(PagingOrderSettingDto<TOrder> orderSetting);

        /// <summary>
        /// Вернуть все записи со всеми зависимостями
        /// </summary>
        /// <returns></returns>
        Task<EntityOperationResult<PageInfoListDto<TDto, TOrder>>> GetAllWithDeletedDetailsAsync(PagingOrderSettingDto<TOrder> orderSetting);
    }

    public interface IGeneralServiceFromDelete<TDto, TId, TOrder> : IGeneralServiceFromDelete<TDto, TOrder>
        where TDto : class, IServiceForDeleteDto<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        /// <summary>
        /// Вернуть конкретный объект из базы
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> GetByIdAsync(TId id);

        /// <summary>
        /// Вернуть конкретный объект из базы со всеми зависимостями
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> GetByIdDetailsAsync(TId id);

        /// <summary>
        /// Пометить на удаление
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> DeleteItemAsync(TId id);

        
        /// <summary>
        /// Удалить объект из БД
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        /// <returns></returns>
        Task<EntityOperationResult<TDto>> DeleteItemFromDbAsync(TId id);
        
    }
}
