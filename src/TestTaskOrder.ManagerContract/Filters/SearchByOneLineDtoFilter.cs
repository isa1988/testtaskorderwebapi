﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive;

namespace TestTaskOrder.ManagerContract.Filters
{
    public class SearchByOneLineDtoFilter<T> : IServiceDto
    where T: Enum
    {
        public string Search { get; set; }
        public PagingOrderSettingDto<T> OrderSetting { get; set; }

        public async Task ClearSpace()
        {
            Search = Search.TrimWithNull();
        }
    }
}
