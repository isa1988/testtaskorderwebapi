﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Authentication;

namespace TestTaskOrder.ManagerContract.Filters.Authentication
{
    public class RoleFilterDto : IServiceDto
    {
        public Guid UserId { get; set; }
        public PagingOrderSettingDto<OrderRoleFields> OrderSetting { get; set; }
        public async Task ClearSpace()
        {

        }
    }
}
