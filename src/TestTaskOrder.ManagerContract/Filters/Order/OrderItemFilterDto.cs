﻿using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ManagerContract.Filters.Order
{
    public class OrderItemFilterDto : IServiceDto
    {
        public Guid OrderId { get; set; }
        public PagingOrderSettingDto<OrderOrderItemFields> OrderSetting { get; set; }
        public async Task ClearSpace()
        {
        }
    }
}
