﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTaskOrder.ManagerContract.Dto;
using TestTaskOrder.ManagerContract.Dto.PagingOrderSettings;
using TestTaskOrder.Primitive;
using TestTaskOrder.Primitive.PagingOrderSettings.Order;

namespace TestTaskOrder.ManagerContract.Filters.Order
{
    public class OrderFilterDto : IServiceDto
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public Guid? ProviderId { get; set; }
        public PeriodFilterDto<DateTime> CreateDate { get; set; }
        public PeriodFilterDto<DateTime> ModifyDate { get; set; }
        public PeriodFilterDto<DateTime> Date { get; set; }
        public Guid? AuthorId { get; set; }
        public bool IsCurrentUser { get; set; }
        public Guid CurrentUserId { get; set; }
        public PagingOrderSettingDto<OrderOrderFields> OrderSetting { get; set; }
        public async Task ClearSpace()
        {
            Number = Number.TrimWithNull();
            Name = Name.TrimWithNull();
        }
    }
}
