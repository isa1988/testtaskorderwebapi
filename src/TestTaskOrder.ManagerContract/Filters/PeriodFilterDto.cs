﻿namespace TestTaskOrder.ManagerContract.Filters
{
    public class PeriodFilterDto<T>
        where T : struct
    {
        public T? Start { get; set; }
        public T? End { get; set; }
    }
}
